﻿
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Znode.Engine.Admin.Agents;

using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;

using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;


namespace Znode.Admin.Custom.Agents.Agents
{
    public class AWCTOrderAgent : OrderAgent
    {
        #region Private Variable

        private readonly IOrderClient _orderClient;
        private readonly IUserClient _userClient;
        private readonly ICustomerClient _customerClient;
        private readonly IAccountClient _accountClient;
        private readonly IPortalClient _portalClient;
        private readonly IEcommerceCatalogClient _ecommerceCatalogClient;
        private readonly IPublishProductClient _publishProductClient;
        private readonly IMediaConfigurationClient _mediaConfigurationClient;
        private readonly IShippingAgent _shippingAgent;
        private readonly IEcommerceCatalogClient _portalCatalogClient;
        private readonly IUserAgent _userAgent;
        private readonly IPaymentClient _paymentClient;
        private readonly IShoppingCartClient _shoppingCartClient;
        private readonly ICartAgent _cartAgent;
        private readonly IPaymentAgent _paymentAgent;
        private readonly IShippingClient _shippingClient;
        private readonly IAccountQuoteClient _quoteClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IPIMAttributeClient _attributeClient;
        private readonly IAddressClient _addressClient;


        #endregion Private Variable
        public AWCTOrderAgent(IShippingClient shippingClient, IShippingTypeClient shippingTypeClient, IStateClient stateClient,
             ICityClient cityClient, IProductsClient productClient, IBrandClient brandClient,
             IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient,
             IDomainClient domainClient, IOrderClient orderClient, IEcommerceCatalogClient ecomCatalogClient,
             ICustomerClient customerClient, IPublishProductClient publishProductClient, IMediaConfigurationClient mediaConfigClient,
             IPaymentClient paymentClient, IShoppingCartClient shoppingCartClient, IAccountQuoteClient accountQuoteClient,
             IOrderStateClient orderStateClient, IPIMAttributeClient pimAttributeClient, ICountryClient countryClient, IAddressClient addressClient) :
            base(shippingClient, shippingTypeClient, stateClient,
              cityClient, productClient, brandClient,
              userClient, portalClient, accountClient, roleClient,
              domainClient, orderClient, ecomCatalogClient,
              customerClient, publishProductClient, mediaConfigClient,
              paymentClient, shoppingCartClient, accountQuoteClient,
              orderStateClient, pimAttributeClient, countryClient, addressClient)

        {
        }

        //Create Single Order Line Item.
        public override void CreateSingleOrderLineItem(OrderViewModel orderModel, List<OrderLineItemViewModel> orderLineItemListModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemViewModel _lineItems in orderModel.OrderLineItems)
            {
                bool isGroupProduct = false;

                if (!CheckForReturnInLineItem(_lineItems?.OrderLineItemStateId))
                {
                    if (HelperUtility.IsNull(_lineItems.ParentOmsOrderLineItemsId))
                    {
                        List<OrderLineItemViewModel> childItems = orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == _lineItems.OmsOrderLineItemsId).ToList();
                        foreach (OrderLineItemViewModel orderLineItem in childItems)
                        {
                            OrderLineItemViewModel orderLineItemModel = new OrderLineItemViewModel();
                            //orderLineItemModel = _lineItems;
                            orderLineItemModel.OmsOrderLineItemsId = orderLineItem.OmsOrderLineItemsId;
                            orderLineItemModel.OrderLineItemState = orderLineItem.OrderLineItemState;
                            orderLineItemModel.OrderLineItemStateId = orderLineItem.OrderLineItemStateId;
                            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable)
                            {
                                orderLineItemModel.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Configurable;
                                orderLineItemModel.ProductName = orderLineItem.ProductName;
                                orderLineItemModel.Sku = orderLineItem.Sku;
                                orderLineItemModel.Quantity = orderLineItem.Quantity;
                                orderLineItemModel.Description = orderLineItem.Description;
                                orderLineItemModel.Price = orderLineItem.Price;
                                orderLineItemModel.Total = (orderLineItem.Quantity * orderLineItem.Price);
                            }
                            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns)
                            {
                                orderLineItemModel.Total = (orderLineItem.Quantity * (orderLineItem.Price + _lineItems.Price));
                                orderLineItemModel.Sku = orderLineItem.Sku;
                                orderLineItemModel.ProductName = _lineItems.ProductName;
                                orderLineItemModel.Price = (orderLineItem.Price + _lineItems.Price);
                            }
                            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)
                            {
                                OrderLineItemViewModel orderLineItems = new OrderLineItemViewModel();
                                orderLineItems = orderLineItem;
                                orderLineItems.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Group;
                                orderLineItems.ProductName = _lineItems.ProductName;
                                orderLineItems.Description = orderLineItem.Description;
                                orderLineItems.Sku = orderLineItem.Sku;
                                orderLineItems.Quantity = orderLineItem.Quantity;
                                orderLineItems.Price = orderLineItem.Price;
                                orderLineItems.Total = (orderLineItem.Quantity * orderLineItem.Price);
                                orderLineItemListModel.Add(orderLineItems);
                                isGroupProduct = true;
                            }
                            if (!isGroupProduct) orderLineItemListModel.Add(orderLineItemModel);
                            orderLineItemModel.Total = (_lineItems.Quantity * _lineItems.Price);
                        }


                    }
                }
            }
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }
    }
}
