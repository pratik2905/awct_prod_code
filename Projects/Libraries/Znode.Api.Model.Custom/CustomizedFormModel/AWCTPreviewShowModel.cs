﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Api.Model.CustomizedFormModel
{
    public class AWCTPreviewShowModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StudioName { get; set; }
        public string CustomerNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string NoOfTeacher { get; set; }
        public string AnotherDay { get; set; }
    }
}
