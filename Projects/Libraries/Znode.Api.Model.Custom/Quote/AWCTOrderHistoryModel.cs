﻿using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class AWCTOrderHistoryModel : BaseModel
    {
        public int OmsHistoryId { get; set; }
        public int OmsOrderId { get; set; }
        public int? OmsOrderDetailsId { get; set; }
        public int? OmsNotesId { get; set; }
        public string TransactionId { get; set; }
        public string Message { get; set; }
        public string Notes { get; set; }
        public string UpdatedBy { get; set; }
        public decimal? OrderAmount { get; set; }
        public int OMSQuoteId { get; set; }
    }
}
