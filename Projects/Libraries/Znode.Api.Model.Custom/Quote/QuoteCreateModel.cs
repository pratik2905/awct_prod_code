﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Resources;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteCreateModel : BaseModel
    {
        [Required(ErrorMessageResourceName = "ErrorUserIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorUserIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int UserId { get; set; }

        [Required(ErrorMessageResourceName = "ErrorPortalIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorPortalIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int PortalId { get; set; }

        public string OmsQuoteStatus { get; set; }

        public int OmsOrderStateId { get; set; }

        public string QuoteNumber { get; set; }

        public string QuoteTypeCode { get; set; }

        public Decimal TaxCost { get; set; }

        public Decimal ShippingCost { get; set; }

        public int PublishStateId { get; set; }

        public string AdditionalInstruction { get; set; }

        public DateTime? InHandDate { get; set; }

        public DateTime? QuoteExpirationDate { get; set; }

        [Required(ErrorMessageResourceName = "ErrorShippingIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorShippingIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int ShippingId { get; set; }

        [Required(ErrorMessageResourceName = "ErrorShippingAddressIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorShippingAddressIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int ShippingAddressId { get; set; }

        [Required(ErrorMessageResourceName = "ErrorBillingAddressIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorBillingAddressIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int BillingAddressId { get; set; }

        public bool FreeShipping { get; set; }

        public string ShippingCode { get; set; }

        [Required(ErrorMessageResourceName = "ErrorQuoteTotalRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)$", ErrorMessageResourceName = "ErrorQuoteTotalRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public Decimal? QuoteTotal { get; set; }

        public string CookieMappingId { get; set; }

        public int OmsQuoteId { get; set; }

        public string ShippingConstraintCode { get; set; }

        public string JobName { get; set; }

        public List<ProductDetailModel> productDetails { get; set; }

        public bool IsTaxExempt { get; set; }
    }
}
