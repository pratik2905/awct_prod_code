﻿using System;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteApprovalModel : BaseModel
    {
        public int OmsQuoteApprovalId { get; set; }

        public int OmsQuoteId { get; set; }

        public int ApproverUserId { get; set; }

        public string Comments { get; set; }

        public string ApproverUserName { get; set; }

        public DateTime CommentModifiedDateTime { get; set; }
    }
}
