﻿using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
//using Znode.Libraries.Data.DataModel;

namespace Znode.Sample.Api.Model.Quote
{
    public class ZnodeOmsQuote : BaseModel
    {
        public ZnodeOmsQuote()
        {
           
        }

        public int OmsQuoteId { get; set; }
        public int PortalId { get; set; }
        public int UserId { get; set; }
        public int OmsOrderStateId { get; set; }
        public Nullable<int> ShippingId { get; set; }
        public Nullable<int> ShippingAddressId { get; set; }
        public Nullable<int> BillingAddressId { get; set; }
        public Nullable<int> ApproverUserId { get; set; }
        public string AdditionalInstruction { get; set; }
        public Nullable<decimal> QuoteOrderTotal { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public Nullable<int> PaymentSettingId { get; set; }
        public bool IsPendingPayment { get; set; }
        public string CardType { get; set; }
        public Nullable<int> CreditCardExpMonth { get; set; }
        public Nullable<int> CreditCardExpYear { get; set; }
        public string PaymentTransactionToken { get; set; }
        public string CreditCardNumber { get; set; }
        public string PoDocument { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public Nullable<bool> IsConvertedToOrder { get; set; }
        public Nullable<decimal> TaxCost { get; set; }
        public Nullable<decimal> ShippingCost { get; set; }
        public Nullable<int> OmsQuoteTypeId { get; set; }
        public string QuoteTypeCode { get; set; }
        public Nullable<byte> PublishStateId { get; set; }
        public Nullable<System.DateTime> QuoteExpirationDate { get; set; }
        public Nullable<System.DateTime> InHandDate { get; set; }
        public string QuoteNumber { get; set; }
        //public Nullable<int> ShippingTypeId { get; set; }
        public int ShippingTypeId { get; set; }
        public string AccountNumber { get; set; }
        public string ShippingMethod { get; set; }
        public string CultureCode { get; set; }
        public string JobName { get; set; }
        public string ShippingConstraintCode { get; set; }
        public Nullable<bool> IsTaxExempt { get; set; }

    }
}