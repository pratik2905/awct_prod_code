﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Resources;

namespace Znode.Sample.Api.Model.Quote
{
    public class UpdateQuoteModel : BaseModel
    {
        [Required(ErrorMessageResourceName = "ErrorQuoteIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorQuoteIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int OmsQuoteId { get; set; }

        public int OmsQuoteStateId { get; set; }

        public int UserId { get; set; }

        public int ShippingId { get; set; }

        public int ShippingTypeId { get; set; }

        public string AccountNumber { get; set; }

        public string ShippingMethod { get; set; }

        public Decimal ShippingCost { get; set; }

        public DateTime? InHandDate { get; set; }

        public DateTime? QuoteExpirationDate { get; set; }

        public int ShippingAddressId { get; set; }

        public int BillingAddressId { get; set; }

        public Decimal TaxCost { get; set; }

        public bool IsTaxExempt { get; set; }

        public Decimal SubTotal { get; set; }

        public Decimal QuoteTotal { get; set; }

        public List<QuoteLineItemModel> QuoteLineItems { get; set; }

        public string QuoteHistory { get; set; }

        public string AdditionalInstructions { get; set; }

        public string JobName { get; set; }

        public string ShippingConstraintCode { get; set; }
    }
}
