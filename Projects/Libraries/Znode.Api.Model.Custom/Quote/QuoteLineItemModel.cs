﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteLineItemModel : BaseModel
    {
        public int OmsQuoteLineItemId { get; set; }

        public int OmsQuoteId { get; set; }

        public int? ParentOmsQuoteLineItemId { get; set; }

        public Decimal? Price { get; set; }

        public Decimal Quantity { get; set; }

        public Decimal ShippingCost { get; set; }

        public string SKU { get; set; }

        public int? OrderLineItemRelationshipTypeId { get; set; }

        public bool IsAllowedTerritories { get; set; } = true;
    }
}
