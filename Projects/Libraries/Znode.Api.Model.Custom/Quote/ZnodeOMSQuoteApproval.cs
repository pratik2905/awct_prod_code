﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data.DataModel;

namespace Znode.Sample.Api.Model.Quote
{
    public class ZnodeOMSQuoteApproval:BaseModel
    {
        public int OmsQuoteApprovalId { get; set; }
        public int OmsQuoteId { get; set; }
        public int ApproverLevelId { get; set; }
        public int ApproverUserId { get; set; }
        public Nullable<int> OmsOrderStateId { get; set; }
        public int UserId { get; set; }
        public bool IsApprovalRoutingComplete { get; set; }
        public string Comments { get; set; }
      
        public int? ApproverOrder { get; set; }
        public int? OmsQuoteCommentId { get; set; }

        public virtual ZnodeOmsQuote ZnodeOmsQuote { get; set; }
        public virtual ZnodeOmsQuoteComment ZnodeOmsQuoteComment { get; set; }
    }
}
