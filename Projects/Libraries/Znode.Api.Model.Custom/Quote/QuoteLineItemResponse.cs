﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteLineItemResponse : BaseResponse
    {
        public List<QuoteLineItemModel> QuoteLineItems { get; set; }
    }
}
