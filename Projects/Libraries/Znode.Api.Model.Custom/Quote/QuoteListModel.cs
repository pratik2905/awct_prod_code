﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteListModel : BaseListModel
    {
        public List<QuoteModel> Quotes { get; set; }

        public string PortalName { get; set; }
    }
}
