﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class ProductDetailModel : BaseModel
    {
        public int OmsSavedcartLineItemId { get; set; }
        public decimal? Price { get; set; }
        public decimal ShippingCost { get; set; }
        public string SKU { get; set; }
    }
}
