﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class AWCTOrderNumberResponse : BaseResponse
    {
        public string OrderNumber { get; set; }
    }
}
