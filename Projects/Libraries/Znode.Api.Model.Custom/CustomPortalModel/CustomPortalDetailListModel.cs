﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class CustomPortalDetailListModel : PortalListModel
    {
        public List<CustomPortalDetailModel> CustomPortalDetailList { get; set; }
    }
}
