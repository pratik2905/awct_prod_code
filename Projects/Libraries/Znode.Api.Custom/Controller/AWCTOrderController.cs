﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Responses;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.Controller
{
    public class AWCTOrderController : BaseController
    {
        #region Private Variables
        private readonly IOrderService _orderService;
        #endregion
        #region Constructor
        public AWCTOrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        #endregion
        [ResponseType(typeof(AWCTOrderNumberResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GenerateOrderNumber()
        {
            SubmitOrderModel model = new SubmitOrderModel();
            HttpResponseMessage response;
            try
            {
                string data = _orderService.GenerateOrderNumber(model,null).ToString();
                response = IsNotNull(data) ? CreateOKResponse(new AWCTOrderNumberResponse { OrderNumber = data }) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new AWCTOrderNumberResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTOrderNumberResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;

        }

    }
}
