﻿using MongoDB.Driver;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Controllers;

using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.CustomizedFormModel;

namespace Znode.Api.Custom.Controller
{
    public class AWCTCustomizedFormController : BaseController
    {
        private readonly IAWCTCustomizedFormService _AWCTCustomizedFormService;

        public AWCTCustomizedFormController(IAWCTCustomizedFormService AWCTCustomizedFormService)
        {
            _AWCTCustomizedFormService = AWCTCustomizedFormService;

        }

        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage ModelSearch(AWCTModelSearchModel model)
        {
            HttpResponseMessage response;

            try
            {

                bool result = _AWCTCustomizedFormService.ModelSearch(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = result });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;

        }

        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage NewCustomerApplication(AWCTNewCustomerApplicationModel model)
        {
            HttpResponseMessage response;

            try
            {

                bool result = _AWCTCustomizedFormService.NewCustomerApplication(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = result });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;

        }

        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage BecomeAContributer(AWCTBecomeAContributerModel model)
        {
            HttpResponseMessage response;

            try
            {

                bool result = _AWCTCustomizedFormService.BecomeAContributer(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = result });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;

        }

        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage PreviewShow(AWCTPreviewShowModel model)
        {
            HttpResponseMessage response;

            try
            {

                bool result = _AWCTCustomizedFormService.PreviewShow(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = result });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;

        }
        /*Start Quote Lookup*/
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage QuoteLookup(AWCTQuoteLookupModel model)
        {
            HttpResponseMessage response;

            try
            {

                bool result = _AWCTCustomizedFormService.QuoteLookup(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = result });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;

        }
        /*End Quote Lookup*/
    }
}
