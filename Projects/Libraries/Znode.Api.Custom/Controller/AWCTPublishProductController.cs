﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache.Cache;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Cart;
using Znode.Sample.Api.Model.Responses;


namespace Znode.Api.Custom.Controller
{
    public class AWCTPublishProductController : BaseController
    {
        #region Private Variables

        private readonly IAWCTPublishProductCache _cache;
        private readonly IAWCTPublishProductService _service;

        #endregion

        #region Constructor
        public AWCTPublishProductController(IAWCTPublishProductService service)//:base(service)
        {
            _service = service;
            _cache = new AWCTPublishProductCache(_service);
        }
        #endregion

        [ResponseType(typeof(AWCTPublishProductResponse))]
        [HttpGet]
        public HttpResponseMessage GetConfigurableProductViewModel(int publishProductId)
        {
            HttpResponseMessage response;
           

            try
            {
                string data = _cache.GetConfigurableProductViewModel(publishProductId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AWCTPublishProductResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }

        [ResponseType(typeof(AWCTPublishProductResponse))]
        [HttpGet]
        public HttpResponseMessage GetPriceSizeList(int configurableProductId)
        {
            HttpResponseMessage response;


            try
            {
                string data = _cache.GetPriceSizeList(configurableProductId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AWCTPublishProductResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }

        [ResponseType(typeof(AWCTPublishProductResponse))]
        [HttpGet]
        public HttpResponseMessage GetNewPublishProduct(int publishProductId)
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetPublishProduct(publishProductId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AWCTPublishProductResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }

        [ResponseType(typeof(AWCTPublishProductResponse))]
        [HttpGet]
        public HttpResponseMessage GetExtendedProductDetails(int publishProductId)
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetExtendedProductDetails(publishProductId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AWCTPublishProductResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }
        [ResponseType(typeof(AWCTPublishProductResponse))]
        [HttpGet]
        public HttpResponseMessage GetGlobalAttributeData(string globalAttributeCodes)
        {
            HttpResponseMessage response;


            try
            {
                string data = _cache.GetGlobalAttributeData(globalAttributeCodes, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AWCTPublishProductResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }

        ///*Nivi Code To Get estimated ship date data*/
        //[ResponseType(typeof(AWCTPublishProductResponse))]
        //[HttpGet]
        //public HttpResponseMessage GetEstimatedShipDateData(string ProductIdliststr)
        //{
        //    HttpResponseMessage response;


        //    try
        //    {
        //        string data = _cache.GetEstimatedShipDateData(ProductIdliststr, RouteUri, RouteTemplate);
        //        response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AWCTPublishProductResponse>(data) : CreateNoContentResponse();
        //    }
        //    catch (ZnodeException ex)
        //    {
        //        response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
        //        ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
        //    }
        //    catch (Exception ex)
        //    {
        //        response = CreateInternalServerErrorResponse(new AWCTPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
        //        ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
        //    }

        //    return response;
        //}
        ///*Nivi Code To Get estimated ship date data*/

    }
}
