﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache.Cache;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Custom.Controller
{
    public class QuoteController : BaseController
    {
        private readonly IQuoteService _quoteService;
        private readonly IQuoteCache _quoteCache;

        public QuoteController(IQuoteService quoteService)
        {
            _quoteService = quoteService;
            _quoteCache = new QuoteCache(quoteService);
        }
        /// <summary>Create Quote</summary>
        /// <param name="quoteCreateModel"></param>
        /// <returns>QuoteCreateModel</returns>
        [ResponseType(typeof(CreateQuoteResponse))]
        [HttpPost]
        [ValidateModel]
        public virtual HttpResponseMessage Create([FromBody] QuoteCreateModel quoteCreateModel)
        {
            HttpResponseMessage httpResponseMessage1;
            try
            {
                QuoteCreateModel quoteCreateModel1 = _quoteService.Create(quoteCreateModel);
                HttpResponseMessage httpResponseMessage2;
                if (!HelperUtility.IsNotNull((object)quoteCreateModel1))
                {
                    httpResponseMessage2 = CreateNoContentResponse();
                }
                else
                {
                    httpResponseMessage2 = CreateCreatedResponse<CreateQuoteResponse>(new CreateQuoteResponse()
                    {
                        Quote = quoteCreateModel1
                    });
                }

                httpResponseMessage1 = httpResponseMessage2;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                TrueFalseResponse data = new TrueFalseResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage1 = CreateInternalServerErrorResponse<TrueFalseResponse>(data);
            }
            catch (Exception ex)
            {
                TrueFalseResponse data = new TrueFalseResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage1 = CreateInternalServerErrorResponse<TrueFalseResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage1;
        }

        /// <summary>Get the list of all Quotes.</summary>
        /// <returns>Returns list of all Quotes.</returns>
        [ResponseType(typeof(QuoteListResponse))]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                string quoteList = _quoteCache.GetQuoteList(RouteUri, RouteTemplate);
                httpResponseMessage = string.IsNullOrEmpty(quoteList) ? CreateNoContentResponse() : CreateOKResponse<QuoteListResponse>(quoteList);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                QuoteListResponse data = new QuoteListResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteListResponse>(data);
            }
            catch (Exception ex)
            {
                QuoteListResponse data = new QuoteListResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteListResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }

        /// <summary>Get Quote Receipt details.</summary>
        /// <param name="quoteId"> quote Id to get Quote Details</param>
        /// <returns> quote details</returns>
        [ResponseType(typeof(QuoteResponse))]
        public HttpResponseMessage GetQuoteReceipt(int quoteId)
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                string quoteReceipt = _quoteCache.GetQuoteReceipt(quoteId, RouteUri, RouteTemplate);
                httpResponseMessage = !string.IsNullOrEmpty(quoteReceipt) ? CreateOKResponse<QuoteResponse>(quoteReceipt) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                QuoteResponse data = new QuoteResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteResponse>(data);
            }
            catch (Exception ex)
            {
                QuoteResponse data = new QuoteResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }

        [ResponseType(typeof(QuoteResponse))]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage GetGuestQuoteReceipt(QuoteModel quoteModel)
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                string quoteReceipt = _quoteCache.GetGuestQuoteReceipt(quoteModel, RouteUri, RouteTemplate);
                httpResponseMessage = !string.IsNullOrEmpty(quoteReceipt) ? CreateOKResponse<QuoteResponse>(quoteReceipt) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                QuoteResponse data = new QuoteResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteResponse>(data);
            }
            catch (Exception ex)
            {
                QuoteResponse data = new QuoteResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }


        /// <summary>Get Quote details by Quote id.</summary>
        /// <param name="omsQuoteId">Quote Id</param>
        /// <returns>Get Quote details.</returns>
        [ResponseType(typeof(QuoteDetailResponse))]
        public HttpResponseMessage GetQuoteById(int omsQuoteId)
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                string quoteById = _quoteCache.GetQuoteById(omsQuoteId, RouteUri, RouteTemplate);
                httpResponseMessage = !string.IsNullOrEmpty(quoteById) ? CreateOKResponse<QuoteDetailResponse>(quoteById) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                QuoteDetailResponse data = new QuoteDetailResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteDetailResponse>(data);
            }
            catch (Exception ex)
            {
                QuoteDetailResponse data = new QuoteDetailResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteDetailResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }

        /// <summary>Convert quote to the order.</summary>
        /// <param name="convertToOrderModel"></param>
        /// <returns>OrderResponse</returns>
        [ResponseType(typeof(OrderResponse))]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage ConvertQuoteToOrder(
          [FromBody] ConvertQuoteToOrderModel convertToOrderModel)
        {
            HttpResponseMessage httpResponseMessage1;
            try
            {
                OrderModel order = _quoteService.ConvertQuoteToOrder(convertToOrderModel);
                HttpResponseMessage httpResponseMessage2;
                if (!HelperUtility.IsNotNull((object)order))
                {
                    httpResponseMessage2 = CreateNoContentResponse();
                }
                else
                {
                    httpResponseMessage2 = CreateCreatedResponse<OrderResponse>(new OrderResponse()
                    {
                        Order = order
                    });
                }

                httpResponseMessage1 = httpResponseMessage2;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                OrderResponse data = new OrderResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage1 = CreateInternalServerErrorResponse<OrderResponse>(data);
            }
            catch (Exception ex)
            {
                OrderResponse data = new OrderResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage1 = CreateInternalServerErrorResponse<OrderResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage1;
        }

        /// <summary>Get Quote LineItems by QuoteId.</summary>
        /// <returns>Returns list of LineItems</returns>
        [ResponseType(typeof(QuoteLineItemResponse))]
        [HttpGet]
        public HttpResponseMessage GetQuoteLineItemByQuoteId(int omsQuoteId)
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                string quoteLineItems = _quoteCache.GetQuoteLineItems(omsQuoteId, RouteUri, RouteTemplate);
                httpResponseMessage = string.IsNullOrEmpty(quoteLineItems) ? CreateNoContentResponse() : CreateOKResponse<QuoteLineItemResponse>(quoteLineItems);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                QuoteLineItemResponse data = new QuoteLineItemResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteLineItemResponse>(data);
            }
            catch (Exception ex)
            {
                QuoteLineItemResponse data = new QuoteLineItemResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<QuoteLineItemResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }

        /// <summary>Update existing Quote.</summary>
        /// <param name="model"></param>
        /// <returns>Updates existing Quote.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        [ValidateModel]
        public HttpResponseMessage UpdateQuote([FromBody] UpdateQuoteModel model)
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                httpResponseMessage = CreateOKResponse<TrueFalseResponse>(new TrueFalseResponse()
                {
                    booleanModel = _quoteService.UpdateQuote(model)
                });
            }
            catch (ZnodeException ex)
            {
                TrueFalseResponse data = new TrueFalseResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<TrueFalseResponse>(data);
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
            }
            catch (Exception ex)
            {
                TrueFalseResponse data = new TrueFalseResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<TrueFalseResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }

    }
}
