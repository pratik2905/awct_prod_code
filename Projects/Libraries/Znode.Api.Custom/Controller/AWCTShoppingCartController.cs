﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Controllers.OMS.ShoppingCartController;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Cart;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Controller
{
   public class AWCTShoppingCartController : BaseController
    {
        #region Private Variables
        private readonly IAWCTShoppingCartService _awctshoppingCartService;
        private readonly IShoppingCartService _shoppingCartService;
        #endregion

        #region Constructor
        public AWCTShoppingCartController()
        {

        }

        public AWCTShoppingCartController(IAWCTShoppingCartService awctshoppingCartService , IShoppingCartService shoppingCartService)
        {
            _awctshoppingCartService = awctshoppingCartService;
            //_shoppingCartService = shoppingCartService;
            //_shoppingCartCache = new ShoppingCartCache(_shoppingCartService);
        }
        #endregion

        /*Nivi Code To Get estimated ship date data*/
        //[ResponseType(typeof(AWCTPublishProductResponse))]
        //[HttpGet]
        //public HttpResponseMessage GetEstimatedShipDateData(string ProductIdliststr)
        //{
        //    HttpResponseMessage httpResponseMessage1;
        //    try
        //    {
        //        var result = _awctshoppingCartService.GetEstimatedShipDateData(ProductIdliststr);
        //        HttpResponseMessage httpResponseMessage2;
        //        if (!HelperUtility.IsNotNull((object)result))
        //        {
        //            httpResponseMessage2 = CreateNoContentResponse();
        //        }
        //        else
        //        {
        //            httpResponseMessage2 = CreateCreatedResponse<AWCTPublishProductResponse>(new AWCTPublishProductResponse()
        //            {
        //                AWCTCartModel = result
        //            });
        //        }

        //        httpResponseMessage1 = httpResponseMessage2;
        //    }
        //    catch (ZnodeException ex)
        //    {
        //        ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
        //        AWCTPublishProductResponse data = new AWCTPublishProductResponse();
        //        data.HasError = true;
        //        data.ErrorMessage = ((Exception)ex).Message;
        //        data.ErrorCode = ex.ErrorCode;
        //        httpResponseMessage1 = CreateInternalServerErrorResponse<AWCTPublishProductResponse>(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        AWCTPublishProductResponse data = new AWCTPublishProductResponse();
        //        data.HasError = true;
        //        data.ErrorMessage = ex.Message;
        //        httpResponseMessage1 = CreateInternalServerErrorResponse<AWCTPublishProductResponse>(data);
        //        ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
        //    }
        //    return httpResponseMessage1;
        //}
        /*Nivi Code To Get estimated ship date data*/

    }
}
