﻿using System;
using System.Configuration;
using System.Diagnostics;
using Znode.Engine.Api.Models;
using Znode.Engine.Taxes;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom
{
    public class AWCTZnodeTaxSalesTax : ZnodeTaxesType
    {
        #region Constructors
        public string shippingCountryCode = string.Empty;
        public int duty = Convert.ToInt32(ConfigurationManager.AppSettings["DutyCanada"]);
        public string DutyCountry = Convert.ToString(ConfigurationManager.AppSettings["DutyCountry"]);
        private decimal oDiscount = 0;
        public AWCTZnodeTaxSalesTax()
        {
            ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            Name = "Sales Tax";
            Description = "Applies sales tax to the shopping cart.";

            Controls.Add(ZnodeTaxRuleControl.SalesTax);
            Controls.Add(ZnodeTaxRuleControl.VAT);
            Controls.Add(ZnodeTaxRuleControl.GST);
            Controls.Add(ZnodeTaxRuleControl.PST);
            Controls.Add(ZnodeTaxRuleControl.HST);
            Controls.Add(ZnodeTaxRuleControl.Precedence);
            Controls.Add(ZnodeTaxRuleControl.Inclusive);
        }

        #endregion Constructors

        #region Public Method

        // Calculates the sales tax and updates the shopping cart.
        public override void Calculate()
        {
            if (IsValid())
            {
                decimal discount = ShoppingCart.OrderLevelDiscount;
                oDiscount = discount / ShoppingCart.ShoppingCartItems.Count;
                ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax-Calculate", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                shippingCountryCode = ShoppingCart.Shipping.ShippingCountryCode == null ? "" : ShoppingCart.Shipping.ShippingCountryCode;
                // Go through each item in the cart
                foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {

                    decimal extendedPrice = GetProductPrice(cartItem);

                    int? groupProductCount = cartItem?.Product?.ZNodeGroupProductCollection?.Count;

                    int? addOnProductCount = cartItem?.Product?.ZNodeAddonsProductCollection?.Count;

                    int? configurableProductCount = cartItem?.Product?.ZNodeConfigurableProductCollection?.Count;

                    //to calculate tax of parent porduct if product type not equals to Group/Configurable product
                    if (Equals(cartItem.Product.TaxClassID, TaxBag.TaxClassId) && Equals(cartItem.IsTaxCalculated, false) && Equals(Convert.ToInt32(groupProductCount), 0) && Equals(Convert.ToInt32(configurableProductCount), 0))
                    {
                        GetTaxesValue(extendedPrice, cartItem.Product);
                        SetTaxesValueToShoppingCart(cartItem.Product);
                        cartItem.IsTaxCalculated = true;
                        cartItem.TaxRuleId = this.TaxBag.TaxRuleId;
                    }
                    //to calculate tax for the product which dont associated to any tax rule but default tax rule is set for the store.
                    else if (IsDefaultTaxApplicable(cartItem.Product.TaxClassID) && Equals(cartItem.IsTaxCalculated, false) && Equals(Convert.ToInt32(groupProductCount), 0) && Equals(Convert.ToInt32(configurableProductCount), 0))
                    {
                        GetTaxesValue(extendedPrice, cartItem.Product);
                        SetTaxesValueToShoppingCart(cartItem.Product);
                        cartItem.IsTaxCalculated = true;
                        cartItem.TaxRuleId = this.TaxBag.TaxRuleId;
                    }

                    //to calculate tax of all Group product
                    if (groupProductCount > 0)
                    {
                        CalculateTaxForGroupProduct(cartItem.Product.ZNodeGroupProductCollection, cartItem);
                    }

                    //to calculate tax of all addOn product
                    if (addOnProductCount > 0)
                    {
                        CalculateTaxForAddonProduct(cartItem.Product.ZNodeAddonsProductCollection, cartItem);
                    }

                    //to calculate tax of all configurable product
                    if (configurableProductCount > 0)
                    {
                        CalculateTaxForConfigurableProduct(cartItem.Product.ZNodeConfigurableProductCollection, cartItem);
                    }

                    if (Convert.ToInt32(groupProductCount) > 0 || Convert.ToInt32(configurableProductCount) > 0)
                    {
                        cartItem.IsTaxCalculated = true;
                        cartItem.TaxRuleId = this.TaxBag.TaxRuleId;
                    }
                }
                ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax-Calculate Custom shipping" + Convert.ToString(ShoppingCart?.CustomShippingCost), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax-Calculate shipping cost" + Convert.ToString(ShoppingCart?.ShippingCost), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                //Add shipping amount in tax list
                if (ShoppingCart?.CustomShippingCost > 0)// && this.TaxBag?.ShippingTaxInd == true)
                {
                    SetShippingInTaxesValueOfShoppingCart(ShoppingCart.ShippingCost);
                }
                else if (ShoppingCart?.ShippingCost > 0)// && this.TaxBag?.ShippingTaxInd == true)
                {
                    SetShippingInTaxesValueOfShoppingCart(ShoppingCart.ShippingCost);
                }

            }
        }

        // Process anything that must be done before the order is submitted.
        public override bool PreSubmitOrderProcess()
        {
            AddressModel destinationaddress = ShoppingCart?.Payment?.BillingAddress ?? new AddressModel();

            if (string.IsNullOrEmpty(destinationaddress.CountryName) || string.IsNullOrEmpty(destinationaddress.StateName))
            {
                // set tax rate and error message
                ShoppingCart.TaxRate = 0;
                ShoppingCart.AddErrorMessage = "tax error: invalid destination country or state code.";
                return false;
            }

            return true;
        }

        // Process anything that must be done after the order is submitted.
        public override void PostSubmitOrderProcess()
        {
            // nothing to see here, move along, move along
        }

        #endregion Public Method

        #region Private Method

        // Calculate extended price.
        private decimal GetValue(decimal extendedPrice, decimal value)
            => extendedPrice * (value / 100);

        // Get product final/actual price.
        private decimal GetProductPrice(ZnodeShoppingCartItem cartItem)
        {
            try
            {
                ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax-Duty custom" + Convert.ToString(cartItem.Custom2), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                if (Convert.ToBoolean(cartItem.Custom2) == true && shippingCountryCode == DutyCountry)
                {
                    ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax-Duty", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                    return (cartItem.UnitPrice * cartItem.Quantity) + ((cartItem.UnitPrice * cartItem.Quantity) * duty) / 100 - cartItem.DiscountAmount - oDiscount;
                }
                else
                {
                    return (cartItem.UnitPrice * cartItem.Quantity) - cartItem.DiscountAmount - oDiscount;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("AWCTZnodeTaxSalesTax-Calculate Error: " + ex.Message, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return (cartItem.UnitPrice * cartItem.Quantity) - cartItem.DiscountAmount;
            }
        }

        // Get Varient product final/actual price.
        private decimal GetVarientProductPrice(ZnodeProductBaseEntity product, ZnodeShoppingCartItem cartItem)
        {
            decimal quantity = cartItem.Quantity;
            if (cartItem?.Product?.ZNodeGroupProductCollection?.Count > 0)
            {
                quantity = cartItem.Product.ZNodeGroupProductCollection[0].SelectedQuantity;
            }

            return (cartItem.UnitPrice * quantity);
        }

        // Get Group product final/actual price.
        private decimal GetGroupProductPrice(ZnodeProductBaseEntity groupItem, ZnodeShoppingCartItem cartItem)
        {
            return (cartItem.UnitPrice * groupItem.SelectedQuantity);
        }

        // Set GST, HST, PST, VAT, SalesTax to product.
        private void GetTaxesValue(decimal extendedPrice, ZnodeProductBaseEntity product)
        {
            product.GST += GetValue(extendedPrice, TaxBag.GST);
            product.HST += GetValue(extendedPrice, TaxBag.HST);
            product.PST += GetValue(extendedPrice, TaxBag.PST);
            product.VAT += GetValue(extendedPrice, TaxBag.VAT);
            product.SalesTax += GetValue(extendedPrice, TaxBag.SalesTax);
        }

        // Set GST, HST, PST, VAT, SalesTax to shopping cart for product.
        private void SetTaxesValueToShoppingCart(ZnodeProductBaseEntity product)
        {
            ShoppingCart.GST += product.GST;
            ShoppingCart.HST += product.HST;
            ShoppingCart.PST += product.PST;
            ShoppingCart.VAT += product.VAT;
            ShoppingCart.SalesTax += product.SalesTax;
        }

        // Set GST, HST, PST, VAT, SalesTax to shipping in shopping cart for product.
        private void SetShippingInTaxesValueOfShoppingCart(decimal shippingCost)
        {
            ShoppingCart.GST += GetValue(shippingCost, TaxBag.GST);
            ShoppingCart.HST += GetValue(shippingCost, TaxBag.HST);
            ShoppingCart.PST += GetValue(shippingCost, TaxBag.PST);
            ShoppingCart.VAT += GetValue(shippingCost, TaxBag.VAT);
            ShoppingCart.SalesTax += GetValue(shippingCost, TaxBag.SalesTax);

            SetTaxOnShipping(shippingCost);
        }

        private void SetTaxOnShipping(decimal shippingCost)
        {
            ShoppingCart.TaxOnShipping += (GetValue(shippingCost, TaxBag.GST)
                                           + GetValue(shippingCost, TaxBag.HST)
                                           + GetValue(shippingCost, TaxBag.PST)
                                           + GetValue(shippingCost, TaxBag.VAT)
                                           + GetValue(shippingCost, TaxBag.SalesTax));
        }

        // Calculate tax for group product.
        private void CalculateTaxForAddonProduct(ZnodeGenericCollection<ZnodeProductBaseEntity> productCollection, ZnodeShoppingCartItem cartItem)
        {
            foreach (ZnodeProductBaseEntity productItem in productCollection)
            {
                decimal extendedPrice = GetVarientProductPrice(productItem, cartItem);
                ApplyTax(productItem, extendedPrice);
            }
        }

        // Calculate tax for group product.
        private void CalculateTaxForGroupProduct(ZnodeGenericCollection<ZnodeProductBaseEntity> productCollection, ZnodeShoppingCartItem cartItem)
        {
            foreach (ZnodeProductBaseEntity productItem in productCollection)
            {
                decimal extendedPrice = GetGroupProductPrice(productItem, cartItem);
                ApplyTax(productItem, extendedPrice);
            }
        }

        // Calculate tax for Configurable product.
        private void CalculateTaxForConfigurableProduct(ZnodeGenericCollection<ZnodeProductBaseEntity> productCollection, ZnodeShoppingCartItem cartItem)
        {
            foreach (ZnodeProductBaseEntity productItem in productCollection)
            {
                decimal extendedPrice = GetProductPrice(cartItem);

                if (extendedPrice == 0)
                {
                    extendedPrice = GetVarientProductPrice(productItem, cartItem);
                }

                ApplyTax(productItem, extendedPrice);
            }
        }

        //to apply tax to product
        private void ApplyTax(ZnodeProductBaseEntity productItem, decimal extendedPrice)
        {
            bool taxCalculated = false;
            if (Equals(productItem.TaxClassID, TaxBag.TaxClassId) && productItem.TaxCalculated == false)
            {
                GetTaxesValue(extendedPrice, productItem);
                SetTaxesValueToShoppingCart(productItem);
                taxCalculated = true;
                productItem.TaxCalculated = true;
            }
            else if (!taxCalculated && IsDefaultTaxApplicable(productItem.TaxClassID) && productItem.TaxCalculated == false)
            {
                GetTaxesValue(extendedPrice, productItem);
                SetTaxesValueToShoppingCart(productItem);
                productItem.TaxCalculated = true;
            }
        }

        //to check store's default tax applicable for the current product
        private bool IsDefaultTaxApplicable(int productTaxClassId)
        {
            return TaxBag.IsDefault == true && (productTaxClassId == 0 || !TaxBag.AssociatedTaxRuleIds.Contains(productTaxClassId.ToString()));
        }

        #endregion Private Method
    }
}
