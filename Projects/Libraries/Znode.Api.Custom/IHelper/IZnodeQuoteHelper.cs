﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Znode.Custom.Data;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Custom.IHelper
{
    public interface IZnodeQuoteHelper
    {
        /// <summary>
        /// to get quote by omsQuoteId
        /// </summary>
        /// <param name="omsQuoteId"></param>
        /// <returns>ZnodeOmsQuote</returns>
        ZnodeOmsQuote GetQuoteById(int omsQuoteId);

        /// <summary>
        /// To get quote line items by omsQuoteId
        /// </summary>
        /// <param name="omsQuoteId"></param>
        /// <returns>List of ZnodeOmsQuoteLineItem</returns>
        List<Libraries.Data.DataModel.ZnodeOmsQuoteLineItem> GetQuoteLineItemByQuoteId(int omsQuoteId);

    }
}
