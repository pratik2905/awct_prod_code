﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Cache;
using Znode.Sample.Api.Model.Cart;

namespace Znode.Api.Custom.Cache.ICache
{
   public interface IAWCTPublishProductCache 
    {
        string GetConfigurableProductViewModel(int publishProductId, string routeUri, string routeTemplate);

        string GetPriceSizeList(int configurableProductId, string routeUri, string routeTemplate);
        string GetGlobalAttributeData(string globalAttributeCodes, string routeUri, string routeTemplate);

        string GetPublishProduct(int publishProductId, string routeUri, string routeTemplate);
        string GetExtendedProductDetails(int publishProductId, string routeUri, string routeTemplate);

        //string GetEstimatedShipDateData(string ProductIdliststr, string routeUri, string routeTemplate);
    }
}
