﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Cart;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Cache.Cache
{
    public class AWCTPublishProductCache : BaseCache,IAWCTPublishProductCache
    {
        #region Private Variable
        private readonly IAWCTPublishProductService _service;
        #endregion

        #region Constructor
        public AWCTPublishProductCache(IAWCTPublishProductService publishProductService) //:base(publishProductService)
        {
            _service = publishProductService;
        }
        #endregion

        public string GetConfigurableProductViewModel(int publishProductId,string routeUri, string routeTemplate)
        {
            //Get data from Cache
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response
                ConfigurableProductViewModel configurableProduct = null;// _service.GetConfigurableProductViewModel(publishProductId, Filters, Expands);
                if (configurableProduct != null)
                {
                    AWCTPublishProductResponse response = new AWCTPublishProductResponse { ConfigurableProductViewModel = configurableProduct };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetPriceSizeList(int configurableProductId, string routeUri, string routeTemplate)
        {
            //Get data from Cache
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response
                ConfigurableProductViewModel configurableProduct = _service.GetPriceSizeList(configurableProductId);
                if (configurableProduct != null)
                {
                    AWCTPublishProductResponse response = new AWCTPublishProductResponse { ConfigurableProductViewModel = configurableProduct };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        public string GetGlobalAttributeData(string globalAttributeCodes, string routeUri, string routeTemplate)
        {
            //Get data from Cache
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response
                AWCTTruColorModel truColorList = _service.GetGlobalAttributeData(globalAttributeCodes);
                if (truColorList != null)
                {
                    AWCTPublishProductResponse response = new AWCTPublishProductResponse { AWCTTruColorModel= truColorList };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public virtual string GetPublishProduct(int publishProductId, string routeUri, string routeTemplate)
        {
            //Get data from Cache
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response
                AWCTPublishProductModel publishProduct = _service.GetPublishProduct(publishProductId, Filters, Expands);
                if (publishProduct != null)
                {
                    AWCTPublishProductResponse response = new AWCTPublishProductResponse { AWCTPublishProduct = publishProduct };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        public virtual string GetExtendedProductDetails(int publishProductId, string routeUri, string routeTemplate)
        {
            //Get data from Cache
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response
                AWCTPublishProductModel publishProduct = _service.GetExtendedProductDetails(publishProductId, Filters, Expands);
                if (publishProduct != null)
                {
                    AWCTPublishProductResponse response = new AWCTPublishProductResponse { AWCTPublishProduct = publishProduct };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        //public string GetEstimatedShipDateData(string ProductIdliststr, string routeUri, string routeTemplate)
        //{
        //    //Get data from Cache
        //    string data = GetFromCache(routeUri);
        //    if (string.IsNullOrEmpty(data))
        //    {
        //        //Create Response
        //        AWCTCartModel aWCTcartModel = _service.GetEstimatedShipDateData(ProductIdliststr);
        //        if (aWCTcartModel != null)
        //        {
        //            AWCTPublishProductResponse response = new AWCTPublishProductResponse { AWCTCartModel = aWCTcartModel };
        //            data = InsertIntoCache(routeUri, routeTemplate, response);
        //        }
        //    }
        //    return data;
        //}

    }
}
