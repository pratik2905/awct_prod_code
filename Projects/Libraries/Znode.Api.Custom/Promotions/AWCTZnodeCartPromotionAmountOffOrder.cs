﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Promotions
{
    public class AWCTZnodeCartPromotionAmountOffOrder : ZnodeCartPromotionType
    {
        
        #region Constructor
        public AWCTZnodeCartPromotionAmountOffOrder()
        {
            Name = "AWCT Amount Off Order";
            Description = "Applies an amount off an entire order; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the amount off an order.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            decimal itemCount = GetCartItemsCount();
            if (itemCount <= 0)
                return;

            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);

            OrderBy = nameof(PromotionModel.OrderMinimum);
            ApplicablePromolist =GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));

            if (Equals(PromotionBag.Coupons, null))
            {
                if (PromotionBag.MinimumOrderAmount <= subTotal && itemCount > 0)
                {
                    List<PromotionModel> Promotionlist = ZnodePromotionHelper.GetMostApplicablePromoList(ApplicablePromolist, subTotal, false);
                    if (!ZnodePromotionHelper.IsApplicablePromotion(Promotionlist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                        return;

                    ApplyDiscount(PromotionBag.Discount);
                    ShoppingCart.IsAnyPromotionApplied = true;
                }
            }
            else if (PromotionBag?.Coupons?.Count > 0)
            {
                bool isCouponValid = ValidateCoupon(couponIndex);
                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        bool status = true;
                        if (PromotionBag.IsUnique)
                        {
                            status = IsExistingOrderContainsCoupon(coupon.Code);
                        }
                        if (status)
                        {
                            // Apply the discount
                            if (PromotionBag.MinimumOrderAmount <= subTotal && isCouponValid && itemCount > 0)
                            {
                                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, true))
                                    return;

                                ApplyDiscount(PromotionBag.Discount, coupon.Code);
                                SetCouponApplied(coupon.Code);
                                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                            }
                        }
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
                AddPromotionMessage(couponIndex.Value);
            }
        }
        #endregion

        #region Private Methods
        //to get all items count in cart
        private decimal GetCartItemsCount()
        {
            decimal count = 0;

            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                decimal lineItemCount = 0;

                if (cartItem.Product.FinalPrice > 0 && (cartItem?.Product?.ZNodeGroupProductCollection?.Count == 0))
                {
                    lineItemCount += 1;
                }

                foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                {
                    if (addon.FinalPrice > 0.0M)
                        lineItemCount++;
                }

                if (cartItem?.Product?.ZNodeGroupProductCollection?.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                    {
                        if (group.FinalPrice > 0.0M)
                            lineItemCount += group.SelectedQuantity;
                    }
                }
                else
                {
                    lineItemCount = (lineItemCount * cartItem.Quantity);
                }

                count += (lineItemCount);
            }
            return count;
        }

        //apply  calculated discount to shopping cart item
        private void ApplyDiscount(decimal lineItemDiscount, string couponCode = "")
        {
            ShoppingCart.OrderLevelDiscount += lineItemDiscount;
            ShoppingCart.OrderLevelDiscountDetails = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), lineItemDiscount, GetDiscountType(couponCode), ShoppingCart.OrderLevelDiscountDetails);

            bool isCalculateTaxAfterDiscount = Convert.ToBoolean(ZnodeApiSettings.CalculateTaxAfterDiscount);
            if (isCalculateTaxAfterDiscount)
            {
                decimal itemCount = ShoppingCart.ShoppingCartItems.Count();
                decimal discountAmount = itemCount > 0 ? (lineItemDiscount / itemCount) : lineItemDiscount;
                foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    AddOrderDiscountAmount(cartItem.Product, discountAmount);

                    if (cartItem.PromotionalPrice == 0 && cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                    {
                        foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                        {
                            AddOrderDiscountAmount(addon, discountAmount);
                        }
                    }

                    if (cartItem.PromotionalPrice == 0 && cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                    {
                        foreach (ZnodeProductBaseEntity config in cartItem.Product.ZNodeConfigurableProductCollection)
                        {
                            AddOrderDiscountAmount(config, discountAmount);
                        }
                    }

                    if (cartItem.PromotionalPrice == 0 && cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                    {
                        foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                        {
                            AddOrderDiscountAmount(group, discountAmount);
                        }
                    }
                }
            }

        }

        // To add order level discount Amount 
        private void AddOrderDiscountAmount(ZnodeProductBaseEntity product, decimal discountAmount)
        {
            if (product.FinalPrice > 0.0M)
            {
                product.OrderDiscountAmount += discountAmount;

            }
        }
        //to remove discount from shopping cart
        private void RemoveDiscount(decimal lineItemDiscount)
        {
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.DiscountAmount > lineItemDiscount)
                    cartItem.Product.DiscountAmount -= lineItemDiscount;

                cartItem.Product.DiscountAmount = (cartItem.Product.DiscountAmount < 0) ? 0 : cartItem.Product.DiscountAmount;

                foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                {
                    if (addon.FinalPrice > 0.0M && addon.DiscountAmount > lineItemDiscount)
                        addon.DiscountAmount -= lineItemDiscount;
                }

                foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                {
                    if (group.FinalPrice > 0.0M && group.DiscountAmount > lineItemDiscount)
                        group.DiscountAmount -= lineItemDiscount;
                }

            }
        }

        internal static List<PromotionModel> GetPromotionsByType(string promotionsType, string promotionName, List<PromotionModel> allPromotions, string orderBy = "QuantityMinimum", int portalId = 0)
        {

            List<PromotionModel> promotions = allPromotions.Where(promo => (DateTime.Today.Date >= promo.StartDate && DateTime.Today.Date <= promo.EndDate)
                                                                            && promo.PromotionType.ClassType.Equals(promotionsType, StringComparison.OrdinalIgnoreCase)
                                                                            && promo.PromotionType.ClassName.Equals(promotionName)
                                                                            && promo.PromotionType.IsActive && (promo.PortalId == portalId || promo.PortalId == null))
                                                           .OrderByDescending(x => typeof(PromotionModel).GetProperty(orderBy).GetValue(x, null))
                                                           .ToList();

            return promotions ?? new List<PromotionModel>();
        }
        #endregion

        #region IsCouponPerCustomer
        //to check coupon is applied to exisiting order thats quantity already deducted
        public bool IsExistingOrderContainsCoupon(string couponcode)
        {
            if (!string.IsNullOrEmpty(couponcode))
            {
                bool status = IsCouponAppliedonExistingOrder(couponcode);
                return !status;
            }
            return false;
        }
        //to get login userid from header
        private int GetLoginUserId()
        {
            const string headerUserId = "Znode-UserId";
            System.Collections.Specialized.NameValueCollection headers = HttpContext.Current.Request.Headers;
            int.TryParse(headers[headerUserId], out int userId);
            return userId;
        }
        //to check applied coupon is used in exiting order by OrderId
        public bool IsCouponAppliedonExistingOrder(string couponcode)
        {
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("@UserID", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Couponcode", couponcode, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Status", null, ParameterDirection.Output, DbType.Int32);
            IList<View_ReturnBoolean> result = objStoredProc.ExecuteStoredProcedureList("Znode_CouponExistInUserOrder @UserID, @Couponcode, @Status OUT", 2, out int status);
            return Convert.ToBoolean(result.FirstOrDefault().Status);
            //if (result.FirstOrDefault().Id.Equals(0))
            //{
            //    return true;
            //}
            //else
            //{
            //    //result.FirstOrDefault().Status.Value;
            //    return false;
            //}
        }
        #endregion
    }
}
    
