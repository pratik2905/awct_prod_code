﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Helper
{
    public class AWCTPublishProductHelper : PublishProductHelper
    {
        private readonly IMongoRepository<SeoEntity> _seoMongoRepository;
        public AWCTPublishProductHelper()
        {
            _seoMongoRepository = new MongoRepository<SeoEntity>();
        }
        public override void GetProductCustomerReviews(PublishProductModel publishProduct, int portalId)
        {
            string sku = publishProduct.ConfigurableProductId > 0 ? publishProduct.ConfigurableProductSKU : publishProduct.SKU;
            //Get Promotions Associated to Publish Product Id
            publishProduct.ProductReviews = GetProductReviews(portalId, sku);

            //Get Product average rating from total product reviews.
            if (publishProduct?.ProductReviews?.Count > 0)
                publishProduct.Rating = Math.Round((decimal)publishProduct.ProductReviews.Sum(x => x.Rating) / publishProduct.ProductReviews.Count, 2);

        }

        //Get product seo and product reviews
        public override void GetProductsSEOAndReviews(int portalId, List<SearchProductModel> productList, int localeId, int catalogVersionId)
        {
            List<string> SKUs = productList?.Select(y => y.SKU).Distinct().ToList();
            List<SeoEntity> publishSEOList = GetPublishSEODetailsForList(portalId, ZnodeConstant.Product, localeId, SKUs, catalogVersionId);
            //Map Product Data.
            MapSearchProductData(productList, publishSEOList);
        }
        private List<SeoEntity> GetPublishSEODetailsForList(int portalId, string seoType, int localeId, List<string> SKUs, int? catalogVersionId = null)
        {
            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            mongoQuery.Add(Query.And(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId),
                               Query<SeoEntity>.EQ(pr => pr.SEOTypeName, seoType),
                               Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId)));

            ////Nivi Code as PortalId check remove.
            //mongoQuery.Add(Query.And(Query<SeoEntity>.EQ(pr => pr.SEOTypeName, seoType),
            //                  Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId)));

            if (catalogVersionId.HasValue && catalogVersionId.Value > 0)
                mongoQuery.Add(Query<SeoEntity>.EQ(pr => pr.VersionId, catalogVersionId.Value));
            if (SKUs.Count > 0)
                mongoQuery.Add(Query<SeoEntity>.In(pr => pr.SEOCode, SKUs));

            List<SeoEntity> publishSEOList = _seoMongoRepository.GetEntityList(Query.And(mongoQuery));
            return publishSEOList;
        }
        //Map product data.
        protected override void MapSearchProductData(List<SearchProductModel> searchProducts, List<SeoEntity> details)
        {
            //foreach(SeoEntity s in details)
            //{
            //    ZnodeLogging.LogMessage("SEO Code" + s.SEOCode, "SEO", System.Diagnostics.TraceLevel.Error);
            //}
            if (searchProducts?.Count > 0)
            {
                searchProducts.ForEach(product =>
                {
                    SeoEntity productDetails = details
                                .FirstOrDefault(productdata => productdata.SEOCode == product.SKU);
                    if (HelperUtility.IsNotNull(productDetails))
                    {
                        product.SEODescription = productDetails.SEODescription;
                        product.SEOKeywords = productDetails.SEOKeywords;
                        product.SEOTitle = productDetails.SEOTitle;
                        product.SEOUrl = productDetails.SEOUrl;
                    }
                });
            }
        }

       
    }
}
