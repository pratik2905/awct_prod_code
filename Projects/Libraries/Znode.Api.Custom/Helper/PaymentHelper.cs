﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.IHelper;
using Znode.Api.Custom.Maps;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model.Quote;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.Helper
{
    public class PaymentHelper : BaseHelper,IPaymentHelper
    {
        #region Private Variables
        private readonly IPaymentClient _paymentClient;
        #endregion

        public PaymentHelper(IPaymentClient paymentClient)
        {
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
        }

        #region Public Methods
        public virtual bool ProcessPayment(ConvertQuoteToOrderModel model, ShoppingCartModel cartModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.API.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter ConvertQuoteToOrderModel having CustomerPaymentId and CustomerProfileId", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { model.PaymentDetails.CustomerPaymentId, model.PaymentDetails.CustomerProfileId });

            if (HelperUtility.IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.PaymentSettingModelNotNull);

            bool isSuccess = false;

            // Condition for "Credit Card" payment.
            if (Equals(model.PaymentDetails.PaymentType, ZnodeConstant.CreditCard.ToLower()))
            {
                if (!string.IsNullOrEmpty(model.PaymentDetails.CustomerPaymentId) || (!string.IsNullOrEmpty(model.PaymentDetails.CustomerProfileId)))
                {
                    if (ProcessCreditCardPayment(model, cartModel))
                    {
                        isSuccess = true;
                    }
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.InvalidData, Api_Resources.ErrorCustomerPaymentIdRequired);//profile id req message
                }

                if (!cartModel.IsGatewayPreAuthorize && !string.IsNullOrEmpty(cartModel.Token))
                {
                    isSuccess = CapturePayment(cartModel.Token);
                }

            }


            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return isSuccess;
        }

        #endregion

        #region Protected Methods
        // Process credit card payment.
        protected virtual bool ProcessCreditCardPayment(ConvertQuoteToOrderModel convertToOrderModel, ShoppingCartModel cartModel)
        {
            SetUsersPaymentDetails(convertToOrderModel.PaymentDetails.PaymentSettingId, cartModel);
            convertToOrderModel.PaymentDetails.PaymentType = cartModel?.Payment?.PaymentName;
            GatewayResponseModel gatewayResponse = GetPaymentResponse(cartModel, convertToOrderModel);
            if (gatewayResponse?.HasError ?? true || string.IsNullOrEmpty(gatewayResponse?.Token))
            {
                return false;
            }

            //Map payment token
            cartModel.Token = gatewayResponse.Token;
            cartModel.IsGatewayPreAuthorize = gatewayResponse.IsGatewayPreAuthorize;

            return true;
        }

        //Capture Payment
        private bool CapturePayment(string paymentTransactionToken)
        {
            try
            {
                BooleanModel booleanModel = _paymentClient.CapturePayment(paymentTransactionToken);
                return !booleanModel.HasError;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return false;
        }


        // Get payment response.
        protected virtual GatewayResponseModel GetPaymentResponse(ShoppingCartModel cartModel, ConvertQuoteToOrderModel convertToOrderModel)
        {
            // Map shopping Cart model and submit Payment view model to Submit payment model 
            SubmitPaymentModel model = PaymentViewModelMap.ToModel(cartModel, convertToOrderModel);

           // Map Customer Payment Guid for Save Credit Card

           //Nivi Code COmmneted..Will do later
            if (!string.IsNullOrEmpty(convertToOrderModel.PaymentDetails.CustomerGuid) && string.IsNullOrEmpty(cartModel.UserDetails.CustomerPaymentGUID))
            {
                IUserService userService = GetService<IUserService>();
                NameValueCollection expands = new NameValueCollection();
                UserModel userModel = userService.GetUserById(convertToOrderModel.UserId, expands);
                userModel.CustomerPaymentGUID = convertToOrderModel.PaymentDetails.CustomerGuid;
                userService.UpdateCustomer(userModel);
            }

            model.Total = ConvertTotalToLocale(Convert.ToString(cartModel.Total));

            return ProcessPayNow(model);
        }

        // Call PayNow method in Payment Application
        protected virtual GatewayResponseModel ProcessPayNow(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.PayNow(model);
            return new GatewayResponseModel { HasError = true, };
        }

        #endregion

        #region Private Methods
        //to convert total amount to locale wise
        private string ConvertTotalToLocale(string total)
            => total.Replace(",", ".");

        private void SetUsersPaymentDetails(int paymentSettingId, ShoppingCartModel model) //, bool isRequiredExpand = false)
        {
            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodePaymentSettingEnum.ZnodePaymentType.ToString(), ZnodePaymentSettingEnum.ZnodePaymentType.ToString());

            IPaymentSettingService _paymentSettingService = GetService<IPaymentSettingService>();

            PaymentSettingModel paymentSetting = _paymentSettingService.GetPaymentSetting(paymentSettingId, expands, model.PortalId);

            string paymentName = string.Empty;
            if (HelperUtility.IsNotNull(paymentSetting))
            {
                paymentName = paymentSetting.PaymentTypeName;
            }

            model.Payment = PaymentViewModelMap.ToPaymentModel(model, paymentSetting, paymentName);

        }

        //bool IPaymentHelper.ProcessPayment(ConvertQuoteToOrderModel convertToOrderModel, ShoppingCartModel cartModel)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion
    }
}
