﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class AWCTZnodeOrderHelper : ZnodeOrderHelper, IZnodeOrderHelper
    {
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDiscount> _omsOrderDiscountRepository;


        public AWCTZnodeOrderHelper(int? webstoreVersionId = null, int? catalogVersionId = null)
        {
            
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _omsOrderDiscountRepository = new ZnodeRepository<ZnodeOmsOrderDiscount>();

        }

        public override int SaveOrderLineItem(OrderLineItemModel orderLineItem)
        {
            int userId = HelperMethods.GetLoginAdminUserId();
            orderLineItem.CreatedBy = userId > 0 ? userId : orderLineItem.CreatedBy;
            orderLineItem.ModifiedBy = userId > 0 ? userId : orderLineItem.ModifiedBy;
            ZnodeOmsOrderLineItem lineItems = _orderLineItemRepository.Insert(orderLineItem.ToEntity<ZnodeOmsOrderLineItem>());

            orderLineItem.OmsOrderLineItemsId = lineItems.OmsOrderLineItemsId;

            //to save order line item partial refund amount
            SavePartialDiscount(lineItems);

            //to save order attributes
            SaveOrderAttributes(orderLineItem);

            if (orderLineItem?.OrderLineItemCollection?.Count > 0)
            {
                orderLineItem.OrderLineItemCollection = orderLineItem.OrderLineItemCollection
                                                        .OrderByDescending(m => m.OrderLineItemRelationshipTypeId).ToList();
                foreach (OrderLineItemModel childLineItems in orderLineItem.OrderLineItemCollection)
                {
                    childLineItems.OmsOrderDetailsId = orderLineItem.OmsOrderDetailsId;
                    BindParentOmsOrderLineItemId(orderLineItem, childLineItems);
                    childLineItems.OrderLineItemStateId = orderLineItem.OrderLineItemStateId;
                    childLineItems.Description = childLineItems.Description == string.Empty ? orderLineItem.Description : childLineItems.Description;
                    ZnodeOmsOrderLineItem childLineItem = _orderLineItemRepository.Insert(childLineItems.ToEntity<ZnodeOmsOrderLineItem>());
                    childLineItems.OmsOrderLineItemsId = childLineItem.OmsOrderLineItemsId;
                    //to save order attributes
                    SaveOrderAttributes(childLineItems);
                    //to save order line item partial refund amount
                    SavePartialDiscount(childLineItem);
                }
            }

            //Save tax order line item.
            SaveTaxOrderLine(orderLineItem);

            //Save Personalise attributes
            SavePersonalizeItem(orderLineItem.OmsOrderLineItemsId, orderLineItem);

            List<OrderLineItemAdditionalCostModel> orderLineAdditionalCostList = new List<OrderLineItemAdditionalCostModel>();
            //Save additional cost per order line item
            if (orderLineItem.AdditionalCost?.Count > 0)
            {
                foreach (KeyValuePair<string, decimal> additionalCost in orderLineItem?.AdditionalCost)
                {
                    OrderLineItemAdditionalCostModel orderLineAdditionalCost = new OrderLineItemAdditionalCostModel();
                    orderLineAdditionalCost.OmsOrderLineItemsId = orderLineItem.OmsOrderLineItemsId;
                    orderLineAdditionalCost.KeyName = additionalCost.Key;
                    orderLineAdditionalCost.KeyValue = additionalCost.Value;
                    orderLineAdditionalCostList.Add(orderLineAdditionalCost);
                }
                IZnodeRepository<ZnodeOmsOrderLineItemsAdditionalCost> _znodeOrderLineAdditionalCostRepository = new ZnodeRepository<ZnodeOmsOrderLineItemsAdditionalCost>();
                _znodeOrderLineAdditionalCostRepository.Insert(orderLineAdditionalCostList.ToEntity<ZnodeOmsOrderLineItemsAdditionalCost>());
            }

            return orderLineItem.OmsOrderLineItemsId;
        }
        protected override void SavePartialDiscount(ZnodeOmsOrderLineItem lineItem)
        {
            if (HelperUtility.IsNotNull(lineItem?.PartialRefundAmount) && lineItem?.Quantity > 0)
            {
                ZnodeOmsOrderDiscount discount = new ZnodeOmsOrderDiscount();
                discount.OmsOrderLineItemId = lineItem.OmsOrderLineItemsId;
                discount.OmsDiscountTypeId = (int)OrderDiscountTypeEnum.PARTIALREFUND;
                discount.DiscountCode = lineItem.ProductName;
                discount.DiscountAmount = lineItem.PartialRefundAmount;
                ZnodeOmsOrderDiscount orderDiscount = _omsOrderDiscountRepository.Insert(discount);
            }
        }
        public override bool SaveOrderDiscount(List<OrderDiscountModel> orderDiscount)
        {
            IEnumerable<ZnodeOmsOrderDiscount> omsOrderDiscounts = _omsOrderDiscountRepository.Insert(orderDiscount.ToEntity<ZnodeOmsOrderDiscount>());
            return omsOrderDiscounts?.Count() > 0;
        }
        ////To save/update SavedCartlineItem data in database
        //public override bool SaveAllCartLineItemsInDatabase(int savedCartId, AddToCartModel shoppingCart)
        //{
        //    if (savedCartId > 0 && !Equals(shoppingCart, null))
        //    {
        //        List<SavedCartLineItemModel> savedCartLineItem = new List<SavedCartLineItemModel>();

        //        //Check if Shopping cart and cart items are null. If not then add it ro SavedCartLineItemModel.
        //        if ((HelperUtility.IsNotNull(shoppingCart.ShoppingCartItems)))
        //        {
        //            //Bind AddOns, Bundle product skus if associated rto cart items.
        //            foreach (var item in shoppingCart.ShoppingCartItems)
        //                BindShoppingCartItemModel(item, shoppingCart.PublishedCatalogId, shoppingCart.LocaleId);

        //            int sequence = 0;
        //            foreach (ShoppingCartItemModel cartitem in shoppingCart.ShoppingCartItems)
        //            {
        //                ZnodeLogging.LogMessage("SAVEALLCARTLINEITEMSINDATABASE" + cartitem.ProductName + " = " + cartitem.Quantity, "CART", System.Diagnostics.TraceLevel.Error);
        //                sequence++;
        //                savedCartLineItem.Add(BindSaveCartLineItem(Convert.ToString(cartitem?.OmsOrderLineItemsId), savedCartId, cartitem, shoppingCart.PublishedCatalogId, shoppingCart.LocaleId)); //, sequence));
        //            }
        //        }
        //        string savedCartLineItemXML = HelperUtility.ToXML(savedCartLineItem);
        //        ZnodeLogging.LogMessage("SAVEDCARTLINEITEMXML =" + savedCartLineItemXML, "CART", System.Diagnostics.TraceLevel.Error);

        //        shoppingCart.UserId = HelperUtility.IsNull(shoppingCart.UserId) ? 0 : shoppingCart.UserId;

        //        //SP call to save/update savedCartLineItem
        //        IZnodeViewRepository<SavedCartLineItemModel> objStoredProc = new ZnodeViewRepository<SavedCartLineItemModel>();
        //        objStoredProc.SetParameter("CartLineItemXML", savedCartLineItemXML, ParameterDirection.Input, DbType.String);
        //        objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), shoppingCart.UserId, ParameterDirection.Input, DbType.Int32);
        //        objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
        //        int status = 0;
        //        List<SavedCartLineItemModel> savedCartLineItemList = objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateSaveCartLineItemQuantity @CartLineItemXML, @UserId ,@Status OUT", 2, out status).ToList();

        //        return status == 1;
        //    }
        //    return false;
        //}
    }
}
