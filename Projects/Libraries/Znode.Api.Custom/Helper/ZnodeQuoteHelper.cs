﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.IHelper;
//using Znode.Custom.Data;
using Znode.Libraries.Data;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Custom.Helper
{
    public class ZnodeQuoteHelper : IZnodeQuoteHelper
    {

        #region Private Variables

        private readonly IZnodeRepository<ZnodeOmsQuote> _quoteRepository;
        private readonly IZnodeRepository<Libraries.Data.DataModel.ZnodeOmsQuoteLineItem> _quoteLineItemRepository;
        #endregion Private Variables

        #region Public Constructor

        public ZnodeQuoteHelper()
        {
            _quoteRepository = new ZnodeRepository<ZnodeOmsQuote>();
            _quoteLineItemRepository = new ZnodeRepository<Libraries.Data.DataModel.ZnodeOmsQuoteLineItem>();
        }

        #endregion Public Constructor
        //to get quote by quoteId
        public virtual ZnodeOmsQuote GetQuoteById(int omsQuoteId)
        {
            if (omsQuoteId <= 0)
                return null;
            else
                return _quoteRepository.Table.FirstOrDefault(x => x.OmsQuoteId == omsQuoteId);
        }

        //to get quote line items by omsQuoteId
        public virtual List<Libraries.Data.DataModel.ZnodeOmsQuoteLineItem> GetQuoteLineItemByQuoteId(int omsQuoteId)
        {

            FilterDataCollection filter = new FilterDataCollection();
            filter.Add(new FilterDataTuple(ZnodeOmsQuoteLineItemEnum.OmsQuoteId.ToString(), FilterOperators.Equals, omsQuoteId.ToString()));
            string whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter)?.WhereClause;
            return _quoteLineItemRepository.GetEntityList(whereClause)?.ToList();
        }
    }
}
