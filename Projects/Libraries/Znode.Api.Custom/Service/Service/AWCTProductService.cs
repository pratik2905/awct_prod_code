﻿using System.Data;
using System.Diagnostics;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTProductService : ProductService
    {
        public override DataSet GetXmlProductsDataSet(FilterCollection filters, PageListModel pageListModel, string pimProductIdsNotIn, ref bool pimProductIdsIn)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            int localeId = CategoryService.GetLocaleId(filters);
            pimProductIdsIn = GetIsProductNotInValue(filters, pimProductIdsIn);
            bool isCatalogFilter = false;
            //Get catalog filter values from FilterCollection.
            int pimCatalogId = ServiceHelper.GetCatalogFilterValues(filters, ref isCatalogFilter);

            bool isCallForAttribute = GetIsCallForAttribute(filters);
            string whereClause = GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());
            whereClause = whereClause.Replace("= false", "= 'false'");
            whereClause = whereClause.Replace("= true", "= 'true'");
            string attributeCode = GetAttributeCodes(filters);
            ZnodeLogging.LogMessage("whereClause and attributeCode to set SP parameters:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { whereClause, attributeCode });

            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();

            objStoredProc.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PimProductId", pimProductIdsNotIn, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@IsProductNotIn", pimProductIdsIn, ParameterDirection.Input, SqlDbType.Bit);
            objStoredProc.GetParameter("@IsCallForAttribute", isCallForAttribute, ParameterDirection.Input, SqlDbType.Bit);
            objStoredProc.GetParameter("@AttributeCode", attributeCode, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@PimCatalogId", pimCatalogId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@IsCatalogFilter", isCatalogFilter, ParameterDirection.Input, SqlDbType.Bit);

            var ds = objStoredProc.GetSPResultInDataSet("Znode_ManageProductList_XML");
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return ds;
        }
    }
}
