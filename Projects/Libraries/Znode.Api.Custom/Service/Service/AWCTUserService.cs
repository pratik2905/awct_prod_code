﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTUserService : UserService
    {
        #region Private Variables

        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private static readonly IZnodeRepository<AspNetZnodeUser> _aspuserRepository = new ZnodeRepository<AspNetZnodeUser>();
        private static readonly IZnodeRepository<AspNetUser> _aspNetUserRepository = new ZnodeRepository<AspNetUser>();
        private static readonly IZnodeRepository<ZnodeUserPortal> _znodeUserPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
        private static IZnodeRepository<ZnodePortalAccount> _znodeAccountPortalRepository = new ZnodeRepository<ZnodePortalAccount>();
        private static readonly IZnodeRepository<ZnodeUser> _ZnodeUserRepository = new ZnodeRepository<ZnodeUser>();
        #endregion

        #region Public Constructors
        public AWCTUserService() : base()
        {
            _userRepository = new ZnodeRepository<ZnodeUser>();
        }
        #endregion

        public override UserModel Login(int portalId, UserModel model, out int? errorCode, NameValueCollection expand = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            errorCode = null;
            //Validate the Login user, and return the user details.
            Engine.Services.ApplicationUser user = GetAspNetUserDetails(ref model, portalId);

            //This method is used to login the User having valid username and password.
            SignInStatus result = SignInManager.PasswordSignIn(user.UserName, model.User.Password, model.User.RememberMe, true);

            if (Equals(result, SignInStatus.Success))
            {
                //Check Password is expired or not.
                ZnodeUserAccountBase.CheckLastPasswordChangeDate(user.Id, out errorCode);

                CheckUserPortalAccessForLogin(model.PortalId.GetValueOrDefault(), (_userRepository.Table.FirstOrDefault(x => x.AspNetUserId == user.Id)?.UserId).GetValueOrDefault());

                //Bind user details required after login.
                return BindDetails(model, user, expand);
            }

            //Throw the Exceptions for Invalid Login.
            InvalidLoginError(user);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }
       
        private void InvalidLoginError(Engine.Services.ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, user?.Id);
            if (!Equals(user, null))
            {
                if (UserManager.IsLockedOut(user.Id))
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                    throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                }
                else
                {
                    //Gets current failed password atttemt count for setting the message.
                    int inValidAttemtCount = user.AccessFailedCount;

                    //Gets Maximum failed password atttemt count from web.config
                    int maxInvalidPasswordAttemptCount = UserManager.MaxFailedAccessAttemptsBeforeLockout;
                    ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { inValidAttemtCount = inValidAttemtCount, maxInvalidPasswordAttemptCount = maxInvalidPasswordAttemptCount });

                    if (inValidAttemtCount > 0 && maxInvalidPasswordAttemptCount > 0)
                    {
                        if (maxInvalidPasswordAttemptCount <= inValidAttemtCount)
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }

                        //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                        if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 2))
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.TwoAttemptsToAccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }
                        else if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 1))
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.OneAttemptToAccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }
                        else
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                        throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                    }
                }
            }
            else
            {
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
            }
        }
        //Get the Login User Details
        private Engine.Services.ApplicationUser GetAspNetUserDetails(ref UserModel model, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);
            }

            if (IsNull(model.User))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);
            }

            //Get the User Details from Znode Mapper
            AspNetZnodeUser znodeUser = GetUserInfo(model.User.Username, (model?.PortalId > 0) ? Convert.ToInt32(model.PortalId) : portalId);
            if (IsNull(znodeUser))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.ErrorUserNotExist, model.User.Username));
            }

            model.UserName = string.IsNullOrEmpty(znodeUser.UserName) ? model.UserName : znodeUser.UserName;

            //Bind the Aspnet Znode User Id.
            model.AspNetZnodeUserId = znodeUser.AspNetZnodeUserId;

            //Get the Login User Details from Owin Mapper.
            Engine.Services.ApplicationUser user = UserManager.FindByName(znodeUser.AspNetZnodeUserId);
            if (IsNull(user))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.ErrorUserExist, model.User.Username));
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return user;
        }
        public override UserModel ChangePassword(int portalId, UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);
            }

            if (IsNull(model.User))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);
            }

            //Get the AspNet Znode user.
            AspNetZnodeUser znodeUser = GetUserInfo(model.User.Username, (model.PortalId > 0) ? model.PortalId.Value : portalId);

            if (IsNull(znodeUser))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNameUnavailable, model.UserName));
            }

            /*NIVI CODE for Resetting password via impersonation and logged in user without Old Password*/
            if (model.User.Password == null)
            {
                Engine.Services.ApplicationUser updateuser = UserManager.FindByNameAsync(znodeUser.AspNetZnodeUserId).Result;
                UpdateOldPassword(updateuser.Id);
                model.User.Password = "admin12345";
            }

            //This method is used to find the User having valid username.
            Engine.Services.ApplicationUser user = UserManager.FindByNameAsync(znodeUser.AspNetZnodeUserId).Result;

            if (IsNull(user))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, "User " + model.User.Username + " not found.");
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return PasswordVerification((model.PortalId > 0) ? model.PortalId.Value : portalId, model, user);
        }
        //Password varification.
        private UserModel PasswordVerification(int portalId, UserModel model, Engine.Services.ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            bool isResetPassword = !string.IsNullOrEmpty(model.User.PasswordToken);

            //This method is used to verify user.
            if (!isResetPassword && SignInManager.HasBeenVerified())
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorCurrentPassword);
            }

            //This method is used to verify the reset password token is valid or not.
            if (isResetPassword && !UserManager.VerifyUserToken(user.Id, "ResetPassword", model.User.PasswordToken))
            {
                throw new ZnodeException(ErrorCodes.ProcessingFailed, Admin_Resources.ResetPasswordLinkExpired);
            }

            bool verified = (isResetPassword) ? true : ZnodeUserAccountBase.VerifyNewPassword(user.Id, model.User.NewPassword);

            if (verified)
            {
                // Update/Reset the password for this user
                IdentityResult result = (isResetPassword)
                    //This method is used to reset the password having valid user Id, password token and new password.
                    ? UserManager.ResetPassword(user.Id, model.User.PasswordToken, model.User.NewPassword)

                    //This method is used to change the password having valid user Id, old password and new password.
                    : UserManager.ChangePassword(user.Id, model.User.Password, model.User.NewPassword);
                if (result.Succeeded)
                {
                    // Log password
                    SetRequestHeaderForPasswordLog(user.Id, 0);
                    ZnodeUserAccountBase.LogPassword(user.Id, model.User.NewPassword);
                    model.User.Password = model.User.NewPassword;

                    //Save recently password change date.
                    ZnodeUserAccountBase.SetPasswordChangedDate(user.Id);
                    return Login(portalId, model, out int? errorCode);
                }
                throw new ZnodeException(ErrorCodes.InvalidData, result.Errors.FirstOrDefault());
            }
            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.EnterNewPassword);
        }
        /// <summary>
        /// Set user id in Header specifally in case of new user created from webstore or password reset through link send on mail
        /// </summary>
        /// <param name="AspNetUserId"> Parameter used to get userId from AspNetUserId</param>
        private void SetRequestHeaderForPasswordLog(string AspNetUserId = null, int userId = 0)
        {
            if (HelperMethods.GetLoginUserId() <= 0)
            {
                if (!string.IsNullOrEmpty(AspNetUserId))
                {
                    userId = _userRepository.Table.Where(x => x.AspNetUserId == AspNetUserId).Select(x => x.UserId).FirstOrDefault();
                }
                if (userId != null && userId > 0)
                {
                    HttpContext.Current.Request.Headers.Add("Znode-UserId", Convert.ToString(userId));
                }
            }
        }
        //Get the Znode User Details.
        public static AspNetZnodeUser GetUserInfo(string username, int? portalId)
        {
            if (!string.IsNullOrEmpty(username))
            {
                var availableUsers = (from userPortal in _znodeUserPortalRepository.Table
                                      join user in _ZnodeUserRepository.Table on userPortal.UserId equals user.UserId
                                      join aspNetUser in _aspNetUserRepository.Table on user.AspNetUserId equals aspNetUser.Id
                                      join aspnetZnodeUser in _aspuserRepository.Table on aspNetUser.UserName equals aspnetZnodeUser.AspNetZnodeUserId
                                      where (aspnetZnodeUser.UserName == username || aspNetUser.Email == username) && (aspnetZnodeUser.PortalId == portalId || aspnetZnodeUser.PortalId == null || portalId == null)
                                      select new
                                      {
                                          AspNetUserId = user.AspNetUserId,
                                          PortalId = aspnetZnodeUser.PortalId,
                                          AspNetZnodeUserId = aspnetZnodeUser.AspNetZnodeUserId
                                      }).ToList();

                if (availableUsers?.Count == 1)
                {
                    //Select the only user which is having portal access.
                    var selectedUser = availableUsers.FirstOrDefault(x => x.PortalId == portalId || x.PortalId == null || portalId == null);
                    if (HelperUtility.IsNotNull(selectedUser))
                    {
                        return _aspuserRepository.Table.FirstOrDefault(x => x.AspNetZnodeUserId == selectedUser.AspNetZnodeUserId);
                    }
                }
                if (availableUsers?.Count > 0)
                {
                    throw new ZnodeException(ErrorCodes.IsUsed, WebStore_Resources.ErrorMultipleRecordsFound);
                }
            }
            return null;
        }
        //Verify reset password link status.
        public override int? VerifyResetPasswordLinkStatus(int portalId, UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);
            }

            //Get the AspNet Znode user.
            AspNetZnodeUser aspnetZnodeUser = GetUserInfo(model.User.Username, (model.PortalId > 0) ? model.PortalId.Value : portalId);

            if (IsNull(aspnetZnodeUser))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ProvideValidUserName);
            }

            int? statusCode = null;

            //Get the application user.
            Engine.Services.ApplicationUser user = UserManager.FindByName(aspnetZnodeUser.AspNetZnodeUserId);
            if (IsNotNull(user))
            {
                //This method is used to verify the reset password token is valid or not.
                statusCode = UserManager.VerifyUserToken(user.Id, "ResetPassword", model.User.PasswordToken) ? ErrorCodes.ResetPasswordContinue : ErrorCodes.ResetPasswordLinkExpired;
            }

            ZnodeLogging.LogMessage("Output Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { statusCode = statusCode });

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return statusCode;
        }
        //Single reset password functionality.
        public override UserModel ForgotPassword(int portalId, UserModel model, bool isUserCreateFromAdmin = false, bool isAdminUser = false)
        {
            ZnodeLogging.LogMessage("Execution Started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);
            }

            if (IsNull(model.User))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);
            }

            portalId = (model.PortalId > 0) ? model.PortalId.Value : PortalId;
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //Get the AspNet Znode user.
            AspNetZnodeUser znodeUser = GetUserInfo(model.User.Username, portalId);

            if (IsNull(znodeUser))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNameUnavailable, model.User));
            }

            //This method is used to find the User having valid username.
            Engine.Services.ApplicationUser user = UserManager.FindByNameAsync(znodeUser.AspNetZnodeUserId).Result;

            if (IsNull(user))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotFound, model.User));
            }

            //This method is used to verify the userwith password stored in database.
            if (UserManager.IsLockedOut(user.Id))
            {
                throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);
            }

            if (portalId < 1)
            {
                // Get the portal id.
                SetAssignedPortal(model, true);
                if (model.PortalId < 1)
                {
                    model.PortalId = PortalId;
                }
            }
            else
            {
                model.PortalId = portalId;
            }

            //Reset Password Link in Email - Start
            SendResetPassword(model, user, isUserCreateFromAdmin, isAdminUser);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return model;
        }
        //Set Parameters for the Email Templates to be get replaced.
        private static void SetParameterOfEmailTemplate(string firstName, string resetUrl, string resetLink, string storeLogo)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName });

            EmailTemplateHelper.SetParameter("#FirstName#", firstName);
            EmailTemplateHelper.SetParameter("#UserName#", firstName);
            EmailTemplateHelper.SetParameter("#Link#", resetLink);
            EmailTemplateHelper.SetParameter("#Url#", resetUrl);
            EmailTemplateHelper.SetParameter("#StoreLogo#", storeLogo);
        }

        //Generate Reset Password Email Content based on the Email Template
        private string GenerateResetPasswordEmail(string firstName, string resetUrl, string resetLink, int portalId, out string subject, out bool isHtml, out bool isEnableBcc, int localeId = 0, bool isUserCreateFromAdmin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName, portalId = portalId });

            // Send email to the user
            string messageText = string.Empty;
            subject = "Reset Password Notification";
            isHtml = false;
            isEnableBcc = false;
            //Method to get Email Template Details.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode((isUserCreateFromAdmin ? "NewUserAccount" : "ResetPassword"), portalId, localeId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                string storeLogo = GetCustomPortalDetails(portalId)?.StoreLogo;
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info, new { storeLogo = storeLogo });

                //Set Parameters for the Email Templates to be get replaced.
                SetParameterOfEmailTemplate(firstName, resetUrl, resetLink, storeLogo);

                //Replace the Email Template Keys, based on the passed email template parameters.
                messageText = EmailTemplateHelper.ReplaceTemplateTokens(emailTemplateMapperModel.Descriptions);
                subject = emailTemplateMapperModel.Subject;
                isHtml = true;
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return messageText;
        }

        //Sends the reset password.
        private void SendResetPassword(UserModel model, Engine.Services.ApplicationUser user, bool isUserCreateFromAdmin = false, bool isAdminUser = false)
        {
            int DanceStore = 7;
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //This method is used to generate password reset token.
            string passwordResetToken = UserManager.GeneratePasswordResetToken(user.Id);
            string baseUrl = string.IsNullOrEmpty(model.BaseUrl) ? ZnodeApiSettings.AdminWebsiteUrl : model.BaseUrl;
            if (isUserCreateFromAdmin && isAdminUser != true)
            {
                //Get Portal details as well as the Domain Url based on the WEbStore Application Type.
                PortalModel portalModel = GetCustomPortalDetails(model.PortalId.Value);
                baseUrl = (IsNotNull(portalModel)) ? $"{HttpContext.Current.Request.Url.Scheme}://{portalModel.DomainUrl}" : ZnodeApiSettings.AdminWebsiteUrl;
                /*Nivi Change*/
                //baseUrl = (IsNull(portalModel)) ? $"{HttpContext.Current.Request.Url.Scheme}://{portalModel.DomainUrl}" : ZnodeApiSettings.AdminWebsiteUrl;
            }
           
           /*Nivi change done by kunal for admin url*/
            if (model.PortalId == DanceStore)
            {
                baseUrl = Convert.ToString(ConfigurationManager.AppSettings["ZnodeWebStoreUri"]);
            }
            else
            {
                //**ZnodeGuardUri for Guard Store**//
                baseUrl = Convert.ToString(ConfigurationManager.AppSettings["ZnodeGuardUri"]);
            }
       
            string passwordResetUrl = $"{baseUrl}/User/ResetPassword?passwordToken={WebUtility.UrlEncode(passwordResetToken)}&userName={WebUtility.UrlEncode(model.User.Username)}";
            string subject = string.Empty;
            bool isHtml = false;
            bool isEnableBcc = false;
            //Generate Reset Password Email Content.

            try
            {
                string messageText = GenerateResetPasswordEmail(string.IsNullOrEmpty(model.UserName) ? model.User.Username : model.UserName, passwordResetUrl, $"<a href=\"{passwordResetUrl}\"> here</a>", model.PortalId.Value, out subject, out isHtml, out isEnableBcc, model.LocaleId, isUserCreateFromAdmin);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info, new { messageText = messageText });

                if (!string.IsNullOrEmpty(messageText))
                {
                    //This method is used to send an email.
                    if (model.PortalId.Value > 0)
                    {
                        ZnodeEmail.SendEmail(model.PortalId.Value, user.Email, ZnodeEmailBase.SMTPUserName, ZnodeEmail.GetBccEmail(isEnableBcc, model.PortalId.Value, string.Empty), subject, messageText, isHtml);
                    }
                    else
                    {
                        ZnodeEmail.SendEmail(user.Email, ZnodeEmailBase.SMTPUserName, ZnodeEmail.GetBccEmail(isEnableBcc, model.PortalId.Value, string.Empty), subject, messageText, isHtml);
                    }
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.EmailTemplateDoesNotExists, Admin_Resources.ErrorResetPasswordLinkReset);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Warning);
                model.IsEmailSentFailed = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                model.IsEmailSentFailed = true;
                throw new ZnodeException(ErrorCodes.ErrorSendResetPasswordLink, Admin_Resources.ErrorSendResetPasswordLink);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }
        //Get the assigned portal id for user.
        private void SetAssignedPortal(UserModel userModel, bool isCustomerEditMode)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Check for userId greater than 0 and sent form customer edit mode.
            if (userModel?.UserId > 0 && isCustomerEditMode)
            {
                SetPortal(userModel);
            }

            if (userModel?.UserId < 1 && !isCustomerEditMode && userModel.PortalIds.Any())
            {
                userModel.PortalId = Convert.ToInt32(userModel.PortalIds.FirstOrDefault());
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }
        //Sets the portal ids.
        private void SetPortal(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //If the user belongs to the account then fetch the account's portal Id otherwise user portal id.
            if (userModel.AccountId > 0)
            {
                userModel.PortalId = GetAccountPortalId(Convert.ToInt32(userModel.AccountId));
            }
            else
            {
                userModel.PortalIds = _znodeUserPortalRepository.Table.Where(x => x.UserId == userModel.UserId)?.Select(x => x.PortalId.ToString())?.ToArray();
                userModel.PortalId = string.IsNullOrEmpty(userModel.PortalIds?.FirstOrDefault()) ? (int?)null : Convert.ToInt32(userModel.PortalIds?.FirstOrDefault());
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }
        //Get the account's portal Id.
        private int? GetAccountPortalId(int accountId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { accountId = accountId });

            IZnodeRepository<ZnodePortalAccount> _portalAccountRepository = new ZnodeRepository<ZnodePortalAccount>();
            return _portalAccountRepository.Table.FirstOrDefault(x => x.AccountId == accountId)?.PortalId;
        }
        //Get the User Account Details by its UserName
        public override UserModel GetUserByUsername(string username, int portalId, bool isSocialLogin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { username = username, portalId = portalId });

            if (string.IsNullOrEmpty(username))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserNameNotEmpty);
            }

            //Get the AspNet Znode user.
            AspNetZnodeUser aspnetZnodeUser = GetUserInfo(username, portalId);

            if (IsNull(aspnetZnodeUser))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotPresent, username));
            }

            Engine.Services.ApplicationUser user = UserManager.FindByName(aspnetZnodeUser.AspNetZnodeUserId);

            if (IsNull(user))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotFound, username));
            }

            //This method is used to get the account details.
            ZnodeUser znodeuser = _userRepository.GetEntity(FilterClauseForUserId(user).WhereClause, FilterClauseForUserId(user).FilterValues);

            //Map ZnodeUser Entity to User Model.
            UserModel model = UserMap.ToModel(znodeuser, user.Email);
            if (IsNotNull(model.User) && IsNotNull(user.Roles) && user.Roles.Count > 0)
            {
                model.User.RoleId = user.Roles?.Select(s => s.RoleId)?.FirstOrDefault();
            }

            //check whether the current login user has Customer user and B2B role or not.
            model.IsAdminUser = IsAdminUser(user);

            NameValueCollection expands = new NameValueCollection();
            expands.Add(FilterKeys.Profiles, FilterKeys.Profiles);

            BindUserDetails(model, expands);

            //Bind user details required after login.
            model = BindDetails(model, user, expands);

            model.UserName = aspnetZnodeUser.UserName;

            //Throw the Exceptions for Invalid social Login.
            if (isSocialLogin && UserManager.IsLockedOut(user.Id))
            {
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }
        ///*Nivi Code to add the user from admin users.*/
        public override UserModel CreateAdminUser(UserModel model)
        {
            model.User.Username = model.FullName;
            model.FullName = null;
            return base.CreateAdminUser(model);
        }

        private void UpdateOldPassword(string userId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(HelperMethods.ConnectionString);
                SqlCommand cmd = new SqlCommand("AWCT_UpdateOldPassword", conn);
                cmd.CommandTimeout = 30000;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);

            }
        }
    }
}
