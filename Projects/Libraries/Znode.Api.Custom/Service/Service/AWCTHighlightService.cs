﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.Service.Service
{
    public class AWCTHighlightService : HighlightService
    {
        private readonly IZnodeRepository<ZnodePublishProductDetail> _publishProductDetailRepository;
        private readonly IPublishProductHelper publishProductHelper;
        public AWCTHighlightService()
        {
            _publishProductDetailRepository = new ZnodeRepository<ZnodePublishProductDetail>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
        }
        //Get paged Highlight list.
        //Get Highlight by highlightId.
        public override HighlightModel GetHighlight(int highlightId, int productId, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("highlightId and productId:", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new { highlightId = highlightId, productId = productId });

            if (highlightId > 0)
            {
                //Generate where clause.
                FilterCollection filter = new FilterCollection();
                filter.Add(new FilterTuple(ZnodeHighlightEnum.HighlightId.ToString(), FilterOperators.Is, Convert.ToString(highlightId)));

                //Get filtervalue of SKU
                string sku = GetFilterValue(filters, FilterKeys.SKU);

                string whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseForSP(filter.ToFilterDataCollection());
                ZnodeLogging.LogMessage("WhereClause generated to set SP parameters :", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, whereClauseModel);

                int localeId = GetLocaleId(filters);
                //SP call
                IZnodeViewRepository<HighlightModel> objStoredProc = new ZnodeViewRepository<HighlightModel>();
                objStoredProc.SetParameter("@WhereClause", whereClauseModel, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter("@Rows", 10, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@PageNo", 1, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@Order_By", "", ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter("@RowCount", 1, ParameterDirection.Output, DbType.Int32);
                objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
                HighlightModel highlight = objStoredProc.ExecuteStoredProcedureList("Znode_GetHighlightDetail @WhereClause,@Rows,@PageNo,@Order_By,@RowCount,@LocaleId")?.FirstOrDefault();

                if (HelperUtility.IsNotNull(highlight) && productId > 0)
                {
                    filter = new FilterCollection { new FilterTuple(ZnodePublishProductEnum.PublishProductId.ToString(), FilterOperators.Equals, productId.ToString()) };
                    string productSku = _publishProductDetailRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause)?.SKU;
                    if (!string.IsNullOrEmpty(highlight.Description))
                    {
                        ZnodeLogging.LogMessage("GetHighlightDescription with parameters :", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new object[] { productId, localeId, highlight });
                        //Commented as it gives error for Item key
                        //highlight.Description = GetHighlightDescription(productId, localeId, highlight, sku);
                        highlight.Description = EmailTemplateHelper.ReplaceTemplateTokens(highlight.Description);
                    }
                    highlight.SEOUrl = GetService<ISEOService>().GetProductSeoData(GetLocaleId(filters), productId, PortalId, ZnodeConstant.Product, sku)?.SEOUrl;
                }

                return highlight;
            }
            return null;
        }
        //Get the locale id from filters.
        private static int GetLocaleId(FilterCollection filters)
        {
            int localeId = 0;
            if (filters?.Count > 0)
            {
                Int32.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);
                filters.RemoveAll(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase));
            }
            return localeId;
        }
        //Get Highlight Description by replacing attributes tokens.
        private string GetHighlightDescription(int productId, int localeId, HighlightModel highlight, string sku = "")
        {
            ProductEntity productDetails;
            if (!string.IsNullOrEmpty(sku))
            {
                productDetails = publishProductHelper.GetPublishProductBySKU(sku, GetPortalCatalogId(PortalId).GetValueOrDefault(), localeId, GetCatalogVersionId(0, localeId, PortalId));
            }
            else
            {
                productDetails = publishProductHelper.GetPublishProduct(productId, PortalId, localeId, GetCatalogVersionId(0, localeId, PortalId));
            }

            if (HelperUtility.IsNotNull(productDetails))
            {
                foreach (AttributeEntity item in productDetails.Attributes.Distinct())
                {
                    EmailTemplateHelper.SetParameter("#" + item.AttributeCode + "#", item.AttributeValues);
                }
            }

            return EmailTemplateHelper.ReplaceTemplateTokens(highlight.Description);
        }
        //Gets the specified key's value of the filter.
        private static string GetFilterValue(FilterCollection filters, string key)
        {
            string value = filters.Find(x => string.Equals(x.FilterName, key, StringComparison.CurrentCultureIgnoreCase))?.FilterValue;
            filters.RemoveAll(x => string.Equals(x.FilterName, key, StringComparison.CurrentCultureIgnoreCase));
            return value;
        }
    }
}
