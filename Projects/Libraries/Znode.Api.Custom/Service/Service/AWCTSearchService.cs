﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.ElasticSearch;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Libraries.Search;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTSearchService : SearchService
    {
        #region Private Variables.

        private PublishProductHelper publishProductHelper = new PublishProductHelper();
        private readonly IZnodeRepository<ZnodeCatalogIndex> _catalogIndexRepository;
        private readonly IZnodeRepository<ZnodeSearchIndexMonitor> _searchIndexMonitorRepository;

        public AWCTSearchService() : base()
        {
            _catalogIndexRepository = new ZnodeRepository<ZnodeCatalogIndex>();
            _searchIndexMonitorRepository = new ZnodeRepository<ZnodeSearchIndexMonitor>();
        }
        #endregion

        #region Constructor

        #endregion

        #region Elastic search
        //Get search results.
        public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            filters.Add("hidefromsearch", FilterOperators.Equals, ZnodeConstant.FalseValue);
            GetParametersValueForFilters(filters, out int portalId, out int catalogId, out int localeId);
            expands.Remove(ZnodeConstant.Pricing);

            sorts.Add("DisplayOrder", "-1");
            KeywordSearchModel searchResult = base.FullTextSearch(model, expands, filters, sorts, page);
            if (searchResult.Products != null)
            {
                AWCTAttributeSwatchHelper attributeSwatchHelper = new AWCTAttributeSwatchHelper();
                attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "awctColorName");
            }

            #region Commented Code swatch code
            //ImageHelper image = new ImageHelper(portalId);
            //string ProductImageName = "";

            //string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);

            //string _imagePath = "";
            //string _swatchImagePath = "";


            //int index = 0;

            ////ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //index = ImageSmallPath.LastIndexOf('/');
            //if (index != -1)
            //    _imagePath = ImageSmallPath.Substring(0, index) + "/";

            ////OriginalImagepath = http://localhost:44762/Data/Media/
            //index = OriginalImagepath.LastIndexOf('/');
            //if (index != -1)
            //{
            //    _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //    _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //}
            //if (searchResult.Products != null)
            //{
            //    List<WebStoreGroupProductModel> simage = GetSwatchImages(String.Join(",", searchResult.Products.Select(x => x.ZnodeProductId).ToList()), _imagePath, _swatchImagePath, "PLP");
            //    foreach (SearchProductModel prd in searchResult.Products)
            //    {

            //        if (prd.AssociatedGroupProducts == null)
            //            prd.AssociatedGroupProducts = new List<WebStoreGroupProductModel>();
            //        prd.AssociatedGroupProducts = simage.Where(x => x.PublishProductId == prd.ZnodeProductId).Select
            //            (t => new WebStoreGroupProductModel()
            //            {
            //                ImageMediumPath = image.GetImageHttpPathSmall(t.ImageMediumPath),
            //                ImageThumbNailPath = t.ImageThumbNailPath
            //            }).ToList<WebStoreGroupProductModel>();
            //    }
            //}
            #endregion

            if (searchResult.Products != null)
            {
                List<string> HighlightProductIds = new List<string>();
                searchResult.Products.ForEach(product =>
                {
                    HighlightProductIds.Add(product.ZnodeProductId.ToString());
                });
                try
                {
                    searchResult = AssociateHighLightDataToSearchResult(searchResult, string.Join(",", HighlightProductIds));
                }
                catch (Exception ex)
                {

                    ZnodeLogging.LogMessage("AssociateHighLightDataToSearchResult" + ex.StackTrace, "Custom1", System.Diagnostics.TraceLevel.Error);
                }
            }

            return searchResult;

        }

        #endregion

        #region To Check Index fails
        ////Inserts data for creating index.
        //public override PortalIndexModel InsertCreateIndexData(PortalIndexModel portalIndexModel)
        //{
        //    try
        //    {
        //        ZnodeLogging.LogMessage("InsertCreateIndexData--Add", "Custom1", System.Diagnostics.TraceLevel.Error);
        //        if (IsNull(portalIndexModel))
        //        {
        //            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorPortalIndexModelNull);
        //        }

        //        ZnodeSearchIndexMonitor searchIndexMonitor;

        //        SearchHelper searchHelper = new SearchHelper();

        //        int searchIndexServerStatusId = 0;

        //        string catalogName = portalIndexModel.CatalogName;

        //        //Allow to insert only if data does not exists.
        //        if (portalIndexModel.CatalogIndexId < 1)
        //        {
        //            //Check if index name is already used by another store.
        //            string indexName = _catalogIndexRepository.Table.Where(x => x.IndexName == portalIndexModel.IndexName).Select(s => s.IndexName)?.FirstOrDefault() ?? string.Empty;

        //            if (!string.IsNullOrEmpty(indexName) || new LoadDefaultData().IsIndexExists(portalIndexModel.IndexName))
        //            {
        //                throw new ZnodeException(ErrorCodes.DuplicateSearchIndexName, Admin_Resources.ErrorIndexNameIsInUse);
        //            }

        //            portalIndexModel.CatalogName = catalogName;
        //            string revisionType = portalIndexModel.RevisionType;
        //            //Save index name in database.
        //            portalIndexModel = _catalogIndexRepository.Insert(portalIndexModel.ToEntity<ZnodeCatalogIndex>())?.ToModel<PortalIndexModel>();
        //            portalIndexModel.RevisionType = revisionType;
        //            catalogName = portalIndexModel.CatalogName;
        //            //Create index monitor entry.
        //            searchIndexMonitor = SearchIndexMonitorInsert(portalIndexModel);
        //            ZnodeLogging.LogMessage("SearchIndexMonitorId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchIndexMonitor?.SearchIndexMonitorId);
        //            ZnodeLogging.LogMessage("InsertCreateIndexData--Step1", "Custom1", System.Diagnostics.TraceLevel.Error);
        //            if (portalIndexModel?.CatalogIndexId > 0 && searchIndexMonitor.SearchIndexMonitorId > 0)
        //            {
        //                portalIndexModel.SearchCreateIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId;

        //                ZnodeLogging.LogMessage(Admin_Resources.SuccessSearchIndexCreate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

        //                //Start status for creating index for server name saved.   

        //                searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
        //                {
        //                    SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
        //                    ServerName = Environment.MachineName,
        //                    Status = ZnodeConstant.SearchIndexStartedStatus
        //                }).SearchIndexServerStatusId;

        //                CallSearchIndexer(portalIndexModel, searchIndexMonitor.CreatedBy, searchIndexServerStatusId);
        //                portalIndexModel.StoreName = catalogName;
        //                return portalIndexModel;
        //            }

        //            ZnodeLogging.LogMessage(string.Format(Admin_Resources.ErrorCreatingLogForIndexCreationForPortalId, portalIndexModel.PortalId), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //        }
        //        else
        //        {
        //            searchIndexMonitor = CreateSearchIndexMonitorEntry(portalIndexModel);
        //        }

        //        //Start status for creating index for server name saved.  
        //        searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
        //        {
        //            SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
        //            ServerName = Environment.MachineName,
        //            Status = ZnodeConstant.SearchIndexStartedStatus
        //        }).SearchIndexServerStatusId;


        //        portalIndexModel.SearchCreateIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId;
        //        CallSearchIndexer(portalIndexModel, searchIndexMonitor.CreatedBy, searchIndexServerStatusId);
        //        ZnodeLogging.LogMessage("PortalIndexModel with PortalIndexId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, portalIndexModel?.PortalIndexId);
        //        ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

        //        ZnodeLogging.LogMessage("InsertCreateIndexData--Done", "Custom1", System.Diagnostics.TraceLevel.Error);
        //        return portalIndexModel;

        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("InsertCreateIndexData" + ex.StackTrace, "Custom1", System.Diagnostics.TraceLevel.Error);
        //        return null;
        //    }
        //}
        ////Calls application to create Index.
        //private void CallSearchIndexer(PortalIndexModel portalIndexModel, int userId, int searchIndexServerStatusId)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("userId and searchIndexServerStatusId to call search indexer: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { userId, searchIndexServerStatusId });
        //    ZnodeLogging.LogMessage(string.Format(Admin_Resources.SearchIndexerCalled, portalIndexModel.IndexName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    portalIndexModel.RevisionType = String.IsNullOrEmpty(portalIndexModel.RevisionType) ? "PRODUCTION" : portalIndexModel.RevisionType;
        //    ProcessStartInfo info = new ProcessStartInfo();
        //    string schedulerPath = GetSchedulerPath();
        //    info.FileName = !string.IsNullOrEmpty(schedulerPath) && File.Exists(schedulerPath) ? schedulerPath : System.Web.Hosting.HostingEnvironment.MapPath(ZnodeConstant.schedulerPath);


        //    string tokenValue = string.IsNullOrEmpty(HttpContext.Current.Request.Headers["Token"]) ? "0" : HttpContext.Current.Request.Headers["Token"];
        //    string apiDomainUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/";

        //    info.Arguments = $"Indexer {portalIndexModel.PublishCatalogId} {portalIndexModel.IndexName}  {portalIndexModel.CatalogIndexId} {portalIndexModel.SearchCreateIndexMonitorId} Manually {apiDomainUrl} {userId} {searchIndexServerStatusId} {portalIndexModel.RevisionType} {HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")}  {tokenValue} {ZnodeApiSettings.RequestTimeout}";
        //    info.UseShellExecute = false;
        //    ZnodeLogging.LogMessage($"Arguments Passed : {info.Arguments}", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    Process process = Process.Start(info);
        //    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    process.WaitForExit();
        //}
        //private ZnodeSearchIndexMonitor CreateSearchIndexMonitorEntry(PortalIndexModel portalIndexModel)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("PortalIndexModel with PortalIndexId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, portalIndexModel?.PortalIndexId);
        //    //Get Catalog index data by id.
        //    PortalIndexModel getCatalogIndexDetail = (from aa in _catalogIndexRepository.Table
        //                                              where aa.CatalogIndexId == portalIndexModel.CatalogIndexId
        //                                              select new PortalIndexModel()
        //                                              {
        //                                                  CatalogIndexId = portalIndexModel.CatalogIndexId,
        //                                                  PublishCatalogId = aa.PublishCatalogId,
        //                                                  IndexName = aa.IndexName,

        //                                              }).FirstOrDefault();
        //    //Check if same as earlier
        //    if (getCatalogIndexDetail.IndexName == portalIndexModel.IndexName)
        //    {
        //        return SearchIndexMonitorInsert(portalIndexModel);
        //    }
        //    else
        //    {
        //        //Check if index name is already used by another store.
        //        string indexName = _catalogIndexRepository.Table.Where(x => x.IndexName == portalIndexModel.IndexName).Select(s => s.IndexName)?.FirstOrDefault() ?? string.Empty;

        //        if (!string.IsNullOrEmpty(indexName) || new LoadDefaultData().IsIndexExists(portalIndexModel.IndexName))
        //        {
        //            throw new ZnodeException(ErrorCodes.DuplicateSearchIndexName, Admin_Resources.ErrorIndexNameIsInUse);
        //        }

        //        bool renameStatus = RenameIndex(portalIndexModel.CatalogIndexId, getCatalogIndexDetail.IndexName, portalIndexModel.IndexName);
        //        if (renameStatus)
        //        {
        //            _catalogIndexRepository.Update(new ZnodeCatalogIndex { CatalogIndexId = getCatalogIndexDetail.CatalogIndexId, PublishCatalogId = getCatalogIndexDetail.PublishCatalogId, IndexName = portalIndexModel.IndexName });
        //        }
        //        else
        //        {
        //            ZnodeSearchIndexMonitor searchIndexMonitor;
        //            searchIndexMonitor = SearchIndexMonitorInsert(portalIndexModel);
        //            SearchHelper searchHelper = new SearchHelper();
        //            int searchIndexServerStatusId = 0;
        //            searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
        //            {
        //                SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
        //                ServerName = Environment.MachineName,
        //                Status = ZnodeConstant.SearchIndexStartedStatus
        //            }).SearchIndexServerStatusId;
        //            ZnodeLogging.LogMessage("InsertCreateIndexData--Step2", "Custom1", System.Diagnostics.TraceLevel.Error);
        //            CreateIndex(portalIndexModel.IndexName, portalIndexModel.RevisionType, portalIndexModel.CatalogIndexId, searchIndexMonitor.SearchIndexMonitorId, searchIndexServerStatusId);
        //            ZnodeLogging.LogMessage("InsertCreateIndexData--Step3", "Custom1", System.Diagnostics.TraceLevel.Error);
        //            RenameIndex(portalIndexModel.CatalogIndexId, getCatalogIndexDetail.IndexName, portalIndexModel.IndexName);
        //            _catalogIndexRepository.Update(new ZnodeCatalogIndex { CatalogIndexId = getCatalogIndexDetail.CatalogIndexId, PublishCatalogId = getCatalogIndexDetail.PublishCatalogId, IndexName = portalIndexModel.IndexName });
        //        }
        //        return SearchIndexMonitorInsert(portalIndexModel);
        //    }
        //}
        ////Insert into ZnodeSearch indexMonitor.
        //private ZnodeSearchIndexMonitor SearchIndexMonitorInsert(PortalIndexModel portalIndexModel)
        //{
        //    return _searchIndexMonitorRepository.Insert(new ZnodeSearchIndexMonitor()
        //    {
        //        SourceId = 0,
        //        CatalogIndexId = portalIndexModel.CatalogIndexId,
        //        SourceType = "CreateIndex",
        //        SourceTransactionType = "INSERT",
        //        AffectedType = "CreateIndex",
        //        CreatedBy = portalIndexModel.CreatedBy,
        //        CreatedDate = portalIndexModel.CreatedDate,
        //        ModifiedBy = portalIndexModel.ModifiedBy,
        //        ModifiedDate = portalIndexModel.ModifiedDate
        //    });
        //}
        ////Create search index  
        //public override void CreateIndex(string indexName, string revisionType, int catalogId, int searchIndexMonitorId, int searchIndexServerStatusId)
        //{
        //    ZnodeLogging.LogMessage("InsertCreateIndexData--Step211", "Custom1", System.Diagnostics.TraceLevel.Error);
        //    ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage(string.Format(Admin_Resources.IndexingStarted, indexName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("revisionType, catalogId, searchIndexMonitorId, searchIndexServerStatusId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { revisionType, catalogId, searchIndexMonitorId, searchIndexServerStatusId });
        //    try
        //    {
        //        long indexstartTime = DateTime.Now.Ticks;

        //        new LoadDefaultData().IndexingDefaultData(indexName, new SearchParameterModel() { CatalogId = catalogId, IndexStartTime = indexstartTime, SearchIndexMonitorId = searchIndexMonitorId, SearchIndexServerStatusId = searchIndexServerStatusId, revisionType = revisionType, ActiveLocales = GetActiveLocaleList() });
        //        ZnodeLogging.LogMessage("InsertCreateIndexData--Step2112", "Custom1", System.Diagnostics.TraceLevel.Error);
        //        ZnodeLogging.LogMessage(string.Format(Admin_Resources.IndexingStarted, indexName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //        //Delete PIM deleted products from search index.
        //        DeleteProductData(indexName, revisionType, indexstartTime);
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("InsertCreateIndexData--Step211" + ex.Message, "Custom1", System.Diagnostics.TraceLevel.Error);
        //        SearchHelper searchHelper = new SearchHelper();
        //        searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchIndexServerStatusId, SearchIndexMonitorId = searchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexFailedStatus });
        //        ZnodeLogging.LogMessage(string.Format(Admin_Resources.ErrorIndexingForIndex, indexName, ex.Message), ZnodeLogging.Components.Search.ToString(), TraceLevel.Error, ex);
        //    }
        //    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //}
        #endregion
        
        private KeywordSearchModel AssociateHighLightDataToSearchResult(KeywordSearchModel searchResult, string highlightProductIdList)
        {
            IZnodeViewRepository<AssociatedProductsModel> objStoredProc = new ZnodeViewRepository<AssociatedProductsModel>();

            objStoredProc.SetParameter("@PublishedProductList", highlightProductIdList, ParameterDirection.Input, DbType.String);

            List<AssociatedProductsModel> list = objStoredProc.ExecuteStoredProcedureList("AWCT_GetHighLightDetails @PublishedProductList").ToList();
            list?.ForEach(x =>
            {
                SearchProductModel prd = searchResult.Products.FirstOrDefault(y => y.ZnodeProductId == x.PublishProductId);
                if (prd != null)
                {
                    prd.AssociatedProducts = prd.AssociatedProducts == null ? new List<AssociatedProductsModel>() : prd.AssociatedProducts;
                    prd.AssociatedProducts.Add(x);
                }

            });

            return searchResult;
        }
        private static void GetParametersValueForFilters(FilterCollection filters, out int portalId, out int catalogId, out int localeId)
        {
            int.TryParse(filters.Where(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out portalId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.ZnodeCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out catalogId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out localeId);
        }
        private List<WebStoreGroupProductModel> GetSwatchImages(string PublishedProductId, string ImagePath, string SwatchPath, string PageName)
        {
            IZnodeViewRepository<WebStoreGroupProductModel> objStoredProc = new ZnodeViewRepository<WebStoreGroupProductModel>();

            objStoredProc.SetParameter("@PublishedProductList", PublishedProductId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@ImagePath", ImagePath, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SwatchPath", SwatchPath, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Case", PageName, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("AWCT_GetSwatchImages @PublishedProductList,@ImagePath,@SwatchPath,@Case").ToList();

        }
    }
}
