﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTPIMAttributeService : PIMAttributeService
    {
        private readonly IZnodeRepository<ZnodePimAttributeValidation> _validationRepository;
        private readonly IZnodeRepository<ZnodePimAttributeDefaultValue> _defaultValueRepository;


        public AWCTPIMAttributeService()
        {
            //Initialisation of PIM Repositories.
            _validationRepository = new ZnodeRepository<ZnodePimAttributeValidation>();
            _defaultValueRepository = new ZnodeRepository<ZnodePimAttributeDefaultValue>();

        }
        public override PIMAttributeDefaultValueListModel GetDefaultValues(int attributeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter attributeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, attributeId);

            _validationRepository.EnableDisableLazyLoading = true;
            PIMAttributeDefaultValueListModel model;
            model = PIMAttributesMap.ToDefaultValueListModel(_defaultValueRepository.GetEntityList(GetWhereClauseForPIMAttributeId(attributeId).WhereClause));
            /*Nivi Code start*/
            model.DefaultValues = model.DefaultValues.OrderBy(x => x.DisplayOrder).ToList();
            /*Nivi Code end*/
            foreach (var item in model.DefaultValues)
            {
                item.MediaPath = item.MediaId > 0 ? GetMediaPath(item) : string.Empty;
            }
            ZnodeLogging.LogMessage("DefaultValues list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, model?.DefaultValues?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return model;
        }

        private string GetMediaPath(PIMAttributeDefaultValueModel model)
        {
            IMediaManagerServices mediaService = GetService<IMediaManagerServices>();
            MediaManagerModel mediaData = mediaService.GetMediaByID(Convert.ToInt32(model?.MediaId), null);
            ZnodeLogging.LogMessage("mediaData generated to get by method GetMediaByID: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, mediaData);
            return model.MediaPath = IsNotNull(mediaData) ? mediaData.MediaServerThumbnailPath : ZnodeAdminSettings.DefaultImagePath;
        }
        private EntityWhereClauseModel GetWhereClauseForPIMAttributeId(int? pimAttributeId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimAttributeEnum.PimAttributeId.ToString(), ProcedureFilterOperators.Equals, pimAttributeId.ToString()));
            return DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
        }
    }
}
