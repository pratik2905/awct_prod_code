﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.Service.Service
{
    public class AWCTPromotionService : PromotionService
    {
        private readonly IZnodeRepository<ZnodePromotion> _promotionRepository;
        private readonly IZnodeRepository<ZnodePromotionCoupon> _couponRepository;
        private readonly IZnodeRepository<ZnodePromotionProduct> _promotionProductRepository;
        public AWCTPromotionService()
        {
            _promotionRepository = new ZnodeRepository<ZnodePromotion>();
            _couponRepository = new ZnodeRepository<ZnodePromotionCoupon>();
            _promotionProductRepository = new ZnodeRepository<ZnodePromotionProduct>();
        }
        //Create new promotion
        public override PromotionModel CreatePromotion(PromotionModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNull(model))
            {
                throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);
            }

            if (IsAlreadyExistsPromotion(model.PromoCode))
            {
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.ErrorPromotionExists);
            }
            foreach (CouponModel coupon in model?.CouponList?.CouponList)
            {
                if (model.IsUnique && string.IsNullOrEmpty(coupon.Code))
                {
                    throw new ZnodeException(ErrorCodes.CreationFailed, Admin_Resources.ErrorPromotionCreate);
                }
                if (IsAlreadyExistsCouponCode(coupon.Code))
                {
                    throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.AlreadyExistsCouponCode);
                }
            }

            //Replace default value by null
            ZnodeLogging.LogMessage("Executing SetDefaultValues.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            SetDefaultValues(model);

            // set the minimum quantity null
            if (model.QuantityMinimum == 0)
            {
                model.QuantityMinimum = null;
            }

            //Create promotion.
            ZnodePromotion promotion = _promotionRepository.Insert(model.ToEntity<ZnodePromotion>());

            model.PromotionId = promotion.PromotionId;
            ZnodeLogging.LogMessage("Create promotion with PromotionId.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, model?.PromotionId);

            //Association to promotion
            SaveAssociation(promotion.PromotionId, model);

            //Create Coupons for promotion.            
            if (promotion.PromotionId > 0 && promotion.IsCouponRequired.GetValueOrDefault() && HelperUtility.IsNotNull(model.CouponList))
            {
                model?.CouponList?.CouponList.ForEach(x => x.PromotionId = promotion.PromotionId);

                if (model.IsUnique && !model.PromotionTypeName.Contains("AWCT"))
                {
                    model?.CouponList?.CouponList.ForEach(x => x.AvailableQuantity = 1);
                }
                else
                {
                    model?.CouponList?.CouponList.ForEach(x => x.InitialQuantity = x.AvailableQuantity);
                }
                _couponRepository.Insert(model.CouponList.CouponList.ToEntity<ZnodePromotionCoupon>().ToList());
            }

            ZnodeEventNotifier<ZnodePromotionCoupon> clearCacheInitializer = new ZnodeEventNotifier<ZnodePromotionCoupon>(new ZnodePromotionCoupon());

            ZnodeLogging.LogMessage(IsNotNull(model) ? Admin_Resources.SuccessPromotionCreate : Admin_Resources.ErrorPromotionCreate, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);

            return model;

        }

        //Update promotion
        public override bool UpdatePromotion(PromotionModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("PromotionModel with PromotionId.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, model?.PromotionId);

            bool isUpdatePromotions = false;

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorPromotionModelNull);
            if (model.PromotionId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorPromotionIdlessThanOne);

            if (model?.PromotionId > 0)
            {
                SetDefaultValues(model);

                // set the minimum quantity null
                if (model.QuantityMinimum == 0)
                    model.QuantityMinimum = null;

                //Update promotion
                isUpdatePromotions = _promotionRepository.Update(model?.ToEntity<ZnodePromotion>());

                if (model.PromotionTypeId == 4 || model.PromotionTypeId == 8)
                {
                    int promotionProductID = _promotionProductRepository.Table.Where(x => x.PromotionId == model.PromotionId).Select(x => x.PromotionProductId).FirstOrDefault();
                    ZnodeLogging.LogMessage("promotionProductID.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, promotionProductID);

                    if (promotionProductID > 0)
                        isUpdatePromotions = _promotionProductRepository.Update(new ZnodePromotionProduct() { PromotionProductId = promotionProductID, PromotionId = model.PromotionId, PublishProductId = !string.IsNullOrEmpty(model.AssociatedProductIds) ? Convert.ToInt32(model.AssociatedProductIds) : 0 });
                    else
                        //Association to promotion
                        isUpdatePromotions = SaveAssociation(model.PromotionId, model);
                }
                if (model.IsCouponRequired.GetValueOrDefault() && isUpdatePromotions 
                    && !model.IsUnique)
                    isUpdatePromotions = UpdateCouponDetails(model);
                else if (!model.IsCouponRequired.GetValueOrDefault() && _couponRepository.Table.Where(x => x.PromotionId == model.PromotionId).Count() > 0)
                    isUpdatePromotions = DeleteCoupons(model);
                else if (model.PromotionId > 0 && model.IsCouponRequired.GetValueOrDefault()
                   && HelperUtility.IsNotNull(model.CouponList) && isUpdatePromotions && model.IsUnique && model.PromotionTypeName.Contains("AWCT"))
                {
                    isUpdatePromotions = UpdateCouponDetails(model);
                }
                else if (model.PromotionId > 0 && model.IsCouponRequired.GetValueOrDefault()
                    && HelperUtility.IsNotNull(model.CouponList) && isUpdatePromotions && model.IsUnique)
                {
                    //Get Already added coupan
                    List<string> addedCoupanList = _couponRepository.Table?.Where(x => x.PromotionId == model.PromotionId).Select(y => y.Code).ToList();
                    ZnodeLogging.LogMessage("Already added coupon added CouponList count.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, addedCoupanList?.Count);

                    //Update existing Coupan
                    foreach (var item in model.CouponList.CouponList)
                    {
                        ZnodePromotionCoupon znodePromotionCoupon = _couponRepository.Table.FirstOrDefault(x => x.IsActive != item.IsActive && x.PromotionId == model.PromotionId && x.Code == item.Code);
                        if (!IsNull(znodePromotionCoupon))
                        {
                            znodePromotionCoupon.IsActive = item.IsActive;
                            _couponRepository.Update(znodePromotionCoupon);
                        }
                    }
                    //Remove Already added coupan from List
                    model.CouponList.CouponList = addedCoupanList.Any() ? model.CouponList.CouponList?.Where(i => !addedCoupanList.Contains(i.Code)).ToList() : model.CouponList.CouponList;

                    model?.CouponList?.CouponList.ForEach(x => { x.PromotionId = model.PromotionId; x.AvailableQuantity = 1; });

                    model?.CouponList?.CouponList.ForEach(x => x.AvailableQuantity = 1);

                    _couponRepository.Insert(model.CouponList.CouponList.ToEntity<ZnodePromotionCoupon>().ToList());
                }
                if (!isUpdatePromotions)
                    throw new ZnodeException(ErrorCodes.InternalItemNotUpdated, Admin_Resources.ErrorPromotionUpdate);
            }
            //Clear webStore Cache on success update.
            if (isUpdatePromotions)
            {
                model.PortalId = PortalId;
                var clearCachePromotionWebstore = new ZnodeEventNotifier<PromotionModel>(model);
            }
            var clearCacheInitializer = new ZnodeEventNotifier<ZnodePromotionCoupon>(new ZnodePromotionCoupon());

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            return isUpdatePromotions;
        }
        private bool IsAlreadyExistsCouponCode(string coupon)
         => _couponRepository.Table.Any(a => a.Code == coupon);
        private bool IsAlreadyExistsPromotion(string promocode)
          => _promotionRepository.Table.Any(a => a.PromoCode == promocode);
        private void SetDefaultValues(PromotionModel promotionModel)
        {
            promotionModel.PortalId = promotionModel.PortalId == 0 ? null : promotionModel.PortalId;
            promotionModel.ProfileId = promotionModel.ProfileId == 0 ? null : promotionModel.ProfileId;
            promotionModel.BrandCode = promotionModel.BrandCode == "0" ? string.Empty : promotionModel.BrandCode;
        }
        //Update single coupon
        private bool UpdateCouponDetails(PromotionModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);

            bool isCouponUpdated = false;
            model.CouponList?.CouponList.ForEach(x => { x.PromotionId = model.PromotionId; x.AvailableQuantity = model.IsUnique && !model.PromotionTypeName.Contains("AWCT") ? 1 : x.AvailableQuantity; });
            CouponModel couponModel = model?.CouponList?.CouponList?.FirstOrDefault();

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePromotionEnum.PromotionId.ToString(), FilterOperators.Equals, model.PromotionId.ToString()));
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());

            ZnodePromotionCoupon promotionCoupon = _couponRepository.GetEntity(whereClauseModel.WhereClause, whereClauseModel.FilterValues);
            if (IsNotNull(promotionCoupon) && IsNotNull(couponModel))
            {
                couponModel.InitialQuantity = promotionCoupon.InitialQuantity;
                couponModel.PromotionCouponId = promotionCoupon.PromotionCouponId;
                ZnodeLogging.LogMessage("PromotionCouponId.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, couponModel.PromotionCouponId);

                isCouponUpdated = _couponRepository.Update(couponModel.ToEntity<ZnodePromotionCoupon>());
                ZnodeLogging.LogMessage(isCouponUpdated ? String.Format(Admin_Resources.SuccessPromotionUpdate, model.PromotionId) : Admin_Resources.ErrorPromotionUpdate, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            }
            else
            {
                isCouponUpdated = IsNotNull(_couponRepository.Insert(couponModel.ToEntity<ZnodePromotionCoupon>()));
                ZnodeLogging.LogMessage(isCouponUpdated ? String.Format(Admin_Resources.SuccessInsertPromotion, model.PromotionId) : Admin_Resources.ErrorInsertPromotion, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);

            return isCouponUpdated;
        }
        // Delete coupon.
        private bool DeleteCoupons(PromotionModel model)
        {
            FilterCollection deleteCouponfilters = new FilterCollection();
            deleteCouponfilters.Add(new FilterTuple(ZnodePromotionCouponEnum.PromotionId.ToString(), ProcedureFilterOperators.Equals, model.PromotionId.ToString()));
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(deleteCouponfilters.ToFilterDataCollection());
            return _couponRepository.Delete(whereClauseModel.WhereClause);
        }
    }
}
