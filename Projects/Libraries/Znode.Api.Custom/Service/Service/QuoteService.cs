﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Znode.Api.Custom.Helper;
using Znode.Api.Custom.IHelper;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model.Quote;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using ZnodeOmsQuote = Znode.Sample.Api.Model.Quote.ZnodeOmsQuote;
using ZnodeOmsQuoteLineItem = Znode.Libraries.Data.DataModel.ZnodeOmsQuoteLineItem;

namespace Znode.Api.Custom.Service.Service
{
    public class QuoteService : BaseService, IQuoteService
    {
        #region Private Variables
        protected readonly IZnodeRepository<ZnodeOmsQuote> _omsQuoteRepository;

        private readonly IZnodeRepository<ZnodeOmsQuoteLineItem> _omsQuoteLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsQuoteType> _quoteType;
        private readonly IZnodeRepository<ZnodeOmsCookieMapping> _cookieMappingRepository;
        private readonly IZnodeRepository<ZnodeShipping> _shippingRepository;
        private readonly IZnodeRepository<ZnodeAddress> _addressRepository;
        private readonly IZnodeRepository<ZnodeShippingType> _shippingTypeRepository;
        private readonly IZnodeRepository<ZnodeAccount> _accountRepository;
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeOmsNote> _OmsNotes;
        private readonly IZnodeRepository<ZnodeUserPortal> _userportalrepository;
        private readonly IZnodeRepository<AspNetUser> _aspnetUserRepository;
        private readonly IZnodeRepository<AspNetZnodeUser> _aspnetZnodeUserRepository;
        private readonly IZnodeRepository<ZnodeUserAddress> _userAddressRepository;

        private readonly IZnodeQuoteHelper quotehelper;
        private IPublishProductHelper publishProductHelper;
        public const string DateTimeRange = "DateTimeRange";

        private string _cultureCode = string.Empty;
        private string _currencyCode = string.Empty;
        #endregion Private Variables

        #region Constructor

        public QuoteService()
        {
            _omsQuoteRepository = new ZnodeRepository<ZnodeOmsQuote>();

            publishProductHelper = GetService<IPublishProductHelper>();
            _addressRepository = new ZnodeRepository<ZnodeAddress>();
            _shippingTypeRepository = new ZnodeRepository<ZnodeShippingType>();
            _shippingRepository = new ZnodeRepository<ZnodeShipping>();
            _accountRepository = new ZnodeRepository<ZnodeAccount>();
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _quoteType = new ZnodeRepository<ZnodeOmsQuoteType>();
            _OmsNotes = new ZnodeRepository<ZnodeOmsNote>();
            quotehelper = GetService<IZnodeQuoteHelper>();
            _cookieMappingRepository = new ZnodeRepository<ZnodeOmsCookieMapping>();
            _omsQuoteLineItemRepository = new ZnodeRepository<ZnodeOmsQuoteLineItem>();
            _userportalrepository = new ZnodeRepository<ZnodeUserPortal>();
            _aspnetUserRepository = new ZnodeRepository<AspNetUser>();
            _aspnetZnodeUserRepository = new ZnodeRepository<AspNetZnodeUser>();
            _userAddressRepository = new ZnodeRepository<ZnodeUserAddress>();

        }

        #endregion Constructor

        #region Public Methods
        //Get quotes list.
        public virtual QuoteListModel GetQuoteList(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            int userId = GetDataFromFilters(filters, ZnodeUserEnum.UserId.ToString());

            int omsQuoteTypeId = GetDataFromFilters(filters, ZnodeOmsQuoteEnum.OmsQuoteTypeId.ToString());

            int portalId = GetDataFromFilters(filters, FilterKeys.PortalId.ToString());

            //Add date time value in filter collection against filter column name Order date.
            filters = AddDateTimeValueInFilterByName(filters, "quotedate");

            //SetPageFilter if not set.
            SetPageFilter(page);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            List<QuoteModel> list = GetQuoteList(pageListModel, userId, omsQuoteTypeId);
            ZnodeLogging.LogMessage("Order list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, list?.Count);
            QuoteListModel quoteListModel = new QuoteListModel { Quotes = list?.ToList(), PortalName = GetPortalName(portalId) };
            if (list?.Count > 0)
            {
                quoteListModel.BindPageListModel(pageListModel);
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return quoteListModel;
        }

        //Creates Quote
        public virtual QuoteCreateModel Create(QuoteCreateModel quoteCreateModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(quoteCreateModel))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorShoppingCartModelNull);
            }

            ZnodeLogging.LogMessage("Properties of input parameter quoteCreateModel:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { PortalId = quoteCreateModel?.PortalId, UserId = quoteCreateModel?.UserId });

            quoteCreateModel.QuoteNumber = !string.IsNullOrEmpty(quoteCreateModel.QuoteNumber) ? quoteCreateModel.QuoteNumber
                                                                                                          : GenerateQuoteNumber(quoteCreateModel.PortalId);
            if (Convert.ToString(quoteCreateModel.Custom1) == "GuestUser")
            {
                string EmailAddress = Convert.ToString(quoteCreateModel.Custom2);
                //int userId = _userRepository.Table.Where(x => x.Email == EmailAddress && x.AspNetUserId != null).Select(y => y.UserId).FirstOrDefault();
                int PortalId = Convert.ToInt32(quoteCreateModel.PortalId);
                // int userId1 = _userportalrepository.Table.Where(x => x.UserId == userId && x.PortalId == PortalId).Select(y => y.UserId);

                //UserModel userDetails = (from u in _userRepository.Table
                //                         join up in _userportalrepository.Table on u.UserId equals up.UserId
                //                         where u.Email == EmailAddress && up.PortalId == PortalId && u.AspNetUserId != null
                //                         select new UserModel
                //                         {
                //                             UserId = u.UserId,
                //                         })?.FirstOrDefault();

                UserModel userDetails = (from u in _userRepository.Table
                                         join Aspnetuser in _aspnetUserRepository.Table on u.AspNetUserId equals Aspnetuser.Id
                                         join aspnetznode in _aspnetZnodeUserRepository.Table on Aspnetuser.UserName equals aspnetznode.AspNetZnodeUserId
                                         where u.Email == EmailAddress && u.AspNetUserId != null && u.IsActive == true
                                        select new UserModel
                                        {
                                             UserName = aspnetznode.UserName

                                        })?.FirstOrDefault();


                if (IsNotNull(userDetails))
                {
                    //string UserName = Convert.ToString(userDetails.UserName);
                    userDetails = (from u in _userRepository.Table
                                   join up in _userportalrepository.Table on u.UserId equals up.UserId
                                   where (u.Email == EmailAddress || (!string.IsNullOrEmpty(u.ExternalId) && u.ExternalId.Contains(userDetails.UserName))) && u.AspNetUserId != null && u.IsActive == true
                                   select new UserModel
                                   {
                                       UserId = u.UserId

                                   })?.FirstOrDefault();

                    ZnodeUserAddress znodeuseraddress = new ZnodeUserAddress();
                    var userId = 0;
                    if (quoteCreateModel.ShippingAddressId != 0)
                    {
                        userId = _userAddressRepository.Table.Where(y => y.AddressId == quoteCreateModel.ShippingAddressId).Select(x => x.UserId).FirstOrDefault();
                    }
                    else
                    {
                        userId = _userAddressRepository.Table.Where(y => y.AddressId == quoteCreateModel.BillingAddressId).Select(x => x.UserId).FirstOrDefault();

                    }

                    if (quoteCreateModel.UserId == userId)
                    {
                        UpdateUserAddressUserId(userId, userDetails.UserId);

                    }
                    quoteCreateModel.UserId = userDetails.UserId;
                }

            }

            //AddressModel ShippingAddress = _addressRepository.GetById(Convert.ToInt32(quoteCreateModel.ShippingAddressId))?.ToModel<AddressModel>();

            //string cost = GetShippingRate(ShippingAddress.StateName, ShippingAddress.CountryName, Convert.ToDecimal(quoteCreateModel.QuoteTotal));
            //quoteCreateModel.ShippingCost = Convert.ToDecimal(cost);

            //save oms quote detail
            ZnodeOmsQuote quote = SaveQuoteDetail(quoteCreateModel);
            ZnodeLogging.LogMessage("OmsQuoteId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, quote?.OmsQuoteId);

            if (quote?.OmsQuoteId > 0)
            {
                //Save quote line items.
                if (SaveQuoteLineItems(quoteCreateModel, quote))
                {
                    SendQuoteReceiptEmailToUser(quoteCreateModel.UserId, quoteCreateModel.PortalId, quote.QuoteNumber, GetLocaleIdFromHeader(), quoteCreateModel, quote);
                    SendQuoteReceiptEmailToAdmin(quoteCreateModel.PortalId, quote.QuoteNumber, GetLocaleIdFromHeader(), quoteCreateModel, quote);
                    return new QuoteCreateModel() { OmsQuoteId = quote.OmsQuoteId };
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return new QuoteCreateModel();
        }

        //Get Order Receipt Datails. 
        public virtual QuoteResponseModel GetQuoteReceipt(int quoteId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (quoteId <= 0)
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorPlaceOrder);
            }

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple("OmsQuoteId", FilterOperators.Equals, quoteId.ToString()));

            QuoteResponseModel quoteModel = new QuoteResponseModel();
            quoteModel = GetQuoteFromDBById(quoteId);

            if (IsNotNull(quoteModel))
            {
                //Set quote line items details
                SetCartItemDetails(quoteModel);

                //Get quote status for quote.
                quoteModel.QuoteStatus = GetQuoteStatus(quoteModel.OmsQuoteStateId);

                quoteModel.ShippingType = GetShippingType(quoteModel.ShippingId);

                //Get shipping and billing address.
                SetShippingBillingAddress(quoteModel);

                //Check UserExpand
                GetUserDetails(quoteModel);

                //Get default currency assigned to current portal.
                if (quoteModel.PortalId > 0)
                {
                    SetPortalDefaultCurrencyCultureCode(quoteModel.PortalId, quoteModel);
                }

                //Get Quote Note Details
                quoteModel.QuoteHistoryList = GetQuoteNotes(quoteModel.OmsQuoteId);

            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return quoteModel;
        }

        //Get quote details by quote id.
        public virtual QuoteResponseModel GetQuoteById(int omsQuoteId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (omsQuoteId <= 0)
            {
                throw new ZnodeException(ErrorCodes.NotFound, "OmsQuoteId does not exist");
            }

            QuoteResponseModel quoteDetail = GetQuoteDetailByQuoteId(omsQuoteId);

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return quoteDetail;
        }

        // Convert the quote to order
        public virtual OrderModel ConvertQuoteToOrder(ConvertQuoteToOrderModel convertToOrderModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNull(convertToOrderModel))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Api_Resources.ModelNotNull);
            }

            if (convertToOrderModel.OmsQuoteId <= 0)
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Api_Resources.ErrorQuoteIdGreaterThanZero);
            }

            ZnodeLogging.LogMessage("Input parameter OmsQuoteId for getting quote details :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { convertToOrderModel?.OmsQuoteId });
            //convertToOrderModel.ToEntity<ZnodeOmsQuote>().ToTable("ZnodeOmsQuote");
            //ZnodeOmsQuote quoteDetails = _omsQuoteRepository.GetById(convertToOrderModel.OmsQuoteId); //null
            ZnodeOmsQuote quoteDetails = GetQuoteDetailsById(convertToOrderModel.OmsQuoteId);
            quoteDetails.PaymentSettingId = convertToOrderModel.PaymentDetails.PaymentSettingId;
            quoteDetails.OmsOrderStateId = GetOmsQuoteStateId(ZnodeOrderStatusEnum.APPROVED.ToString());
            //convertToOrderModel.UserId = convertToOrderModel.UserId > 0 ? convertToOrderModel.UserId : quoteDetails.UserId;
            convertToOrderModel.UserId = convertToOrderModel.UserId > 0 ? convertToOrderModel.UserId : quoteDetails.UserId;
            //quoteDetails.UserId = convertToOrderModel.UserId > 0 ? convertToOrderModel.UserId : quoteDetails.UserId;
            if (IsNull(quoteDetails))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Api_Resources.DetailsNotFound);
            }

            if (IsQuoteValidForConvertToOrder(quoteDetails.QuoteExpirationDate, quoteDetails.OmsOrderStateId, Convert.ToBoolean(quoteDetails.IsConvertedToOrder)))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Api_Resources.ErrorConvertQuoteToOrder);
            }

            CartParameterModel cartParameterModel = ToCartParameterModel(quoteDetails.UserId, quoteDetails.PortalId, convertToOrderModel.OmsQuoteId, Convert.ToInt32(quoteDetails.ShippingId));

            AccountQuoteModel accountQuoteModel = new AccountQuoteModel();
            accountQuoteModel.CultureCode = quoteDetails.CultureCode;
            AWCTCurrencyService _currencyService = new AWCTCurrencyService();
            accountQuoteModel.CurrencyCode = _currencyService.GetCurrencyDetail(quoteDetails.PortalId)?.CurrencyCode;

            AWCTOrderService _orderService = new AWCTOrderService();

            ShoppingCartModel shoppingCartModel = _orderService.GetShoppingCartDetails(quoteDetails, accountQuoteModel, cartParameterModel);
            //Set IsCalculatePromotionAndCoupon to not calculate promotion and coupon discount for quote.
            //Commentd as entity is not available
            // shoppingCartModel.IsCalculatePromotionAndCoupon = false;
            SubmitOrderModel submitOrderModel = new SubmitOrderModel();
            shoppingCartModel.OrderNumber = _orderService.GenerateOrderNumber(submitOrderModel, new ParameterModel() { Ids = Convert.ToString(shoppingCartModel.PortalId) });

            bool isCreditCardPayment = IsCreditCardPayment(convertToOrderModel.PaymentDetails.PaymentType);

            bool isPaymentSuccess = ProcessPayment(convertToOrderModel, shoppingCartModel);

            if (isPaymentSuccess)
            {
                //Get generated unique order number on basis of current date.
                submitOrderModel.OrderNumber = shoppingCartModel.OrderNumber;
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                //OrderModel orderModel = _orderService.SaveOrder(shoppingCartModel, submitOrderModel);
                OrderModel orderModel = _orderService.CreateOrder(shoppingCartModel);

                if (!shoppingCartModel.IsGatewayPreAuthorize && isCreditCardPayment)
                {
                    _orderService.UpdateOrderPaymentStatus(orderModel.OmsOrderId, ZnodeConstant.CAPTURED.ToString());
                }

                SendQuoteConvertToOrderMail(convertToOrderModel.UserId, orderModel.PortalId, quoteDetails.QuoteNumber, orderModel.OrderNumber, GetLocaleIdFromHeader());

                return orderModel;
            }
            else
            {
                ZnodeLogging.LogMessage("Error while processing payment", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return new OrderModel();
            }
        }

        //to get quote line items by omsQuoteId
        public virtual List<QuoteLineItemModel> GetQuoteLineItems(int omsQuoteId)
        {
            if (IsNull(omsQuoteId) || omsQuoteId == 0)
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorQuoteIdNullOrZero);
            }

            IZnodeRepository<ZnodeOmsQuoteLineItem> _quoteLineItemRepository = new ZnodeRepository<ZnodeOmsQuoteLineItem>();
            FilterDataCollection filter = new FilterDataCollection();
            filter.Add(new FilterDataTuple(ZnodeOmsQuoteLineItemEnum.OmsQuoteId.ToString(), FilterOperators.Equals, omsQuoteId.ToString()));
            string whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter)?.WhereClause;
            return _quoteLineItemRepository.GetEntityList(whereClause)?.ToModel<QuoteLineItemModel>().ToList();
        }

        //Update existing Quote.
        public virtual BooleanModel UpdateQuote(UpdateQuoteModel model)
        {
            try
            {
                BooleanModel isQuoteUpdated = null;

                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                if (IsNull(model))
                {
                    throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorQuoteModelNull);
                }

                if (IsNull(model.OmsQuoteId) || model?.OmsQuoteId == 0)
                {
                    throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorQuoteIdNullOrZero);
                }

                if (IsAllowedTerritories(model?.QuoteLineItems))
                {
                    throw new ZnodeException(ErrorCodes.AllowedTerritories, Admin_Resources.AllowedTerritoriesError);
                }

                //if there is no change in Quote data then no need to update quote
                if (!IsQuoteDataUpdated(model))
                {
                    return new BooleanModel { IsSuccess = true };
                }

                ZnodeLogging.LogMessage(string.Format("Update Quote process is initiated for the Quote Id: {0}", model.OmsQuoteId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                int notesId = 0;
                if (!string.IsNullOrEmpty(model.AdditionalInstructions))
                {
                    OrderNotesModel notesModel = new OrderNotesModel() { Notes = model.AdditionalInstructions, OmsQuoteId = model.OmsQuoteId, CreatedBy = GetLoginUserId(), ModifiedBy = GetLoginUserId() };
                    AddQuoteNote(notesModel);
                    notesId = notesModel.OmsNotesId;
                }

                if (!string.IsNullOrEmpty(model.QuoteHistory))
                {
                    CreateQuoteHistory(new AWCTOrderHistoryModel { OMSQuoteId = model.OmsQuoteId, Message = model.QuoteHistory, OmsNotesId = notesId, OrderAmount = BindQuoteAmount(model), CreatedBy = model.CreatedBy, ModifiedBy = model.ModifiedBy });
                }

                //Update Quote Chnages in database
                isQuoteUpdated = UpdateQuoteDetails(model);
                if (model.QuoteLineItems?.Count > 0)
                {
                    isQuoteUpdated.IsSuccess = UpdateQuoteLineItem(model);
                }
                return isQuoteUpdated;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return new BooleanModel { IsSuccess = false, ErrorMessage = ex.ToString() };
            }
        }

        #endregion

        #region Protected Methods

        //To generate unique order number on basis of current date.
        protected virtual string GenerateQuoteNumber(int portalId)
        {
            string portalName = GetPortalName(portalId);
            string orderNumber = string.Empty;

            if (!string.IsNullOrEmpty(portalName))
            {
                orderNumber = portalName.Trim().Length > 2 ? portalName.Substring(0, 2) : portalName.Substring(0, 1);
            }

            DateTime date = DateTime.Now;
            string strDate = date.ToString("yyMMdd-HHmmss-fff");
            orderNumber += $"-{strDate}";

            return orderNumber.ToUpper();
        }

        //Saves Quote Details
        protected virtual ZnodeOmsQuote SaveQuoteDetail(QuoteCreateModel quoteCreateModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (quoteCreateModel.FreeShipping)
            {
                quoteCreateModel.ShippingId = _shippingRepository.Table.FirstOrDefault(x => x.ShippingCode == "FreeShipping").ShippingId;
            }

            ZnodeOmsQuote quoteEntity = quoteCreateModel.ToEntity<ZnodeOmsQuote>();
            quoteEntity = ToQuoteEntity(quoteEntity, quoteCreateModel);
            ZnodeOmsQuote quote = SaveQuoteDetailsDB(quoteEntity);//_omsQuoteRepository.Insert(quoteEntity);

            ZnodeLogging.LogMessage(quote.OmsQuoteId > 0 ? Admin_Resources.SuccessQuoteCreated : Api_Resources.ErrorQuoteCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return quote;
        }
        protected virtual ZnodeOmsQuote SaveQuoteDetailsDB(ZnodeOmsQuote quoteEntity)
        {
            ZnodeLogging.LogMessage("Inside SaveQuoteDetailsDB = PortalId=" + Convert.ToString(quoteEntity.PortalId) + "UserId=" + Convert.ToString(quoteEntity.UserId) + "OmsOrderStateId=" + Convert.ToString(quoteEntity.OmsOrderStateId) + "ShippingId=" + Convert.ToString(quoteEntity.ShippingId) + "ShippingAddressId=" + Convert.ToString(quoteEntity.ShippingAddressId) + "ShippingTypeId=" + Convert.ToString(quoteEntity.ShippingTypeId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

            //SP call to save/update savedCartLineItem
            int Status = 0;
            IZnodeViewRepository<ZnodeOmsQuote> objStoredProc = new ZnodeViewRepository<ZnodeOmsQuote>();
            objStoredProc.SetParameter("PortalId", quoteEntity.PortalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("UserId", quoteEntity.UserId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("OmsOrderStateId", quoteEntity.OmsOrderStateId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("ShippingId", quoteEntity.ShippingId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("ShippingAddressId", quoteEntity.ShippingAddressId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("BillingAddressId", quoteEntity.BillingAddressId, ParameterDirection.Input, DbType.Int32);
            if (!String.IsNullOrEmpty(quoteEntity.AdditionalInstruction))
            {
                objStoredProc.SetParameter("AdditionalInstruction", quoteEntity.AdditionalInstruction, ParameterDirection.Input, DbType.String);
            }
            else
            {
                objStoredProc.SetParameter("AdditionalInstruction", DBNull.Value, ParameterDirection.Input, DbType.String);
            }
            objStoredProc.SetParameter("QuoteOrderTotal", quoteEntity.QuoteOrderTotal, ParameterDirection.Input, DbType.Decimal);
            //objStoredProc.SetParameter("CreatedBy", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("CreatedBy", quoteEntity.UserId, ParameterDirection.Input, DbType.Int32);

            objStoredProc.SetParameter("IsPendingPayment", quoteEntity.IsPendingPayment, ParameterDirection.Input, DbType.Boolean);
            if ((quoteEntity.IsConvertedToOrder) != null)
            {
                objStoredProc.SetParameter("IsConvertedToOrder", quoteEntity.IsConvertedToOrder, ParameterDirection.Input, DbType.Boolean);
            }
            else
            {
                objStoredProc.SetParameter("IsConvertedToOrder", DBNull.Value, ParameterDirection.Input, DbType.Boolean);
            }
            objStoredProc.SetParameter("TaxCost", quoteEntity.TaxCost, ParameterDirection.Input, DbType.Decimal);
            objStoredProc.SetParameter("ShippingCost", quoteEntity.ShippingCost, ParameterDirection.Input, DbType.Decimal);
            objStoredProc.SetParameter("OmsQuoteTypeId", quoteEntity.OmsQuoteTypeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("QuoteTypeCode", quoteEntity.QuoteTypeCode, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("PublishStateId", quoteEntity.PublishStateId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("QuoteExpirationDate", quoteEntity.QuoteExpirationDate, ParameterDirection.Input, DbType.DateTime);
            objStoredProc.SetParameter("QuoteNumber", quoteEntity.QuoteNumber, ParameterDirection.Input, DbType.String);
            if ((quoteEntity.InHandDate) != null)
            {
                objStoredProc.SetParameter("InHandDate", quoteEntity.InHandDate, ParameterDirection.Input, DbType.DateTime);
            }
            else
            {
                objStoredProc.SetParameter("InHandDate", DBNull.Value, ParameterDirection.Input, DbType.DateTime);
            }
            objStoredProc.SetParameter("ShippingTypeId", quoteEntity.ShippingTypeId, ParameterDirection.Input, DbType.Int32);
            if (!String.IsNullOrEmpty(quoteEntity.AccountNumber))
            {
                objStoredProc.SetParameter("AccountNumber", quoteEntity.AccountNumber, ParameterDirection.Input, DbType.String);
            }
            else
            {
                objStoredProc.SetParameter("AccountNumber", DBNull.Value, ParameterDirection.Input, DbType.String);
            }
            if (!String.IsNullOrEmpty(quoteEntity.ShippingMethod))
            {
                objStoredProc.SetParameter("ShippingMethod", quoteEntity.ShippingMethod, ParameterDirection.Input, DbType.String);
            }
            else
            {
                objStoredProc.SetParameter("ShippingMethod", DBNull.Value, ParameterDirection.Input, DbType.String);
            }
            objStoredProc.SetParameter("CultureCode", quoteEntity.CultureCode, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("IsTaxExempt", quoteEntity.IsTaxExempt, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@Status", Status, ParameterDirection.Output, DbType.Int32);

            ZnodeOmsQuote omsQuote = objStoredProc.ExecuteStoredProcedureList("AWCT_SaveQuote @PortalId,@UserId,@OmsOrderStateId,@ShippingId,@ShippingAddressId,@BillingAddressId,@AdditionalInstruction,@QuoteOrderTotal,@CreatedBy,@IsPendingPayment,@IsConvertedToOrder,@TaxCost,@ShippingCost,@OmsQuoteTypeId,@QuoteTypeCode,@PublishStateId,@QuoteExpirationDate,@QuoteNumber ,@InHandDate,@ShippingTypeId,@AccountNumber,@ShippingMethod,@CultureCode,@IsTaxExempt,@Status OUT", 24, out Status).FirstOrDefault();
            return omsQuote;
        }
        //Set ZnodeOmsQuote properties from QuoteCreateModel
        protected virtual ZnodeOmsQuote ToQuoteEntity(ZnodeOmsQuote quoteEntity, QuoteCreateModel quoteCreateModel)
        {
            quoteEntity.QuoteOrderTotal = Convert.ToDecimal(quoteCreateModel.QuoteTotal);
            quoteEntity.OmsOrderStateId = GetOmsQuoteStateId(quoteCreateModel.OmsQuoteStatus);
            quoteEntity.OmsQuoteTypeId = Convert.ToInt32(GetQuoteTypeIdByCode(quoteCreateModel.QuoteTypeCode));
            quoteEntity.PublishStateId = quoteCreateModel.PublishStateId > 0 ? (byte)quoteCreateModel.PublishStateId : (byte)PublishStateId;


            AWCTCurrencyService _currencyService = new AWCTCurrencyService();
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(FilterKeys.CurrencyId, FilterOperators.Equals, _currencyService.GetCurrencyDetail(quoteEntity.PortalId)?.CurrencyId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, ZnodeConstant.TrueValue));
            filter.Add(new FilterTuple("isdefault", FilterOperators.Equals, ZnodeConstant.TrueValue));
            quoteEntity.CultureCode = _currencyService.GetCultureCode(filter)?.CultureCode;

            return quoteEntity;
        }

        //Get the quoteTypeId.
        protected int? GetQuoteTypeIdByCode(string quoteTypeCode)
        {
            int? quoteType = _quoteType?.Table?.FirstOrDefault(x => x.QuoteTypeCode.ToLower() == quoteTypeCode.ToLower())?.OmsQuoteTypeId;
            return quoteType > 0 ? quoteType : 3;
        }

        //Get OmsOrderStateId based on order status.
        protected virtual int GetOmsQuoteStateId(string quoteStatus)
        {
            IZnodeRepository<ZnodeOmsOrderState> _orderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
            int quoteStateId = Convert.ToInt32(_orderStateRepository.Table.FirstOrDefault(x => string.Equals(x.OrderStateName, quoteStatus)).OmsOrderStateId);
            return quoteStateId > 0 ? quoteStateId : Convert.ToInt32(_orderStateRepository.Table.FirstOrDefault(x => string.Equals(x.OrderStateName, "SUBMITTED"))?.OmsOrderStateId);
        }

        //Save quote line items.
        protected virtual bool SaveQuoteLineItems(QuoteCreateModel quoteCreateModel, ZnodeOmsQuote quote)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsQuoteId = quote.OmsQuoteId });

            //Save all quote line item.
            if (SaveAllQuoteCartLineItems(quote.OmsQuoteId, quoteCreateModel))
            {
                //Add Addtional Notes for Quotes.
                if (!string.IsNullOrEmpty(quoteCreateModel.AdditionalInstruction))
                {
                    AddAdditionalNotes(quote.OmsQuoteId, quoteCreateModel.AdditionalInstruction);
                }

                // Remove all saved cart items.
                RemoveSavedCartItems(quoteCreateModel.UserId, quoteCreateModel.CookieMappingId);

                return true;
            }
            ZnodeLogging.LogMessage("OmsQuoteId property of QuoteCreateModel to be returned:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsQuoteId = quote?.OmsQuoteId });
            return false;
        }

        //Add Addtional Notes for Quotes.
        protected virtual void AddAdditionalNotes(int quoteId, string additionalNotes)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { quoteId = quoteId, additionalNotes = additionalNotes });
            if (!string.IsNullOrEmpty(additionalNotes) && quoteId > 0)
            {
                //Add additional notes for quotes.
                _OmsNotes.Insert(new ZnodeOmsNote() { OmsQuoteId = quoteId, Notes = additionalNotes });
            }
        }

        //To save SavedCartlineItem data in database
        protected virtual bool SaveAllQuoteCartLineItems(int quoteId, QuoteCreateModel quoteCreateModel)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { quoteId = quoteId });
            int savedCartId = 0;
            if (quoteId > 0 && !Equals(quoteCreateModel, null))
            {
                //Get Product details.
                DataTable productDetails = GetProductDetails(quoteCreateModel);

                int cookieMappingId = 0;
                //Get SavedCartId
                savedCartId = GetSavedCartId(quoteCreateModel, ref cookieMappingId);

                // quoteCreateModel.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);

                //If the new cookie Mapping Id gets generated, then it should assign back within the requested model.
                quoteCreateModel.CookieMappingId = new ZnodeEncryption().EncryptData(cookieMappingId.ToString());

                ZnodeLogging.LogMessage("SavedCartId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, savedCartId);

                //SP call to save/update savedCartLineItem
                IZnodeViewRepository<QuoteCreateModel> objStoredProc = new ZnodeViewRepository<QuoteCreateModel>();
                objStoredProc.SetParameter("OmsQuoteId", quoteId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), quoteCreateModel.UserId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("OmsSavedCartId", savedCartId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
                objStoredProc.SetTableValueParameter("@SKUPriceForQuote", productDetails, ParameterDirection.Input, SqlDbType.Structured, "dbo.SKUPriceForQuote");
                objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateQuoteLineItem @OmsQuoteId,@UserId,@OmsSavedCartId,@Status OUT,@SKUPriceForQuote", 3, out int status);

                return status == 1;
            }
            return false;
        }

        // Remove all saved cart items.
        protected virtual void RemoveSavedCartItems(int userId, string cookieMapping)
        {
            IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();
            int cookieMappingId = !string.IsNullOrEmpty(cookieMapping) ? Convert.ToInt32(new ZnodeEncryption().DecryptData(cookieMapping)) : 0;
            ZnodeLogging.LogMessage("userId and cookieMappingId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { userId = userId, cookieMappingId = cookieMappingId });
            _shoppingCartService.RemoveSavedCartItems(userId, cookieMappingId);
        }

        //Get shipping and billing address.
        protected virtual void SetShippingBillingAddress(QuoteResponseModel quote)
        {
            ZnodeLogging.LogMessage("BillingAddressId and ShippingAddressId of input QuoteResponseModel:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { BillingAddressId = quote?.BillingAddressId, ShippingAddressId = quote?.ShippingAddressId });

            //Check if Shipping address id is same as Billing address id, assign billing address to shipping address. 
            if (Equals(quote?.ShippingAddressId, quote?.BillingAddressId))
            {
                quote.BillingAddressModel = _addressRepository.Table.Where(x => x.AddressId == quote.BillingAddressId)?.ToModel<AddressModel>()?.FirstOrDefault();
                quote.ShippingAddressModel = quote.BillingAddressModel;
            }
            else
            {
                //If Billing address id greater, get billing address.
                if (quote?.BillingAddressId > 0)
                {
                    quote.BillingAddressModel = _addressRepository.Table.Where(x => x.AddressId == quote.BillingAddressId)?.ToModel<AddressModel>()?.FirstOrDefault();
                }

                //If Shipping address id greater, get shipping address.
                if (quote?.ShippingAddressId > 0)
                {
                    quote.ShippingAddressModel = _addressRepository.Table.Where(x => x.AddressId == quote.ShippingAddressId)?.ToModel<AddressModel>()?.FirstOrDefault();
                }
            }
            quote.BillingAddressHtml = IsNotNull(quote.BillingAddressModel) ? GetOrderBillingAddress(quote.BillingAddressModel) : "";
            quote.ShippingAddressHtml = IsNotNull(quote.ShippingAddressModel) ? GetOrderShipmentAddress(quote.ShippingAddressModel) : "";
        }

        //Get Shopping cart details required for quote.
        protected virtual void SetCartItemDetails(QuoteResponseModel quoteModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("OmsQuoteId of QuoteResponseModel:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsQuoteId = quoteModel?.OmsQuoteId });

            //Map parameters of quoteModel to CartParameterModel.
            CartParameterModel cartParameterModel = ToCartParameterModel(quoteModel.UserId, quoteModel.PortalId, quoteModel.OmsQuoteId, quoteModel.ShippingId, quoteModel.LocaleId);

            IShoppingCartMap _shoppingCartMap = GetService<IShoppingCartMap>();
            IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();

            //LoadFromDatabase gives required details for Quote line items.
            //Need to write code as Mongo --To do 
            quoteModel.ShoppingCartItems = LoadCartFromQuote(cartParameterModel, quoteModel)?.ShoppingCartItems;

            quoteModel.SubTotal = Convert.ToDecimal(quoteModel?.ShoppingCartItems?.Sum(x => x.ExtendedPrice));

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Set Cart parameter model from QuoteResponseModel
        protected virtual CartParameterModel ToCartParameterModel(int userId, int portalId, int omsQuoteId, int shippingId = 0, int localeId = 0)
        {
            if (userId > 0 && portalId > 0)
            {
                IZnodeOrderHelper znodeOrderHelper = GetService<IZnodeOrderHelper>();
                return new CartParameterModel
                {
                    LocaleId = localeId > 0 ? localeId : GetDefaultLocaleId(),
                    UserId = userId,
                    PortalId = portalId,
                    ShippingId = shippingId,
                    OmsQuoteId = omsQuoteId,
                    PublishedCatalogId = GetPublishCatalogId(userId, portalId),
                    ProfileId = GetProfileId(),
                    CookieId = znodeOrderHelper.GetCookieMappingId(userId, portalId)
                };
            }
            else
            {
                return new CartParameterModel();
            }
        }

        //Get shipping type by shipping type id.
        protected virtual string GetShippingType(int shippingId)
        {
            string shippingType = string.Empty;
            if (shippingId > 0)
            {
                shippingType = _shippingRepository.Table.FirstOrDefault(w => w.ShippingId == shippingId)?.Description;
            }

            return shippingType;
        }


        //to get shipping address
        protected virtual string GetOrderBillingAddress(AddressModel quoteBilling)
        {
            if (IsNotNull(quoteBilling))
            {
                string street1 = string.IsNullOrEmpty(quoteBilling.Address2) ? string.Empty : "<br />" + quoteBilling.Address2;
                return $"{quoteBilling.FirstName}{" "}{quoteBilling.LastName}{"<br />"}{quoteBilling.CompanyName}{"<br />"}" +
                       $"{quoteBilling.Address1}{street1}{"<br />"}{quoteBilling.CityName}{"<br />"}" +
                       $"{(string.IsNullOrEmpty(quoteBilling.StateCode) ? quoteBilling.StateName : quoteBilling.StateCode)}" +
                       $"{"<br />"}{quoteBilling.PostalCode}{"<br />"}{quoteBilling.CountryName}{"<br />"}" +
                       $"{Admin_Resources.LabelPhoneNumber}{" : "}{quoteBilling.PhoneNumber}";
            }
            return string.Empty;
        }

        protected virtual string GetOrderShipmentAddress(AddressModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                ZnodeLogging.LogMessage("AddressId to get shipping company name", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { AddressId = orderShipment.AddressId });
                string ShippingcompanyName = _addressRepository.Table.FirstOrDefault(x => x.AddressId == orderShipment.AddressId)?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.Address2) ? string.Empty : "<br />" + orderShipment.Address2;

                return $"{orderShipment?.FirstName}{" "}{ orderShipment?.LastName}{"<br />"}" +
                       $"{(string.IsNullOrEmpty(orderShipment?.CompanyName) ? ShippingcompanyName : orderShipment.CompanyName)}{"<br />"}" +
                       $"{orderShipment.Address1}{street1}{"<br />"}{ orderShipment.CityName}{"<br />"}" +
                       $"{(string.IsNullOrEmpty(orderShipment.StateCode) ? orderShipment.StateName : orderShipment.StateCode)}{"<br />"}" +
                       $"{orderShipment.PostalCode}{"<br />"}{orderShipment.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}" +
                       $"{orderShipment.PhoneNumber}{"<br />"}";
            }
            return string.Empty;
        }

        //Get quote list by sp.
        protected virtual List<QuoteModel> GetQuoteList(PageListModel pageListModel, int userId, int? omsQuoteTypeId)
        {
            List<QuoteModel> quoteList;
            try
            {
                ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("Input parameters to get order list:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { pageListModel = pageListModel?.ToDebugString() });
                 
                if (omsQuoteTypeId < 0)
                {
                    throw new ZnodeException(ErrorCodes.NotFound, "QuoteTypeId does not exist");
                }
                int rowcount = pageListModel.PagingLength > 0 ? pageListModel.PagingLength : 10;
                int PagingStart = pageListModel.PagingStart > 0 ? pageListModel.PagingStart : 1;
                IZnodeViewRepository<QuoteModel> objStoredProc = new ZnodeViewRepository<QuoteModel>();
                objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter("@Rows", rowcount, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@PageNo", PagingStart, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
                objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@OmsQuoteTypeId", omsQuoteTypeId, ParameterDirection.Input, DbType.Int32);

                quoteList = objStoredProc.ExecuteStoredProcedureList("Znode_GetQuoteList" + " @WhereClause, @Rows,@PageNo,@Order_By,@RowCount OUT,@UserId,@OmsQuoteTypeId", 4, out pageListModel.TotalRowCount).ToList();

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Portal.ToString(), TraceLevel.Error);
                quoteList = new List<QuoteModel>();
            }
           
            return quoteList;
        }

        //Get Quote details by QuoteId.
        protected virtual QuoteResponseModel GetQuoteDetailByQuoteId(int omsQuoteId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { omsQuoteId = omsQuoteId });

            QuoteResponseModel quoteModel = new QuoteResponseModel();

            if (omsQuoteId > 0)
            {
                ZnodeOmsQuote quote = null;

                IZnodeQuoteHelper _quotehelper = GetService<IZnodeQuoteHelper>();
                quote = null;// _quotehelper.GetQuoteById(omsQuoteId);

                if (IsNotNull(quote))
                {
                    //Map quote object to QuoteResponseModel object.
                    // quoteModel = quote.ToModel<QuoteResponseModel>();

                    //Get othe Quote details.
                    GetQuoteDetails(quoteModel);
                }
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return quoteModel;
        }

        //Get othe Quote details.
        protected virtual void GetQuoteDetails(QuoteResponseModel quoteModel)
        {
            if (IsNotNull(quoteModel))
            {
                //Get quote status for quote.
                quoteModel.QuoteStatus = GetQuoteStatus(quoteModel.OmsQuoteStateId);

                //Get shipping and billing address.
                SetShippingBillingAddress(quoteModel);

                //Check UserExpand
                GetUserDetails(quoteModel);

                MapPortalData(quoteModel);

                //Get Shopping cart details required for quote.
                MapCartItemDataForQuote(quoteModel);

                //Get Shipping Details
                GetShippingdetails(quoteModel);

                quoteModel.SubTotal = GetQuoteSubTotal(quoteModel?.ShoppingCartItems);

                //Get Quote History Details
                MapQuoteHistory(quoteModel);
            }
        }

        protected virtual void GetShippingdetails(QuoteResponseModel quoteModel)
        {
            quoteModel.ShippingDiscountDescription = _shippingRepository.Table.FirstOrDefault(w => w.ShippingId == quoteModel.ShippingId)?.Description;
            quoteModel.ShippingTypeId = _shippingRepository.Table.FirstOrDefault(w => w.ShippingId == quoteModel.ShippingId).ShippingTypeId;
            quoteModel.ShippingTypeClassName = _shippingTypeRepository.Table.FirstOrDefault(w => w.ShippingTypeId == quoteModel.ShippingTypeId)?.ClassName;
        }


        //Get user details by id.
        protected virtual void GetUserDetails(QuoteResponseModel quote)
        {
            ZnodeLogging.LogMessage("UserId to get user details: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { UserId = quote?.UserId });

            if (IsNotNull(quote))
            {
                UserModel userDetails = GetUserNameByUserId(quote.UserId);
                if (IsNotNull(userDetails))
                {
                    quote.UserName = userDetails.UserName;
                    quote.CreatedByName = userDetails.FirstName + " " + userDetails.LastName;
                }

                userDetails = (from user in _userRepository.Table
                               where user.UserId == quote.UserId
                               select new UserModel
                               {
                                   FirstName = user.FirstName,
                                   LastName = user.LastName,
                                   Email = user.Email,
                                   PhoneNumber = user.PhoneNumber
                               })?.FirstOrDefault();

                if (IsNotNull(userDetails))
                {
                    quote.FirstName = userDetails.FirstName;
                    quote.LastName = userDetails.LastName;
                    quote.PhoneNumber = userDetails.PhoneNumber;
                    quote.Email = userDetails.Email;
                }
            }
        }

        //Map Portal related data.
        protected virtual void MapPortalData(QuoteResponseModel quote)
        {
            if (IsNotNull(quote))
            {
                IZnodeRepository<ZnodePortal> _portalRepository = new ZnodeRepository<ZnodePortal>();
                quote.StoreName = _portalRepository.Table?.FirstOrDefault(x => x.PortalId == quote.PortalId)?.StoreName;
                quote.LocaleId = GetLocaleIdFromHeader();
                quote.PublishCatalogId = GetCatalogId(quote.PortalId);
            }
        }

        //Map ShoppingCart related data.
        protected virtual void MapCartItemDataForQuote(QuoteResponseModel quoteModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(quoteModel))
            {
                IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();

                //Get shopping cart model by using omsQuoteId.
                quoteModel.ShoppingCartItems = _shoppingCartService.GetShoppingCart(new CartParameterModel
                {
                    LocaleId = GetLocaleIdFromHeader(),
                    PortalId = quoteModel.PortalId,
                    UserId = quoteModel.UserId,
                    PublishedCatalogId = quoteModel.PublishCatalogId > 0 ? quoteModel.PublishCatalogId : 0,
                    OmsQuoteId = quoteModel.OmsQuoteId
                }).ShoppingCartItems;

                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            }
        }

        //Map Quote history from histroy as well as notes.
        protected virtual void MapQuoteHistory(QuoteResponseModel quote)
        {
            try
            {
                if (IsNotNull(quote) && quote.OmsQuoteId > 0)
                {
                    IZnodeViewRepository<OrderHistoryModel> objStoredProc = new ZnodeViewRepository<OrderHistoryModel>();
                    objStoredProc.SetParameter("@QuoteId", quote?.OmsQuoteId, ParameterDirection.Input, DbType.Int32);
                    List<OrderHistoryModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetQuoteHistory @QuoteId").ToList();
                    ZnodeLogging.LogMessage("Quote history list count and OmsQuoteId to get Quote history: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { QuoteHistoryListCount = list?.Count, OmsQuoteId = quote?.OmsQuoteId });
                    quote.QuoteHistoryList = list;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                quote.QuoteHistoryList = new List<OrderHistoryModel>();
            }
        }

        //Add Quote Notes
        protected virtual void AddQuoteNote(OrderNotesModel quoteNotesModel)
        {
            if (!string.IsNullOrEmpty(quoteNotesModel?.Notes))
            {
                IZnodeRepository<ZnodeOmsNote> _omsNoteRepository = new ZnodeRepository<ZnodeOmsNote>();
                ZnodeOmsNote notes = _omsNoteRepository.Insert(quoteNotesModel.ToEntity<ZnodeOmsNote>());
                ZnodeLogging.LogMessage("QuoteNotesModel inserted having OmsNotesId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsNotesId = quoteNotesModel?.OmsNotesId });
                quoteNotesModel.OmsNotesId = notes.OmsNotesId;
            }
        }

        // Check for allowed territories.
        protected virtual bool IsAllowedTerritories(List<QuoteLineItemModel> cartItem) => cartItem.Where(w => w.IsAllowedTerritories == false).ToList().Count > 0;

        //to check Quote data is updated
        protected virtual bool IsQuoteDataUpdated(UpdateQuoteModel model)
        {
            bool IsQuoteDataUpdated = true;
            if (IsNotNull(model))
            {
                if (IsNull(model.QuoteHistory))
                {
                    IsQuoteDataUpdated = false;
                }

                if (!string.IsNullOrEmpty(model.AdditionalInstructions))
                {
                    IsQuoteDataUpdated = true;
                }
            }
            return IsQuoteDataUpdated;
        }

        //to save Quote history in database
        protected virtual void CreateQuoteHistory(AWCTOrderHistoryModel quoteHistoryModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(quoteHistoryModel))
            {
                throw new ZnodeException(ErrorCodes.NullModel, "Quote history model can not be null.");
            }

            if (quoteHistoryModel.OmsNotesId == 0)
            {
                quoteHistoryModel.OmsNotesId = null;
            }

            if (quoteHistoryModel.OrderAmount == 0)
            {
                quoteHistoryModel.OrderAmount = null;
            }

            IZnodeRepository<ZnodeOmsQuoteHistory> _quoteHistoryRepository = new ZnodeRepository<ZnodeOmsQuoteHistory>();
            _quoteHistoryRepository.Insert(quoteHistoryModel.ToEntity<ZnodeOmsQuoteHistory>());
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Save Quote Details for manage 
        protected virtual BooleanModel UpdateQuoteDetails(UpdateQuoteModel model)
        {
            try
            {
                ZnodeLogging.LogMessage("OmsQuoteId for ZnodeOmsQuote model :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, model?.OmsQuoteId);
                BooleanModel isQuoteDetailsUpdated = new BooleanModel();
                if (IsNotNull(model))
                {
                    ZnodeOmsQuote quote = MapUpdatedQuoteDetails(model);
                    if (IsNotNull(quote))
                    {
                        isQuoteDetailsUpdated.IsSuccess = false;//_omsQuoteRepository.Update(quote);
                    }
                }
                return isQuoteDetailsUpdated;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return new BooleanModel { IsSuccess = true, ErrorMessage = Admin_Resources.QuoteProcessingFailedError };
            }
        }

        //Updated Lineitem data
        protected virtual bool UpdateQuoteLineItem(UpdateQuoteModel quote)
        {
            if (IsNotNull(quote))
            {
                List<ZnodeOmsQuoteLineItem> quoteLineItems = _omsQuoteLineItemRepository.Table.Where(x => x.OmsQuoteId == quote.OmsQuoteId && x.ParentOmsQuoteLineItemId != null).ToList();

                foreach (ZnodeOmsQuoteLineItem lineineItem in quoteLineItems)
                {
                    QuoteLineItemModel item = quote.QuoteLineItems.FirstOrDefault(x => x.OmsQuoteLineItemId == lineineItem.OmsQuoteLineItemId);
                    if (IsNotNull(item))
                    {
                        ZnodeOmsQuoteLineItem znodeOmsQuoteLineItem = quoteLineItems.FirstOrDefault(x => x.OmsQuoteLineItemId == lineineItem.OmsQuoteLineItemId);
                        znodeOmsQuoteLineItem.Price = item.Price;
                        znodeOmsQuoteLineItem.Quantity = item.Quantity;
                        // znodeOmsQuoteLineItem.ShippingCost = item.ShippingCost;
                        znodeOmsQuoteLineItem.ModifiedBy = quote.ModifiedBy;
                        znodeOmsQuoteLineItem.ModifiedDate = quote.ModifiedDate;
                        _omsQuoteLineItemRepository.Update(znodeOmsQuoteLineItem);
                    }
                    else if (lineineItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)
                    {
                        DeleteQuoteLineItemFromDataBase(lineineItem, quote);
                    }
                }
                return true;
            }
            return false;
        }

        //Delete line Item from Database
        protected virtual bool DeleteQuoteLineItemFromDataBase(Libraries.Data.DataModel.ZnodeOmsQuoteLineItem lineineItem, UpdateQuoteModel quote)
        {
            IZnodeRepository<Libraries.Data.DataModel.ZnodeOmsQuoteLineItem> _omsQuoteLineItemRepository = new ZnodeRepository<Libraries.Data.DataModel.ZnodeOmsQuoteLineItem>();
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            bool isDeleteLineItem = false;
            FilterCollection filters = new FilterCollection();

            List<int> quoteLineItem = _omsQuoteLineItemRepository.Table.Where(x => x.ParentOmsQuoteLineItemId == lineineItem.OmsQuoteLineItemId || x.OmsQuoteLineItemId == lineineItem.OmsQuoteLineItemId).Select(d => d.OmsQuoteLineItemId).ToList();
            ZnodeLogging.LogMessage("quoteLineItem list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { quoteLineItemListCount = quoteLineItem?.Count });

            if (IsNotNull(quoteLineItem))
            {
                filters.Add(new FilterTuple(ZnodeOmsQuoteLineItemEnum.OmsQuoteLineItemId.ToString(), ProcedureFilterOperators.In, string.Join(",", lineineItem.ParentOmsQuoteLineItemId, string.Join(",", quoteLineItem.ToArray()))));
            }
            else
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidParentQuoteLineItemId);
            }

            isDeleteLineItem = _omsQuoteLineItemRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);

            if (!isDeleteLineItem)
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorQuoteLineItemDelete);
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return isDeleteLineItem;
        }

        //Bind quote amount 
        protected virtual decimal BindQuoteAmount(UpdateQuoteModel model)
        {
            decimal quoteAmount = 0;// _omsQuoteRepository.GetById(model.OmsQuoteId).QuoteOrderTotal.GetValueOrDefault() - model.QuoteTotal;

            return quoteAmount;
        }

        //This method will send email to relevant user about quote creation.          
        protected virtual void SendQuoteReceiptEmailToUser(int userId, int portalId, string quoteNumber, int localeId, QuoteCreateModel quoteCreateModel, ZnodeOmsQuote quote)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { portalId = portalId, quoteNumber = quoteNumber, localeId = localeId });

            if (userId > 0 && portalId > 0 && localeId > 0)
            {
                UserModel userDetails = GetUserNameByUserId(userId);
                if (IsNull(userDetails))

                {
                    userDetails = (from user in _userRepository.Table
                                   where user.UserId == userId
                                   select new UserModel
                                   {
                                       FirstName = user.FirstName,
                                       LastName = user.LastName,
                                       UserName = user.Email,
                                       PhoneNumber = user.PhoneNumber,
                                       Email = user.Email
                                   })?.FirstOrDefault();
                }
                if (IsNotNull(userDetails))
                {
                    string customerName = $"{userDetails?.FirstName} {userDetails?.LastName}";
                    if (string.IsNullOrEmpty(customerName.Trim()))
                    {
                        customerName = userDetails?.UserName;
                    }

                    
                    PortalModel portalModel = GetCustomPortalDetails(portalId);
                    if (IsNotNull(portalModel))
                    {
                        //QuoteRequestAcknowledgementNotificationForCustomer
                        EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("QuoteRequestAcknowledgementNotificationForCustomer", portalId, localeId);

                        if (IsNotNull(emailTemplateMapperModel))
                        {

                            string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, "");
                            int QuoteId = quote.OmsQuoteId;
                            receiptContent = ReplaceTokenWithMessageText("#UserName#", customerName, receiptContent);
                            receiptContent = ReplaceTokenWithMessageText(ZnodeConstant.QuoteId, quoteNumber, receiptContent);
                            string messagetext = EmailTemplateHelper.ReplaceTemplateTokens(GetQuoteReceiptHtml(receiptContent, QuoteId, userDetails.Email, userDetails.UserName));

                            string subject = $"{emailTemplateMapperModel?.Subject} - {ZnodeConfigManager.SiteConfig.StoreName}";

                            
                            //Send  mail to user.
                            SendEmail(customerName, userDetails?.Email, subject, messagetext, portalId, emailTemplateMapperModel.IsEnableBcc);
                        }
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //This method will send email to relevant user about quote Conversion to Order.          
        protected virtual void SendQuoteConvertToOrderMail(int userId, int portalId, string quoteNumber, string orderNumber, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { portalId = portalId, quoteNumber = quoteNumber, localeId = localeId });

            if (userId > 0)
            {
                UserModel userDetails = GetUserNameByUserId(userId);
                if (IsNotNull(userDetails))
                {
                    string customerName = $"{userDetails?.FirstName} {userDetails?.LastName}";
                    if (string.IsNullOrEmpty(customerName.Trim()))
                    {
                        customerName = userDetails?.UserName;
                    }

                    PortalModel portalModel = GetCustomPortalDetails(portalId);
                    if (IsNotNull(portalModel))
                    {
                        EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("QuoteConvertedToOrderNotificationForCustomer", portalId, localeId);

                        if (IsNotNull(emailTemplateMapperModel))
                        {
                            string subject = $"{emailTemplateMapperModel?.Subject} - {ZnodeConfigManager.SiteConfig.StoreName}";

                            string messageText = emailTemplateMapperModel?.Descriptions;
                            messageText = ReplaceTokenWithMessageText(ZnodeConstant.StoreLogo, portalModel.StoreLogo, messageText);
                            messageText = ReplaceTokenWithMessageText("#UserName#", customerName, messageText);
                            messageText = ReplaceTokenWithMessageText(ZnodeConstant.QuoteId, quoteNumber, messageText);
                            messageText = ReplaceTokenWithMessageText("#OrderNo#", orderNumber, messageText);
                            messageText = ReplaceTokenWithMessageText(ZnodeConstant.StoreName, portalModel.StoreName, messageText);
                            //Send  mail to user.
                            SendEmail(customerName, userDetails?.Email, subject, messageText, portalId, emailTemplateMapperModel.IsEnableBcc);
                        }
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Send Email.
        protected virtual void SendEmail(string userName, string email, string subject, string messageText, int portalId, bool isEnableBcc)
        {
            ZnodeEmail.SendEmail(portalId, email, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, string.Empty), subject, messageText, true);
        }

        //Get product details
        protected virtual DataTable GetProductDetails(QuoteCreateModel quoteCreateModel)
        {
            if (IsNotNull(quoteCreateModel) && IsNotNull(quoteCreateModel.productDetails))
            {

                foreach (ProductDetailModel childLineItem in quoteCreateModel.productDetails)
                {
                    string Personalisevalue = Convert.ToString(childLineItem.Custom3);
                    string[] PersonalisevalueDetails = Personalisevalue.Split('-');

                    if (PersonalisevalueDetails[1] == "TRU")
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(childLineItem.Custom4)))
                        {
                            childLineItem.Custom4 = childLineItem.Custom5;
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(childLineItem.Custom5)))
                        {
                            childLineItem.Custom4 = "Guard";
                        }

                    }
                }
                return GetProductPriceDetailsForSP(quoteCreateModel.productDetails);
            }
            return new DataTable();
        }

        // Get Product PriceDetails For SP in table format
        protected virtual DataTable GetProductPriceDetailsForSP(List<ProductDetailModel> cartItem)
        {
            DataTable table = new DataTable("SKUPriceForQuote");
            table.Columns.Add(ZnodeConstant.ProductSKU, typeof(string));
            table.Columns.Add("OmsSavedCartLineItemId", typeof(int));
            table.Columns.Add(ZnodeConstant.Price, typeof(decimal));
            table.Columns.Add("ShippingCost", typeof(decimal));
            table.Columns.Add("PersonaliseValue", typeof(string));

            foreach (ProductDetailModel item in cartItem)
            {
                table.Rows.Add(item.SKU, item.OmsSavedcartLineItemId, item.Price, item.ShippingCost, item.Custom4);
            }

            return table;
        }

        //Map Update Details of Quote
        protected virtual ZnodeOmsQuote MapUpdatedQuoteDetails(UpdateQuoteModel model)
        {
            if (IsNotNull(model))
            {
                ZnodeOmsQuote quote = null;// _omsQuoteRepository.Table.FirstOrDefault(w => w.OmsQuoteId == model.OmsQuoteId);
                if (IsNotNull(quote))
                {
                    quote.OmsOrderStateId = (model.OmsQuoteStateId <= 0) ? quote.OmsOrderStateId : model.OmsQuoteStateId;
                    quote.ShippingId = (model.ShippingId <= 0) ? quote.ShippingId : model.ShippingId;
                    quote.ShippingCost = (IsNull(model.ShippingCost)) ? quote.ShippingCost : model.ShippingCost;
                    quote.ShippingAddressId = (model.ShippingAddressId <= 0) ? quote.ShippingAddressId : model.ShippingAddressId;
                    quote.BillingAddressId = (model.BillingAddressId <= 0) ? quote.BillingAddressId : model.BillingAddressId;
                    quote.QuoteOrderTotal = (model.QuoteTotal <= 0) ? quote.QuoteOrderTotal : model.QuoteTotal;
                    quote.ModifiedBy = GetLoginUserId() > 0 ? GetLoginUserId() : quote.ModifiedBy;
                    quote.ModifiedDate = DateTime.Now;
                    quote.TaxCost = (IsNull(model.TaxCost)) ? quote.TaxCost : model.TaxCost;
                    quote.InHandDate = IsNull(model?.InHandDate) ? quote.InHandDate : model.InHandDate;
                    quote.QuoteExpirationDate = IsNull(model?.QuoteExpirationDate) ? Convert.ToDateTime(quote.QuoteExpirationDate) : Convert.ToDateTime(model.QuoteExpirationDate);
                    quote.ShippingTypeId = (model.ShippingTypeId <= 0) ? quote.ShippingTypeId : model.ShippingTypeId;
                    quote.AccountNumber = string.IsNullOrEmpty(model.AccountNumber) ? quote.AccountNumber : model.AccountNumber;
                    quote.ShippingMethod = string.IsNullOrEmpty(model.ShippingMethod) ? quote.ShippingMethod : model.ShippingMethod;
                    quote.JobName = string.IsNullOrEmpty(model.JobName) ? quote.JobName : model.JobName;
                    quote.ShippingConstraintCode = string.IsNullOrEmpty(model.ShippingConstraintCode) ? quote.ShippingConstraintCode : model.ShippingConstraintCode;
                    quote.IsTaxExempt = IsNull(model.IsTaxExempt) ? quote.IsTaxExempt : model.IsTaxExempt;
                }
                return quote;
            }
            return new ZnodeOmsQuote();
        }
        //Get Quote Note Details
        protected virtual List<OrderHistoryModel> GetQuoteNotes(int omsQuoteId)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { quoteId = omsQuoteId });
            try
            {
                if (omsQuoteId > 0)
                {
                    IZnodeViewRepository<OrderHistoryModel> objStoredProc = new ZnodeViewRepository<OrderHistoryModel>();
                    objStoredProc.SetParameter("@QuoteId", omsQuoteId, ParameterDirection.Input, DbType.Int32);
                    List<OrderHistoryModel> noteList = objStoredProc.ExecuteStoredProcedureList("Znode_GetOmsQuoteNotesList @QuoteId").ToList();
                    ZnodeLogging.LogMessage("Quote Notes list count and OmsQuoteId to get Quote history: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { QuoteHistoryListCount = noteList?.Count, OmsQuoteId = omsQuoteId });
                    return noteList;
                }
                return new List<OrderHistoryModel>();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return new List<OrderHistoryModel>();
            }
        }

        //Process Payment
        protected virtual bool ProcessPayment(ConvertQuoteToOrderModel convertToOrderModel, ShoppingCartModel shoppingCartModel)
        {
            bool isPaymentSuccess = false;
            if (IsCODPaymentType(convertToOrderModel.PaymentDetails.PaymentType) || IsPOPaymentType(convertToOrderModel.PaymentDetails.PaymentType))
            {
                if (IsPOPaymentType(convertToOrderModel.PaymentDetails.PaymentType))
                {
                    shoppingCartModel.PurchaseOrderNumber = convertToOrderModel.PaymentDetails.PurchaseOrderNumber;
                    shoppingCartModel.PODocumentName = !string.IsNullOrEmpty(convertToOrderModel.PaymentDetails.PODocumentName) ? $"{"~/Data/Media/PODocument"}/{convertToOrderModel.PaymentDetails.PODocumentName}" : null;
                }
                isPaymentSuccess = true;
            }
            else
            {
                IPaymentHelper paymentHelper = GetService<IPaymentHelper>();
                isPaymentSuccess = paymentHelper.ProcessPayment(convertToOrderModel, shoppingCartModel);
            }

            return isPaymentSuccess;
        }

        //This method will send email to admin user about new quote request        .          
        protected virtual void SendQuoteReceiptEmailToAdmin(int portalId, string quoteNumber, int localeId, QuoteCreateModel quoteCreateModel, ZnodeOmsQuote quote)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { portalId = portalId, quoteNumber = quoteNumber, localeId = localeId });

            if (quoteCreateModel.UserId > 0 && portalId > 0 && localeId > 0)
            {
               
                    UserModel userDetails = GetUserNameByUserId(quoteCreateModel.UserId);
                    if (IsNull(userDetails))

                    {
                        userDetails = (from user in _userRepository.Table
                                       where user.UserId == quoteCreateModel.UserId
                                       select new UserModel
                                       {
                                           FirstName = user.FirstName,
                                           LastName = user.LastName,
                                           UserName = user.Email,
                                           PhoneNumber = user.PhoneNumber,
                                           Email = user.Email
                                       })?.FirstOrDefault();
                    }
                
                PortalModel portalModel = GetCustomPortalDetails(portalId);
                if (IsNotNull(portalModel))
                {
                    //NewQuoteRequestNotificationForAdmin
                    EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("NewQuoteRequestNotificationForAdmin", portalId, localeId);

                    if (IsNotNull(emailTemplateMapperModel))
                    {
                        string subject = $"{emailTemplateMapperModel?.Subject} - {ZnodeConfigManager.SiteConfig.StoreName}";
                        string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, "");
                        int QuoteId = quote.OmsQuoteId;
                        receiptContent = ReplaceTokenWithMessageText(ZnodeConstant.QuoteId, quoteNumber, receiptContent);

                        string messagetext = EmailTemplateHelper.ReplaceTemplateTokens(GetQuoteReceiptHtml(receiptContent, QuoteId, userDetails.Email, userDetails.UserName));
                        
                        //Send  mail to admin.                        
                        ZnodeEmail.SendEmail(portalId, ZnodeConfigManager.SiteConfig.CustomerServiceEmail, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), subject, messagetext, true);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Adds date time value in filter collection against created date column.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <param name="filterName">Filter Name</param>
        /// <returns>returns filters</returns>
        private FilterCollection AddDateTimeValueInFilterByName(FilterCollection filters, string filterName)
        {
            if (filters?.Count > 0)
            {
                string dateTimeRange = filters.FirstOrDefault(x => string.Equals(x.FilterName, DateTimeRange, StringComparison.InvariantCultureIgnoreCase))?.FilterValue;
                //Update filters if _dateTimeRange is not null.
                if (!string.IsNullOrEmpty(dateTimeRange))
                {
                    filters.RemoveAll(x => string.Equals(x.FilterName, filterName, StringComparison.InvariantCultureIgnoreCase));
                    filters.Add(new FilterTuple(filterName, FilterOperators.Between, dateTimeRange));
                    filters.RemoveAll(x => string.Equals(x.FilterName, DateTimeRange, StringComparison.InvariantCultureIgnoreCase));
                }
            }
            return filters;
        }
        //get subtotal for cart
        private decimal GetQuoteSubTotal(List<ShoppingCartItemModel> ShoppingCartItems)
        {
            return Convert.ToDecimal(ShoppingCartItems?.Sum(x => x.ExtendedPrice));

        }

        //SetPageFilter if not set.
        private void SetPageFilter(NameValueCollection page)
        {
            if (string.IsNullOrEmpty(page.Get("Index")))
            {
                page.Set("Index", "1");
            }
            if (string.IsNullOrEmpty(page.Get("size")))
            {
                page.Set("size", "50");
            }
        }

        //Get saved cart id from cookieMappingId, userId and PortalId
        private int GetSavedCartId(QuoteCreateModel quoteCreateModel, ref int cookieMappingId)
        {
            IZnodeOrderHelper znodeOrderHelper = GetService<IZnodeOrderHelper>();

            int cookieId = !string.IsNullOrEmpty(quoteCreateModel.CookieMappingId) ? Convert.ToInt32(new ZnodeEncryption().DecryptData(quoteCreateModel.CookieMappingId)) : 0;

            //Get CookieMappingId
            cookieMappingId = cookieId == 0 ? znodeOrderHelper.GetCookieMappingId(quoteCreateModel.UserId, quoteCreateModel.PortalId) : cookieId;

            return GetSavedCartId(ref cookieMappingId);
        }

        //Get Quote Status
        private string GetQuoteStatus(int quoteStateId)
        {
            string quoteStatus = string.Empty;
            if (quoteStateId > 0)
            {
                IZnodeRepository<ZnodeOmsOrderState> _orderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
                quoteStatus = _orderStateRepository.Table.FirstOrDefault(x => x.OmsOrderStateId == quoteStateId)?.Description;
            }
            return quoteStatus;
        }

        //Get default currency assigned to current portal.
        private void SetPortalDefaultCurrencyCultureCode(int portalId, QuoteResponseModel quote)
        {
            IZnodeRepository<ZnodePortalUnit> _portalUnitRepository = new ZnodeRepository<ZnodePortalUnit>();
            FilterCollection filter = new FilterCollection();
            filter.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, portalId.ToString());

            NameValueCollection expand = new NameValueCollection();
            expand.Add(ZnodePortalUnitEnum.ZnodeCurrency.ToString(), ZnodePortalUnitEnum.ZnodeCurrency.ToString());
            expand.Add(ZnodePortalUnitEnum.ZnodeCulture.ToString(), ZnodePortalUnitEnum.ZnodeCulture.ToString());

            ZnodePortalUnit portalUnit = _portalUnitRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause, GetExpands(expand));
            quote.CultureCode = portalUnit?.ZnodeCulture?.CultureCode;
        }

        //Get expands and add them to navigation properties.
        private List<string> GetExpands(NameValueCollection expands)
        {
            List<string> navigationProperties = new List<string>();
            if (!Equals(expands, null) && expands.HasKeys())
            {
                foreach (string key in expands.Keys)
                {
                    //Add expand keys
                    if (Equals(key.ToLower(), ZnodePortalUnitEnum.ZnodeCurrency.ToString().ToLower()))
                    {
                        SetExpands(ZnodePortalUnitEnum.ZnodeCurrency.ToString(), navigationProperties);
                    }

                    if (Equals(key.ToLower(), ZnodePortalUnitEnum.ZnodeCulture.ToString().ToLower()))
                    {
                        SetExpands(ZnodePortalUnitEnum.ZnodeCulture.ToString(), navigationProperties);
                    }
                }
            }
            ZnodeLogging.LogMessage("NavigationProperties list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, navigationProperties?.Count);
            return navigationProperties;
        }

        //Get published catalog Id
        private int GetPublishCatalogId(int userId, int portalId)
        {
            ZnodeLogging.LogMessage("UserId of input AccountQuoteModel:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { UserId = userId });
            if (userId > 0)
            {
                //Get accountId on basis of UserId
                int? accountId = _userRepository.Table.FirstOrDefault(x => x.UserId == userId)?.AccountId;
                ZnodeLogging.LogMessage("AccountId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, accountId);
                if (accountId > 0)
                {
                    //Get account details on basis of accountId and get catalogId
                    ZnodeAccount accountDetails = _accountRepository.GetById(accountId.GetValueOrDefault());

                    if (accountDetails?.PublishCatalogId > 0)
                    {
                        return accountDetails.PublishCatalogId.GetValueOrDefault();
                    }
                    //If account not present then looking for parent accountId and get catalogId
                    else if (accountDetails?.ParentAccountId > 0)
                    {
                        ZnodeAccount parentAccountDetails = _accountRepository.GetById(accountDetails.ParentAccountId.GetValueOrDefault());
                        if (parentAccountDetails?.PublishCatalogId > 0)
                        {
                            return parentAccountDetails.PublishCatalogId.GetValueOrDefault();
                        }
                    }
                }
            }
            //if userId is 0 then get catalogId on basis of portal
            return GetCatalogId(portalId);
        }

        //If catalog is not present for account then get catalog id on basis of portalId
        private int GetCatalogId(int portalId)
        {
            int publishCatalogId = 0;
            if (portalId > 0)
            {
                int? portalCatalogId = _portalCatalogRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.PublishCatalogId;
                ZnodeLogging.LogMessage("portalCatalogId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, portalCatalogId);
                if (portalCatalogId > 0)
                {
                    publishCatalogId = portalCatalogId.GetValueOrDefault();
                }
            }
            return publishCatalogId;
        }

        //Get Portal name on the basis of portal id.
        private string GetPortalName(int portalId)
        {
            if (portalId > 0)
            {
                IZnodeRepository<ZnodePortal> _znodePortal = new ZnodeRepository<ZnodePortal>();
                return _znodePortal.Table.FirstOrDefault(x => x.PortalId == portalId)?.StoreName;
            }
            return string.Empty;
        }


        //Get data from filters.
        private int GetDataFromFilters(FilterCollection filters, string filterName)
        {
            int filterId = 0;
            if (filters.Exists(x => x.FilterName.Equals(filterName, StringComparison.InvariantCultureIgnoreCase)))
            {
                filterId = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, filterName, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            }
            return filterId;
        }

        //Check if the Quote is Valid For Convert To an Order
        private bool IsQuoteValidForConvertToOrder(DateTime? quoteExpirationDate, int quoteStatusId, bool isConvertedToOrder)
        {
            string quoteStatus = GetQuoteStatus(quoteStatusId);
            return (IsNotNull(quoteExpirationDate) && quoteExpirationDate == DateTime.Now ||
                    (string.Equals(quoteStatus, "EXPIRED", StringComparison.InvariantCultureIgnoreCase)
                    //|| string.Equals(quoteStatus, ZnodeOrderStatusEnum.SUBMITTED.ToString(), StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(quoteStatus, ZnodeOrderStatusEnum.APPROVED.ToString(), StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(quoteStatus, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    || isConvertedToOrder) ? false : true;
        }

        private bool IsCODPaymentType(string paymentType)
        {
            return (!string.IsNullOrEmpty(paymentType) && (string.Equals(paymentType, ZnodeConstant.COD.ToString(), StringComparison.InvariantCultureIgnoreCase)));
        }

        private bool IsCreditCardPayment(string paymentType)
        {
            return string.Equals(paymentType, ZnodeConstant.CreditCard, StringComparison.InvariantCultureIgnoreCase);
        }

        private bool IsPOPaymentType(string paymentType)
        {
            return (!string.IsNullOrEmpty(paymentType) && (string.Equals(paymentType, ZnodeConstant.PurchaseOrder.ToString(), StringComparison.InvariantCultureIgnoreCase)));
        }

        //to get savedcartId by cookieMappingId
        public virtual int GetSavedCartId(ref int cookieMappingId, int portalId = 0, int? userId = 0)
        {
            int? localMappingId = cookieMappingId;
            if (cookieMappingId > 0)
            {
                IZnodeRepository<ZnodeOmsSavedCart> _savedCartRepository = new ZnodeRepository<ZnodeOmsSavedCart>();
                ZnodeOmsSavedCart savedCart = _savedCartRepository.Table.FirstOrDefault(x => x.OmsCookieMappingId == localMappingId);
                if (HelperUtility.IsNull(savedCart))
                {

                    ZnodeOmsCookieMapping cookieMapping = _cookieMappingRepository.Table.FirstOrDefault(x => x.OmsCookieMappingId == localMappingId);
                    if (HelperUtility.IsNull(cookieMapping) || cookieMapping.OmsCookieMappingId <= 0)
                    {
                        cookieMappingId = CreateCookieMappingId(userId, portalId);
                    }
                    //savedcartId not exist for cookieMappingId then create new.
                    ZnodeOmsSavedCart newCart = _savedCartRepository.Insert(new ZnodeOmsSavedCart()
                    {
                        OmsCookieMappingId = cookieMappingId,
                        CreatedDate = HelperUtility.GetDateTime(),
                        ModifiedDate = HelperUtility.GetDateTime()
                    });
                    return newCart.OmsSavedCartId;
                }
                return Convert.ToInt32(savedCart.OmsSavedCartId);
            }
            return 0;
        }
        //create new cookiemappingid for cart
        protected virtual int CreateCookieMappingId(int? userId, int portalId)
        {
            ZnodeOmsCookieMapping cookieMapping = _cookieMappingRepository.Insert(new ZnodeOmsCookieMapping()
            {
                UserId = userId == 0 ? null : userId,
                CreatedDate = HelperUtility.GetDateTime(),
                ModifiedDate = HelperUtility.GetDateTime(),
                PortalId = portalId
            });
            return Convert.ToInt32(cookieMapping?.OmsCookieMappingId);
        }

        private QuoteResponseModel GetQuoteFromDBById(int QuoteId)
        {
            //SP call to save/update savedCartLineItem
            int RowCount = 0;
            IZnodeViewRepository<QuoteResponseModel> objStoredProc = new ZnodeViewRepository<QuoteResponseModel>();
            objStoredProc.SetParameter("QuoteId", QuoteId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("RowCount", RowCount, ParameterDirection.Output, DbType.Int32);
            QuoteResponseModel omsQuote = objStoredProc.ExecuteStoredProcedureList("AWCT_GetQuotebyId @QuoteId,@RowCount OUT", 1, out RowCount).FirstOrDefault();
            return omsQuote;
        }
        #endregion

        #region LoadFromDatabaseQuote From ZNodeShoppingCart
        //For Quote
        //to load shoppingCart from database by QuoteId
        public virtual ShoppingCartModel LoadCartFromQuote(CartParameterModel model, QuoteResponseModel quoteModel, int? catalogVersionId = null)
        {
            ShoppingCartModel cartModel;
            List<ZnodeOmsQuoteLineItem> parentDetails = null;

            //Check if QuoteId is null or 0.
            if (IsNull(model.OmsQuoteId) || model.OmsQuoteId == 0)
            {
                return null;
            }
            ZnodeOmsQuote quote = quoteModel.ToEntity<ZnodeOmsQuote>();
            cartModel = quote?.ToEntity<ShoppingCartModel>() ?? new ShoppingCartModel();

            List<QuoteLineItemModel> ZnodeOmsQuoteLineItem = GetQuoteLineItems(cartModel.OmsQuoteId);
            AWCTZnodeShoppingCart aWCTZnodeShoppingCart = new AWCTZnodeShoppingCart();
            string PersonalizeValue = null;
            string EstimatedShipdate = null;
            List<AccountQuoteLineItemModel> accountQuoteLineItemModels = aWCTZnodeShoppingCart.GetQuoteLineItemsById(model);
            //accountQuoteLineItemModels.ForEach(x =>
            //{

            //    string[] PersonaliseValues = x.ProductName.Split('|');
            //    //x.Custom4 = PersonaliseValues[4];
            //    PersonalizeValue = PersonaliseValues[4];

            //    EstimatedShipdate = PersonaliseValues[3];
            //});


            List<ZnodeOmsQuoteLineItem> allquoteLineItems = quotehelper.GetQuoteLineItemByQuoteId(cartModel.OmsQuoteId);

            allquoteLineItems.ForEach(x =>
            {
                string ProductName = accountQuoteLineItemModels.Where(y => y.SKU == x.SKU).Select(y => y.ProductName).FirstOrDefault().ToString();
                string[] PersonaliseValues = ProductName.Split('|');
                //x.Custom4 = PersonaliseValues[4];
                PersonalizeValue = PersonaliseValues[4];

                EstimatedShipdate = PersonaliseValues[3];

                if (x.Description.Contains("True Colors"))
                {
                    x.CustomText = PersonalizeValue + '|' + EstimatedShipdate + '|';
                }
                else
                {
                    x.CustomText = PersonalizeValue + '|' + EstimatedShipdate + '|';
                }

            });
            List<ZnodeOmsQuoteLineItem> quoteLineItems = allquoteLineItems?
                                                     .Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns)
                                                     && m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)).ToList();


            List<ProductEntity> productList = publishProductHelper.GetPublishProductBySKUs(string.Join(",", quoteLineItems?.Select(x => x.SKU)), model.PublishedCatalogId, model.LocaleId, catalogVersionId)?.ToModel<ProductEntity>()?.ToList();

            cartModel.ShoppingCartItems = new List<ShoppingCartItemModel>();
            parentDetails = quoteLineItems.Where(o => o.ParentOmsQuoteLineItemId == null).ToList();
            SetParentLineItemDetailsForQuote(parentDetails, productList);

            foreach (ZnodeOmsQuoteLineItem lineItem in quoteLineItems.Where(quoteLineItem => quoteLineItem.ParentOmsQuoteLineItemId.HasValue))
            {
                ShoppingCartItemModel item = lineItem.ToModel<ShoppingCartItemModel>();
                ProductEntity product = productList?.FirstOrDefault(x => x.SKU == item.SKU);
                if (lineItem.ParentOmsQuoteLineItemId == 0)
                {
                    item.ProductType = ZnodeConstant.BundleProduct;
                }

                item.IsActive = (product?.IsActive).GetValueOrDefault();
                item.CartDescription = Convert.ToString(lineItem.CustomText) + "Collection" + " - " + product?.Attributes?.FirstOrDefault(x => x.AttributeCode == "Season")?.SelectValues[0]?.Value + "<br>" + item.CartDescription;
                //item.Description = "Collection" + " - " + product?.Attributes?.FirstOrDefault(x => x.AttributeCode == "Season")?.SelectValues[0]?.Value + "<br>" + item.CartDescription;
                //to set configurable/group product quantity for cart line item



                SetConfigurableOrGroupProductQuantityForQuote(item, new List<ZnodeOmsQuoteLineItem>() { lineItem }, productList);
                CalculateLineItemPriceForQuote(item, allquoteLineItems);
                SetAssociateQuoteProductType(item, allquoteLineItems);
                SetProductImage(item, model.PublishedCatalogId, model.LocaleId, model.OmsOrderId.GetValueOrDefault());

                SetGroupAndConfigurableParentProductDetailsOfQuote(parentDetails, lineItem, item);

                SetInventoryData(item, product);

                string style = product.Attributes?.FirstOrDefault(x => x.AttributeCode == "awctstyle")?.AttributeValues;
                product.Name = product.Name.Contains(style) ? product.Name : style + " " + product.Name;
                string AddonProduct = Convert.ToString(item.CustomText);
                if (Convert.ToString(AddonProduct).Contains("AddOn"))
                {
                    item.ProductName = product.Name;
                }
                cartModel.ShoppingCartItems.Add(item);
            }
            return cartModel;
        }
        private void SetParentLineItemDetailsForQuote(List<ZnodeOmsQuoteLineItem> parentDetails, List<ProductEntity> productList)
        {
            if (productList.Count > 0)
            {
                parentDetails?.ForEach(item =>
                {
                    string productType = productList.FirstOrDefault(x => x.SKU == item.SKU)?.Attributes?
                   .Where(x => x.AttributeCode == ZnodeConstant.ProductType)?
                   .Select(x => x.SelectValues?
                   .Select(m => m.Code)?.FirstOrDefault())?.FirstOrDefault();

                    if (productType == ZnodeConstant.BundleProduct)
                    {
                        item.ParentOmsQuoteLineItemId = 0;
                    }
                });
            }
        }
        //to set Configurable/group product quantity for quote product
        private void SetConfigurableOrGroupProductQuantityForQuote(ShoppingCartItemModel item, List<ZnodeOmsQuoteLineItem> childItems, List<ProductEntity> productList)
        {
            if (item.Quantity == 0)//to set Configurable product quantity
            {
                if (Convert.ToDecimal(childItems.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable)?.Select(s => s.Quantity)) > 0)
                {
                    SetChildItemDataForQuote(item, (int)ZnodeCartItemRelationshipTypeEnum.Configurable, childItems, productList);
                }
                else
                {
                    SetChildItemDataForQuote(item, (int)ZnodeCartItemRelationshipTypeEnum.Group, childItems, productList);
                }
            }
            else if (IsNotNull(childItems.FirstOrDefault(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns)))
            {
                item.AddOnLineItemId = childItems.FirstOrDefault().OmsQuoteLineItemId;
                SetChildItemDataForQuote(item, (int)ZnodeCartItemRelationshipTypeEnum.AddOns, childItems, productList);
            }
        }

        private void SetChildItemDataForQuote(ShoppingCartItemModel item, int relationTypeId, List<ZnodeOmsQuoteLineItem> childItems, List<ProductEntity> productList)
        {
            ZnodeOmsQuoteLineItem child = childItems.FirstOrDefault(x => x.OrderLineItemRelationshipTypeId == relationTypeId);
            if (IsNotNull(child))
            {
                ProductEntity childProduct = productList?.FirstOrDefault(x => x.SKU == child.SKU);
                SetInventoryData(item, childProduct);
                item.IsActive = (productList?.FirstOrDefault(x => x.SKU == child.SKU)?.IsActive).GetValueOrDefault();
                item.Quantity = child.Quantity.GetValueOrDefault();
                item.ChildProductId = child.OmsQuoteLineItemId;
            }
        }
        private void SetInventoryData(ShoppingCartItemModel item, ProductEntity product)
        {
            if (IsNotNull(product))
            {
                List<SelectValuesEntity> inventorySettingList = product.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues;
                string inventorySettingCode = inventorySettingList?.FirstOrDefault().Code;
                if (string.Equals(ZnodeConstant.DontTrackInventory, inventorySettingCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    item.TrackInventory = false;
                }
                else if (string.Equals(ZnodeConstant.AllowBackOrdering, inventorySettingCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    item.AllowBackOrder = true;
                    item.TrackInventory = false;
                }
                else
                {
                    item.TrackInventory = true;
                }
            }
        }
        //to calculate unit price and extended price for quote lineitems
        private void CalculateLineItemPriceForQuote(ShoppingCartItemModel lineItem, List<ZnodeOmsQuoteLineItem> childItems)
        {
            if (IsNotNull(childItems.FirstOrDefault(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable)) || IsNotNull(childItems.FirstOrDefault(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)))
            {
                lineItem.UnitPrice = lineItem.UnitPrice > 0 ? lineItem.UnitPrice :
                    childItems.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable
                    || x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group).Sum(x => x.Price).GetValueOrDefault();
            }

            if (IsNotNull(childItems.FirstOrDefault(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns)))
            {
                lineItem.UnitPrice += childItems
                                    .Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                                    && x.ParentOmsQuoteLineItemId == lineItem.OmsQuoteLineItemId
                                    ).Sum(x => x.Price).GetValueOrDefault();
            }

            lineItem.ExtendedPrice = lineItem.UnitPrice * lineItem.Quantity;
            //to set externalid of line item
            lineItem.ExternalId = Guid.NewGuid().ToString();
        }

        //to set product type data for shoppingcart line item 
        private void SetAssociateQuoteProductType(ShoppingCartItemModel lineItem, List<ZnodeOmsQuoteLineItem> childItems)
        {
            lineItem.AddOnProductSKUs = string.Join(",", childItems
                                        .Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                                         && x.ParentOmsQuoteLineItemId == lineItem.OmsOrderLineItemsId).AsEnumerable().Select(b => b.SKU));
            lineItem.BundleProductSKUs = string.Join(",", childItems.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Bundles
                                         && x.ParentOmsQuoteLineItemId == lineItem.OmsOrderLineItemsId).AsEnumerable().Select(b => b.SKU));
            lineItem.ConfigurableProductSKUs = string.Join(",", childItems.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable && x.OmsQuoteLineItemId == lineItem.OmsOrderLineItemsId).AsEnumerable().Select(b => b.SKU));
            lineItem.GroupProducts = childItems.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group
                                                   && x.OmsQuoteLineItemId == lineItem.OmsOrderLineItemsId)?.ToModel<AssociatedProductModel>()?.ToList();
        }

        //Set Parent Product Name for Group Product
        private void SetGroupAndConfigurableParentProductDetailsOfQuote(List<ZnodeOmsQuoteLineItem> parentDetails, ZnodeOmsQuoteLineItem lineItem, ShoppingCartItemModel item)
        {
            if ((item.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group) && item.GroupProducts?.Count > 0) || (item.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Configurable) && !string.IsNullOrEmpty(item.ConfigurableProductSKUs)))
            {
                string parentName = parentDetails?.FirstOrDefault(p => p.OmsQuoteLineItemId == lineItem.ParentOmsQuoteLineItemId)?.ProductName;
                if (!string.IsNullOrEmpty(parentName))
                {
                    item.ProductName = parentName;

                }
                //Set Parent SKU for group Product.
                item.SKU = parentDetails?.FirstOrDefault(p => p.OmsQuoteLineItemId == lineItem.ParentOmsQuoteLineItemId)?.SKU;
            }
        }

        //to set set product image
        //to set set product image
        private void SetProductImage(ShoppingCartItemModel lineItem, int publishedCatalogId, int localeId, int omsOrderId = 0)
        {
            int catalogversionId = GetCatalogVersionId(publishedCatalogId, localeId);
            ProductEntity product = new ProductEntity();
            if (lineItem?.GroupProducts?.Count > 0)
            {
                product = publishProductHelper.GetPublishProductBySKU(lineItem.GroupProducts.FirstOrDefault().Sku, publishedCatalogId, localeId, catalogversionId, omsOrderId);
            }
            else
            {
                product = publishProductHelper.GetPublishProductBySKU(!string.IsNullOrEmpty(lineItem.ConfigurableProductSKUs) ? lineItem.ConfigurableProductSKUs : lineItem.SKU, publishedCatalogId, localeId, catalogversionId, omsOrderId);

            }
            if (IsNotNull(product))
            {

                lineItem.ImagePath = product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                //lineItem.ImagePath = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString() + ConfigurationManager.AppSettings["ImagePath"].ToString() + lineItem.ImagePath;
                lineItem.ImagePath = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString() + ConfigurationManager.AppSettings["ImagePath"].ToString() + lineItem.ImagePath;
                lineItem.MinQuantity = Convert.ToDecimal(product.Attributes?.Where(x => x.AttributeCode == ZnodeConstant.MinimumQuantity)?.FirstOrDefault()?.AttributeValues);
                lineItem.MaxQuantity = Convert.ToDecimal(product.Attributes?.Where(x => x.AttributeCode == ZnodeConstant.MaximumQuantity)?.FirstOrDefault()?.AttributeValues);
                lineItem.UOM = product.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.UOM)?.SelectValues?.FirstOrDefault()?.Value;
                if (lineItem?.GroupProducts?.Count > 0)
                {
                    lineItem.GroupProducts.Where(w => w.Sku == lineItem.GroupProducts.FirstOrDefault().Sku).ToList().ForEach(s => s.ProductId = product.ZnodeProductId);
                }
            }
        }
        //to get catalog version Id by  published catalog Id
        private int GetCatalogVersionId(int publishedCatalogId, int localeId = 0)
        {

            return publishProductHelper.GetCatalogVersionId(publishedCatalogId, localeId);

        }

        protected virtual ZnodeOmsQuote GetQuoteDetailsById(int OmsQuoteId)
        {

            ZnodeOmsQuote quoteDetails = new ZnodeOmsQuote();
            try
            {
                if (OmsQuoteId > 0)
                {
                    int RowCount = 0;
                    IZnodeViewRepository<ZnodeOmsQuote> objStoredProc = new ZnodeViewRepository<ZnodeOmsQuote>();
                    objStoredProc.SetParameter("OmsQuoteId", OmsQuoteId, ParameterDirection.Input, DbType.Int32);
                    objStoredProc.SetParameter("RowCount", RowCount, ParameterDirection.Output, DbType.Int32);
                    quoteDetails = objStoredProc.ExecuteStoredProcedureList("AWCT_GetQuotebyId @OmsQuoteId,@RowCount OUT", 1, out RowCount).FirstOrDefault();
                }
                return quoteDetails;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return quoteDetails;
            }

        }

        private string GetShippingRate(string StateName, string CountryName, decimal SubTotal)
        {
            IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();

            objStoredProc.SetParameter("@State", StateName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Country", CountryName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SubTotal", SubTotal, ParameterDirection.Input, DbType.Decimal);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("Nivi_AWCTShippingRate @State,@Country,@SubTotal").Take(1).First();
        }

        protected virtual ZnodeOmsQuote GetGuestQuoteDetailsById(string QuoteNumber, string EmailAddress)
        {

            ZnodeOmsQuote quoteDetails = new ZnodeOmsQuote();
            try
            {
                if (!string.IsNullOrEmpty(QuoteNumber) && !string.IsNullOrEmpty(EmailAddress))
                {
                    int RowCount = 0;
                    IZnodeViewRepository<ZnodeOmsQuote> objStoredProc = new ZnodeViewRepository<ZnodeOmsQuote>();
                    objStoredProc.SetParameter("QuoteNumber", QuoteNumber.Trim(), ParameterDirection.Input, DbType.String);
                    objStoredProc.SetParameter("EmailAddress", EmailAddress.Trim(), ParameterDirection.Input, DbType.String);
                    //objStoredProc.SetParameter("RowCount", RowCount, ParameterDirection.Output, DbType.Int32);
                    quoteDetails = objStoredProc.ExecuteStoredProcedureList("AWCT_GetGuestQuotebyId @QuoteNumber,@EmailAddress").FirstOrDefault();
                }
                return quoteDetails;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return quoteDetails;
            }

        }

        public virtual QuoteResponseModel GetGuestQuoteReceipt(QuoteModel model)
        {
            ZnodeLogging.LogMessage("Execution started: GetGuestQuoteReceipt", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

            int userId = _userRepository.Table.Where(x => x.Email == model.EmailId).Select(y => y.UserId).FirstOrDefault();
            //int quoteId = _omsQuoteRepository.Table.Where(x => x.QuoteNumber == Convert.ToString(model.QuoteNumber)).Select(y => y.OmsQuoteId).FirstOrDefault();

            ZnodeOmsQuote quoteDetails = GetGuestQuoteDetailsById(model.QuoteNumber, model.EmailId);
            if (quoteDetails.OmsQuoteId <= 0)
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorPlaceOrder);
            }

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple("OmsQuoteId", FilterOperators.Equals, quoteDetails.OmsQuoteId.ToString()));

            QuoteResponseModel quoteModel = new QuoteResponseModel();
            quoteModel = GetQuoteFromDBById(quoteDetails.OmsQuoteId);

            if (IsNotNull(quoteModel))
            {
                //Set quote line items details
                SetCartItemDetails(quoteModel);

                //Get quote status for quote.
                quoteModel.QuoteStatus = GetQuoteStatus(quoteModel.OmsQuoteStateId);

                quoteModel.ShippingType = GetShippingType(quoteModel.ShippingId);

                //Get shipping and billing address.
                SetShippingBillingAddress(quoteModel);

                //Check UserExpand
                GetUserDetails(quoteModel);

                //Get default currency assigned to current portal.
                if (quoteModel.PortalId > 0)
                {
                    SetPortalDefaultCurrencyCultureCode(quoteModel.PortalId, quoteModel);
                }

                //Get Quote Note Details
                quoteModel.QuoteHistoryList = GetQuoteNotes(quoteModel.OmsQuoteId);

            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return quoteModel;
        }

        private static new UserModel GetUserNameByUserId(int userId)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("userId to get user name: ", string.Empty, TraceLevel.Verbose, userId);
            if (userId > 0)
            {
                IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();
                IZnodeRepository<AspNetUser> _aspNetUserRepository = new ZnodeRepository<AspNetUser>();
                IZnodeRepository<AspNetZnodeUser> _aspNetZnodeUserRepository = new ZnodeRepository<AspNetZnodeUser>();

                return (from user in _userRepository.Table
                        join aspNetUser in _aspNetUserRepository.Table on user.AspNetUserId equals aspNetUser.Id
                        join aspNetZnodeUser in _aspNetZnodeUserRepository.Table on aspNetUser.UserName equals aspNetZnodeUser.AspNetZnodeUserId
                        where user.UserId == userId
                        select new UserModel
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            UserName = aspNetZnodeUser.UserName,
                            PhoneNumber = user.PhoneNumber,
                            Email = user.Email
                        })?.FirstOrDefault();
            }
            return new UserModel();
        }

        #endregion
        #region
        //// Gets the HTML used when showing receipts in the UI.
        public virtual string GetQuoteReceiptHtml(string templatePath, int QuoteId,string Email,string UserName)
        => GenerateQuoteReceipt(templatePath, QuoteId, Email, UserName);

        ////Generates the HTML used in email receipts.
        public virtual string GenerateQuoteReceipt(string templateContent, int QuoteId, string Email, string UserName)
        {
            //TO set initial template for Order Receipt
            string receiptTemplate = templateContent;

            return CreateQuoteReceipt(receiptTemplate, QuoteId,  Email,  UserName);
        }

        public virtual string CreateQuoteReceipt(string template, int QuoteId, string Email, string UserName)
        {
            if (string.IsNullOrEmpty(template))
            {
                return template;
            }

            //order to bind order details in data tabel
            System.Data.DataTable orderTable = SetQuoteData(QuoteId,  Email,  UserName);
         //  orderTable.Rows[0]["CustomerServiceEmail"] = orderTable.Email;
            //int UserID = _orderDetailRepository.Table.FirstOrDefault(x => x.OmsOrderId == Order.OrderID).UserId;
            //string AccountNo = _userRepository.Table.FirstOrDefault(x => x.UserId == Order.UserID).ExternalId;
            //orderTable.Rows[0]["CustomerServicePhoneNumber"] = AccountNo;
            //orderTable.Rows[0]["Custom1"] = Order.Custom2 == "S" ? "I want my items faster. Ship them as they become available (Additional shipping cost may apply)" : "Group my items into as few shipment as possible.";
            //orderTable.Rows[0]["Message"] = Order.Custom1;
            //orderTable.Rows[0]["OrderDate"] = Order.OrderDateWithTime;
            //create order line Item
            DataTable orderlineItemTable = CreateQuoteLineItemTable();

            //order to bind order amount details in data tabel
            DataTable orderAmountTable = SetQuoteAmountData(QuoteId);

            DataTable DepositTable = SetDepositValue();

            //create multiple Address
            DataTable multipleAddressTable = CreateQuoteAddressTable();

            //create multiple tax address
            DataTable multipleTaxAddressTable = CreateQuoteTaxAddressTable();

            //bind line item data
            BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable, QuoteId);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

            // Parse order table
            receiptHelper.Parse(orderTable.CreateDataReader());

            // Parse order line items table
            receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                DataView filterData = orderlineItemTable.DefaultView;

                List<DataTable> group = filterData.ToTable().AsEnumerable()
                .GroupBy(r => new { Col1 = r["GroupId"] })
                .Select(g => g.CopyToDataTable()).ToList();

                //filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
               //receiptHelper.ParseWithGroup("LineItems" + address["OmsOrderShipmentID"], group);

                receiptHelper.ParseWithGroup("LineItems" , group);

                //Parse Tax based on order shipment
                //DataView amountFilterData = multipleTaxAddressTable.DefaultView;
                //amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                //receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
            }
            // Parse order amount table
            receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());
            //receiptHelper.Parse("DepositAmountLineItems", DepositTable.CreateDataReader());
            //Replace the Email Template Keys, based on the passed email template parameters.

            // Return the HTML output
            return receiptHelper.Output;
        }

        ////to set order details
        public virtual DataTable SetQuoteData(int QuoteId, string Email, string UserName)
        {
            // Create new row
            DataTable orderTable = CreateQuoteTable();
            DataRow orderRow = orderTable.NewRow();
            IZnodeOrderHelper helper = GetService<IZnodeOrderHelper>();
            ZnodeOmsQuote Order = GetQuoteDetailsById(QuoteId);
            PortalModel portal = helper.GetPortalDetailsByPortalId(Order.PortalId);
            //_currencyCode = Order.CurrencyCode;
            _cultureCode = Order.CultureCode;

            UserModel userDetails = (from u in _userRepository.Table
                                     join Aspnetuser in _aspnetUserRepository.Table on u.AspNetUserId equals Aspnetuser.Id
                                     join aspnetznode in _aspnetZnodeUserRepository.Table on Aspnetuser.UserName
                                     equals aspnetznode.AspNetZnodeUserId
                                     where u.UserId == Order.UserId && u.AspNetUserId != null && u.IsActive == true

                                     select new UserModel
                                     {
                                         UserName = aspnetznode.UserName,
                                         Email = Aspnetuser.Email

                                     })?.FirstOrDefault();

            // Additional info
            orderRow["SiteName"] = portal?.StoreName ?? ZnodeConfigManager.SiteConfig.StoreName;
            orderRow["StoreLogo"] = helper.SetPortalLogo(Order.PortalId);
            orderRow["ReceiptText"] = string.Empty;
            //orderRow["CustomerServiceEmail"] = FormatStringComma(portal?.CustomerServiceEmail) ?? FormatStringComma(ZnodeConfigManager.SiteConfig.CustomerServiceEmail);
            orderRow["CustomerServiceEmail"] = string.IsNullOrEmpty(Convert.ToString(userDetails?.Email))? Email: Convert.ToString(userDetails?.Email);
            orderRow["CustomerServicePhoneNumber"] = string.IsNullOrEmpty(Convert.ToString(userDetails?.UserName))? UserName: Convert.ToString(userDetails?.UserName);
            //orderRow["CustomerServicePhoneNumber"] = portal?.CustomerServicePhoneNumber.Trim() ?? ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber.Trim();
            //orderRow["FeedBack"] = FeedbackUrl;
            //orderRow["ShippingName"] = Order?.ShippingName;

            //Payment info
            //if (!String.IsNullOrEmpty(Order.PaymentTrancationToken))
            //{
            //    orderRow["CardTransactionID"] = Order.PaymentTrancationToken;
            //    orderRow["CardTransactionLabel"] = Admin_Resources.LabelTransactionId;
            //}

            //orderRow["PaymentName"] = Order.PaymentDisplayName;

            if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
            {
                orderRow["PONumber"] = Order.PurchaseOrderNumber;
                orderRow["PurchaseNumberLabel"] = Admin_Resources.LabelPurchaseOrderNumber;
            }

            //Customer info
            orderRow["OrderId"] = Order?.QuoteNumber;
            orderRow["OrderDate"] = Order.CreatedDate;

            AddressModel ShippingAddress = _addressRepository.GetById(Convert.ToInt32(Order.ShippingAddressId))?.ToModel<AddressModel>();
            AddressModel BillingAddress = _addressRepository.GetById(Convert.ToInt32(Order.BillingAddressId))?.ToModel<AddressModel>();
            string BillingAddressHtml = Convert.ToString(BillingAddress.FirstName) + "<br/>" + Convert.ToString(BillingAddress.LastName) + "<br/>" + Convert.ToString(BillingAddress.CompanyName) + "<br/>" + Convert.ToString(BillingAddress.Address1) + "<br/>" + Convert.ToString(BillingAddress.Address2) + "<br/>" + Convert.ToString(BillingAddress.CityName) + "<br/>" + Convert.ToString(BillingAddress.CountryName) + "<br/>" + Convert.ToString(BillingAddress.PostalCode) + "<br/>" + Convert.ToString(BillingAddress.PhoneNumber);
            string ShippingAddressHtml = Convert.ToString(ShippingAddress.FirstName) + "<br/>" + Convert.ToString(ShippingAddress.LastName) + "<br/>" + Convert.ToString(ShippingAddress.CompanyName) + "<br/>" + Convert.ToString(ShippingAddress.Address1) + "<br/>" + Convert.ToString(ShippingAddress.Address2) + "<br/>" + Convert.ToString(ShippingAddress.CityName) + "<br/>" + Convert.ToString(ShippingAddress.CountryName) + "<br/>" + Convert.ToString(ShippingAddress.PostalCode) + "<br/>" + Convert.ToString(ShippingAddress.PhoneNumber);



            orderRow["BillingAddress"] = BillingAddressHtml;
            // orderRow["PromotionCode"] = Order.CouponCode;

            //var addresses = ((ZnodePortalCart)ShoppingCart).AddressCarts;
            //orderRow["ShippingAddress"] = addresses.Count > 1 ? Admin_Resources.MessageKeyShippingMultipleAddress : GetOrderShipmentAddress(Order.OrderLineItems.FirstOrDefault().ZnodeOmsOrderShipment);
            orderRow["ShippingAddress"] = ShippingAddressHtml;
            orderRow["TotalCost"] = GetFormatPriceWithCurrency(Convert.ToDecimal(Order.QuoteOrderTotal));
            if (Order.AdditionalInstruction != null)
            {
                orderRow["AdditionalInstructions"] = Order.AdditionalInstruction;
                orderRow["AdditionalInstructLabel"] = Admin_Resources.LabelAdditionalNotes;
            }
            // Add rows to order table
            orderTable.Rows.Add(orderRow);
            return orderTable;
        }

        //to create order order line item table
        public virtual DataTable CreateQuoteLineItemTable()
        {
            DataTable orderlineItemTable = new DataTable();
            orderlineItemTable.Columns.Add("ProductImage");
            orderlineItemTable.Columns.Add("Name");
            orderlineItemTable.Columns.Add("SKU");
            orderlineItemTable.Columns.Add("Quantity");
            orderlineItemTable.Columns.Add("Description");
            orderlineItemTable.Columns.Add("UOMDescription");
            orderlineItemTable.Columns.Add("Price");
            orderlineItemTable.Columns.Add("ExtendedPrice");
            orderlineItemTable.Columns.Add("OmsOrderShipmentID");
            orderlineItemTable.Columns.Add("ShortDescription");
            orderlineItemTable.Columns.Add("ShippingId");
            orderlineItemTable.Columns.Add("OrderLineItemState");
            orderlineItemTable.Columns.Add("TrackingNumber");
            orderlineItemTable.Columns.Add("Custom1");
            orderlineItemTable.Columns.Add("Custom2");
            orderlineItemTable.Columns.Add("Custom3");
            orderlineItemTable.Columns.Add("Custom4");
            orderlineItemTable.Columns.Add("Custom5");
            orderlineItemTable.Columns.Add("GroupId");
            orderlineItemTable.Columns.Add("GroupingRowspan");
            orderlineItemTable.Columns.Add("GroupingDisplay");
            return orderlineItemTable;
        }

        public virtual DataTable SetQuoteAmountData(int QuoteId)
        {
            ZnodeOmsQuote Order = GetQuoteDetailsById(QuoteId);

            List<QuoteLineItemModel> quoteLineItemModels = GetQuoteLineItems(QuoteId);
            decimal subtotal = 0;
            decimal SubTotal = 0;
            foreach (QuoteLineItemModel lineItem in quoteLineItemModels.Where(quoteLineItem => quoteLineItem.ParentOmsQuoteLineItemId.HasValue))
            {
               subtotal = Convert.ToDecimal(double.Parse(Convert.ToString(lineItem.Quantity * lineItem.Price)));
                SubTotal += subtotal;
            }
            //SubTotal += subtotal;
           //string VolumeDiscount = ShoppingCart.Custom4;
           //string VolumeDiscount = "";
           //ZnodeLogging.LogMessage("VolumeDiscount Before-" + VolumeDiscount, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
           //if (VolumeDiscount == "-1")
           //{
           //    VolumeDiscount = string.Empty;
           //}
           //ZnodeLogging.LogMessage("VolumeDiscount after-" + VolumeDiscount, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);

            //int discount = 0;
            //string volumeCode = "", CouponCode = "";
            //decimal CouponDiscount = 0, VolumeDiscountAmt = 0;
            ////if (ShoppingCart.Custom5 == "Remove" || Convert.ToString(ShoppingCart.Custom1) == "Remove")
            ////{
            ////    VolumeDiscount = string.Empty;
            ////}
            //if (VolumeDiscount != string.Empty)
            //{
            //    try
            //    {
            //        discount = Convert.ToInt32(Convert.ToDecimal(VolumeDiscount) * 100);
            //    }
            //    catch (Exception)
            //    {
            //        discount = 0;
            //    }
            //    IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = null;
            //    // ((ZnodePortalCart)ShoppingCart).AddressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());
            //    string ExclusionCategory = ConfigurationManager.AppSettings["ExclusionCategory"].ToString();// "Value,Clearance";
            //    decimal subtotal = Convert.ToDecimal(shoppingCartItems.Where(x => !ExclusionCategory.Split(',').Contains(x.Custom1)).Sum(x => x.Quantity * x.UnitPrice));
            //    VolumeDiscountAmt = Convert.ToDecimal((subtotal * discount) / 100);
            //}
            //string ExclusionCategory = ConfigurationManager.AppSettings["ExclusionCategory"].ToString();// "Value,Clearance";
            //decimal subtotal = Convert.ToDecimal(Order.Where(x => !ExclusionCategory.Split(',').Contains(x.Custom1)).Sum(x => x.Quantity * x.UnitPrice));
            // VolumeDiscountAmt = Math.Round(VolumeDiscountAmt, 2);              
            //decimal CouponDis = Convert.ToDecimal(Math.Round((ShoppingCart.Discount - VolumeDiscountAmt), 2));
            //if (discount > 0)
            //{
            //    volumeCode = "(" + discount.ToString() + "%)";
            //}
            //if (CouponDis > 0)
            //{
            //    CouponDiscount = CouponDis; //Order.Discount - //discountAmt;
            //    CouponCode = "(" + Order.CouponCode + ")";

            //}
            // Create order amount table
            DataTable orderAmountTable = CreateQuoteAmountTable();
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal")) ? Admin_Resources.LabelSubTotal : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), Convert.ToDecimal(SubTotal), orderAmountTable);

            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), Order.PST, orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), Order.GST, orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount")) ? "Volume Discount" + volumeCode : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount").ToString(), -VolumeDiscountAmt, orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount")) ? "Coupon Discount" + CouponCode : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount").ToString(), -CouponDiscount, orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax")) ? "18% Canadian Duty" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), Order.SalesTax, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost")) ? "Shipping" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost").ToString(), Convert.ToDecimal(Order.ShippingCost), orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount")) ? Admin_Resources.LabelGiftCardAmount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount").ToString(), -Order.GiftCardAmount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost")) ? "Tax" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost").ToString(), Convert.ToDecimal(Order.TaxCost), orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), Order.SalesTax, orderAmountTable);


            return orderAmountTable;
        }

        private DataTable SetDepositValue()
        {
            DataTable depositTable = CreateDepositTable();
            //if (ShoppingCart.Payment.PaymentSettingId == 16)
            //{
            //    BuildDepositAmountTable("Deposit Paid", Order.Total, depositTable);
            //    BuildDepositAmountTable("<Span style='color:Red'>* Balance must be paid before order ships. Balance Due</Span>", 0, depositTable);

            //}
            //else if (ShoppingCart.Payment.PaymentSettingId == 12)
            //{
            //    BuildDepositAmountTable("Deposit Paid", Order.Total / 2, depositTable);
            //    BuildDepositAmountTable("<Span style='color:Red'>* Balance must be paid before order ships. Balance Due</Span>", Order.Total / 2, depositTable);
            //}
            return depositTable;
        }

        //to create order address table
        public virtual DataTable CreateQuoteAddressTable()
        {
            DataTable multipleAddressTable = new DataTable();
            multipleAddressTable.Columns.Add("ShipTo");
            multipleAddressTable.Columns.Add("OmsOrderShipmentID");
            multipleAddressTable.Columns.Add("ShipmentNo");
            return multipleAddressTable;
        }
        //to create order address table
        public virtual DataTable CreateQuoteTaxAddressTable()
        {
            DataTable multipleTaxAddressTable = new DataTable();
            multipleTaxAddressTable.Columns.Add("OmsOrderShipmentID");
            multipleTaxAddressTable.Columns.Add("Title");
            multipleTaxAddressTable.Columns.Add("Amount");
            return multipleTaxAddressTable;
        }

        public virtual void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable, int QuoteId)
        {
            //List<ZnodeOmsQuoteLineItem> allquoteLineItems = quotehelper.GetQuoteLineItemByQuoteId(QuoteId);

            List<ZnodeOmsQuoteLineItem> allquoteLineItems = GetQuoteDetailsForReceiptById(QuoteId);

            //List<OrderLineItemModel> OrderLineItemList = allquoteLineItems?.P.GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();

            List<ZnodeOmsQuoteLineItem> quoteLineItems = allquoteLineItems?
                                                    .Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns)
                                                    && m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)).ToList();

            QuoteResponseModel quoteModel = new QuoteResponseModel();
            quoteModel = GetQuoteFromDBById(QuoteId);

            int PublishCatalogId = GetPublishCatalogId(quoteModel.UserId, quoteModel.PortalId);

            List<ProductEntity> productList = publishProductHelper.GetPublishProductBySKUs(string.Join(",", quoteLineItems?.Select(x => x.SKU)), PublishCatalogId, 1, null)?.ToModel<ProductEntity>()?.ToList();



            //IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;

            //foreach (ZnodeOmsQuoteLineItem orderShipment in quoteLineItems)
            //{
            DataRow addressRow = multipleAddressTable.NewRow();

            
            // If multiple shipping addresses then display the address for each group
            //if (quoteLineItems.Count() > 1)
            //{
            //    //addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.}";
            //    //addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
            //}

            //addressRow["OmsOrderShipmentID"] = orderShipment.OmsOrderShipmentId;
            int counter = 0;
            /*Changes for Size Display Order*/
            List<string> sizelist = new List<string>();
            //foreach (ZnodeOmsQuoteLineItem lineitem in allquoteLineItems.Where(x => x.OmsQuoteLineItemId == quoteLineItems.).OrderBy(x => x.ProductName).ThenByDescending(s => s.ParentOmsQuoteLineItemId))
            //{
            //    //if (lineitem.Count > 0)
            //    //{
            //        ZnodeOmsQuoteLineItem childLineItem = lineitem;
            //        childLineItem.Description.Replace("<br/>", "");
            //        string[] spearator = { "<br>" };
            //        string[] desc = childLineItem.Description.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            //        sizelist.Add(desc[0].Replace("Awct_Size - ", "").Replace("<br/>", ""));
            //    //}
            //}
            string sizeliststr = String.Join(",", sizelist);
            DataTable sizesWithDisplayOrder = GetSizesDisplayOrder(sizeliststr);
            /*Changes for Size Display Order*/
            //foreach (ZnodeOmsQuoteLineItem lineitem in quoteLineItems.Where(x => x.OmsQuoteLineItemId == orderShipment.OmsQuoteLineItemId).OrderBy(x => x.ProductName).ThenByDescending(s => s.ParentOmsQuoteLineItemId))
            //{
            /*Original Code Working for dance*/
            //IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = ((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());
            //IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = null;
            //((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId || (x.OrderShipmentID == 0 && orderShipment.OmsOrderShipmentId != 0)).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());

            //lineitem.OrderLineItemCollection.RemoveAll(x => x.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns));
            //if (lineitem.OrderLineItemCollection?.Any(x => x.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)) ?? false)
            //{
            foreach (ZnodeOmsQuoteLineItem lineItem in quoteLineItems.Where(quoteLineItem => quoteLineItem.ParentOmsQuoteLineItemId.HasValue))
            {
                //ZnodeShoppingCartItem shoppingCartItem = null;
                //if (Order.Order.IsQuoteOrder)
                //{
                //shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && (!string.IsNullOrEmpty(s.SKU) ? childLineItem.Sku.Contains(s.SKU) : false) && s.OrderLineItemRelationshipTypeId.HasValue);
                //if (IsNull(allquoteLineItems))
                //{
                //    /*Original Code Working for dance*/
                //    //shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && childLineItem.Sku == s.Custom5 && s.OrderLineItemRelationshipTypeId.HasValue);
                //    allquoteLineItems = allquoteLineItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && lineitem.SKU == s.ParentProductSKU && s.OrderLineItemRelationshipTypeId.HasValue);
                //}
                //}
                //else
                //{
                //    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && s.SKU == childLineItem.Sku && s.OrderLineItemRelationshipTypeId.HasValue);
                //}

                //Get ShoppingCartItem when GroupId is Null
                //if (IsNull(shoppingCartItem))
                //{
                //    shoppingCartItem = shoppingCartItems.FirstOrDefault(m => m.Custom5.Equals(childLineItem.Sku));
                //}
                //setGroupProductDetails(lineitem, childLineItem);

                //IEnumerable<ZnodeOmsQuoteLineItem> quoteLineItem = allquoteLineItems.Where(x => x.SKU == orderShipment.SKU && x.GroupId == lineitem.GroupId);

                //AddressModel ShippingAddress = _addressRepository.GetById(Convert.ToInt32(lineItem.Shi))?.ToModel<AddressModel>();
                //AddressModel BillingAddress = _addressRepository.GetById(Convert.ToInt32(quoteCreateModel.BillingAddressId))?.ToModel<AddressModel>();
                //string BillingAddressHtml = Convert.ToString(BillingAddress.Address1) + "<br/>" + Convert.ToString(BillingAddress.Address2) + "<br/>" + Convert.ToString(BillingAddress.CityName) + "<br/>" + Convert.ToString(BillingAddress.CountryName) + "<br/>" + Convert.ToString(BillingAddress.PostalCode) + "<br/>" + Convert.ToString(BillingAddress.PhoneNumber);
                //string ShippingAddressHtml = Convert.ToString(ShippingAddress.Address1) + "<br/>" + Convert.ToString(ShippingAddress.Address2) + "<br/>" + Convert.ToString(ShippingAddress.CityName) + "<br/>" + Convert.ToString(ShippingAddress.CountryName) + "<br/>" + Convert.ToString(ShippingAddress.PostalCode) + "<br/>" + Convert.ToString(ShippingAddress.PhoneNumber);

                ShoppingCartItemModel item = lineItem.ToModel<ShoppingCartItemModel>();
                ProductEntity product = productList?.FirstOrDefault(x => x.SKU == item.SKU);

                string style = product.Attributes?.FirstOrDefault(x => x.AttributeCode == "awctstyle")?.AttributeValues;
                product.Name = product.Name.Contains(style) ? product.Name : style + " " + product.Name;
                string AddonProduct = Convert.ToString(item.CustomText);
                string[] ProductName;
                if (Convert.ToString(AddonProduct).Contains("AddOn"))
                {
                    StringBuilder sb = new StringBuilder();
                    item.ProductName = product.Name;
                    ProductName = Convert.ToString(item.ProductName).Split('#');
                    sb.Append(ProductName[0]);
                }

                else {
                    StringBuilder sb = new StringBuilder();
                    ProductName = Convert.ToString(lineItem.ProductName).Split('|');
                    sb.Append(ProductName[0]);
                }
                

                //if (!String.IsNullOrEmpty(shoppingCartItem?.Product?.DownloadLink?.Trim()))
                //{
                //    sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                //}

                //if (orderLineItemTable != null)
                //{
                string size = "", Color = "";
                //ZnodeLogging.LogMessage("Item Custom3-" + lineitem?.Custom3, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                string[] DateColor = Convert.ToString(lineItem?.CustomText).Split('|');
                string[] NewEstimateDate = Convert.ToString(DateColor[2]).Split('-');
                string EstimateDate = "", ColorCode = "";
                if (DateColor.Length > 0)
                {
                    //EstimateDate = DateColor[0].ToString();
                    /*Original Code*/
                    //EstimateDate = DateTime.Now.AddDays(Convert.ToDouble(NewEstimateDate[0])).ToString("MM/dd/yyyy");
                    /*Nivi Code start for Getting Estimated Ship Date in Quote Order*/
                    if ((Equals(DateTime.Now.AddDays(Convert.ToDouble(NewEstimateDate[0])).DayOfWeek, DayOfWeek.Saturday)))
                    {

                        EstimateDate = DateTime.Now.AddDays(Convert.ToDouble(NewEstimateDate[0]) + 2).ToString("MM/dd/yyyy");
                    }
                    else if ((Equals(DateTime.Now.AddDays(Convert.ToDouble(NewEstimateDate[0])).DayOfWeek, DayOfWeek.Sunday)))
                    {
                        EstimateDate = DateTime.Now.AddDays(Convert.ToDouble(NewEstimateDate[0]) + 1).ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        EstimateDate = DateTime.Now.AddDays(Convert.ToDouble(NewEstimateDate[0])).ToString("MM/dd/yyyy");
                    }
                    /*Nivi Code end for Getting Estimated Ship Date in Quote Order*/

                    ColorCode = NewEstimateDate[1].ToString();
                }
                //StringBuilder PersonalizeDesc = new StringBuilder();
                //string personalizeDetails = Convert.ToString(orderShipment.PersonaliseValuesDetail?.FirstOrDefault(x => x.PersonalizeCode == "OptionValue")?.PersonalizeValue);
                //if (!string.IsNullOrEmpty(personalizeDetails))
                //{
                //    if (personalizeDetails.ToString() != "PersonaliseCode[sku")
                //    {
                //        PersonalizeDesc.Append(personalizeDetails + "<br />");
                //        string fileName = Convert.ToString(lineitem.PersonaliseValuesDetail?.FirstOrDefault(x => x.PersonalizeCode == "Filename").PersonalizeValue);
                //        string SVGPath = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString() + "/" + ConfigurationManager.AppSettings["SVGPathDownload"].ToString();
                //        PersonalizeDesc.Append("<a href =\"" + SVGPath + fileName + "\" target =\"_blank\">Preview Design</a>");
                //    }


                //}
                lineItem.Description.Replace("<br/>", "");
                string[] spearator = { "<br>" };
                //Collection - Dance < br > Awct_Color - Fuchsia < br > Awct_Size - ISC < br />
                string[] desc = lineItem.Description.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                Color = desc[1].Replace("Awct_Color - ", "");
                size = desc[0].Replace("Awct_Size - ", "").Replace("<br/>", "");
                /*Changes for Size Display Order*/
                DataRow[] dr1 = sizesWithDisplayOrder.Select("Size='" + size + "'");
                /*Changes for Size Display Order end*/
                desc[0] = desc[0].Replace("Collection -", "");
                StringBuilder productDesc = new StringBuilder();
                //if (Convert.ToString(childLineItem.Custom5) != string.Empty)
                //{
                //    productDesc.Append(childLineItem.Description.Replace("Awct_Size", "Size").Replace("Awct_Color", "Color " + ColorCode) + "<br />");
                //    productDesc.Append(desc[0] + "<br />");
                //    string UploadFilePath = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString() + "/Data/Media/UploadDocument/" + childLineItem.Custom5;
                //    productDesc.Append("<a href =\"" + UploadFilePath + "\" target =\"_blank\">View File</a>");
                //}
                //else
                //{
                //productDesc.Append(desc[0] + "<br />");
                //productDesc.Append(lineItem.Description.Replace("Awct_Size", "Size").Replace("Awct_Color", "Color " + ColorCode));
                string strAddOn= Convert.ToString(DateColor[3]);
                if (Convert.ToString(DateColor[2]).Contains("TRU") &&  strAddOn!="AddOn")
                {
                    productDesc.Append(Convert.ToString(DateColor[3]));
                }
                else
                {
                    productDesc.Append(lineItem.Description.Replace("Awct_Size", "Size").Replace("Awct_Color", "Color " + ColorCode));
                }
                // }

                //if (lineItem.Custom4 == "AddOn")
                //{

                //    //string sku = Convert.ToString
                //    //    (_omsOrderLineItemRepository.Table.Where(x => x.OmsOrderLineItemsId == childLineItem.ParentOmsOrderLineItemsId)
                //    //              .Select(x => x.Sku).FirstOrDefault().ToString());
                //    //string Name = Convert.ToString
                //    //  (_omsOrderLineItemRepository.Table.Where(x => x.Sku == sku && x.Custom4 != "AddOn" && x.OmsOrderDetailsId == childLineItem.OmsOrderDetailsId)
                //    //            .Select(x => x.ProductName).FirstOrDefault());
                //    //sku = sku.Split('_')[1];
                //    //childLineItem.Description = sku + " " + Name;
                //}

                DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                //orderlineItemDbRow["ProductImage"] = lineitem.ProductImagePath;
                orderlineItemDbRow["Name"] = ProductName[0];
                orderlineItemDbRow["SKU"] = lineItem.SKU;
                orderlineItemDbRow["Description"] = productDesc.Replace("\\n", "<br />");
                //orderlineItemDbRow["UOMDescription"] = PersonalizeDesc.ToString().Replace("\\n", "<br />");
                orderlineItemDbRow["Quantity"] = Convert.ToString(double.Parse(Convert.ToString(lineItem.Quantity)));
                orderlineItemDbRow["ShortDescription"] = EstimateDate;
                orderlineItemDbRow["Custom2"] = size;
                orderlineItemDbRow["Custom3"] = ColorCode + "-" + Color;
                orderlineItemDbRow["Custom5"] = lineItem.ProductName.ToUpper().Contains("SETUP") ? "Value" : productDesc.ToString();

                ///*Changes for Size Display Order*/
                //if (dr1.Length > 0)
                //{
                //    orderlineItemDbRow["Custom4"] = dr1[0]["DisplayOrder"];
                //}
                ///*Changes for Size Display Order end*/
                //if (shoppingCartItem != null)
                //{
                    orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(Convert.ToDecimal(lineItem.Price), "");
                    orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(Convert.ToDecimal(lineItem.Price)* Convert.ToDecimal(double.Parse(Convert.ToString(lineItem.Quantity))), "");
                    //orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                //}

                //orderlineItemDbRow["OmsOrderShipmentID"] = childLineItem.OmsOrderShipmentId;
                orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineItem.GroupId) ? Guid.NewGuid().ToString() : lineItem.GroupId;

                orderLineItemTable.Rows.Add(orderlineItemDbRow);
                //}
                counter++;
                //}
            }
            //else
            //{
            //    ZnodeShoppingCartItem shoppingCartItem = shoppingCartItems.ElementAt(counter++);

            //    foreach (OrderLineItemModel orderLineItem in lineitem.OrderLineItemCollection)
            //    {
            //        setGroupProductDetails(lineitem, orderLineItem);
            //    }

            //    StringBuilder sb = new StringBuilder();
            //    sb.Append(lineitem.ProductName + "<br />");

            //    if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
            //    {
            //        sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
            //    }

            //    if (orderLineItemTable != null)
            //    {
            //        DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
            //        orderlineItemDbRow["ProductImage"] = lineitem.ProductImagePath;
            //        orderlineItemDbRow["Name"] = sb.ToString();
            //        orderlineItemDbRow["SKU"] = lineitem.Sku;
            //        orderlineItemDbRow["Description"] = lineitem.Description;
            //        orderlineItemDbRow["UOMDescription"] = string.Empty;
            //        orderlineItemDbRow["Quantity"] = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.GroupProductQuantity : Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
            //        orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
            //        orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
            //        orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
            //        orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
            //        orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;
            //        orderLineItemTable.Rows.Add(orderlineItemDbRow);
            //    }
            //}
            //}
            //Code for SOrt by 
            DataView dv = orderLineItemTable.DefaultView;
            // dv.Sort = "Description,Name,Custom3,Custom2 ASC";
            dv.Sort = "Custom5,Name,Custom3,Custom4 ASC";
            orderLineItemTable = dv.ToTable();
            //ZnodeMultipleAddressCart addressCart = null;
            //object globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
            //BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), "SubTotal", ProductName[3], multipleTaxAddressTable);
            //BuildOrderShipmentTotalLineItem($"Shipping Cost({addressCart.Shipping.ShippingName})", addressCart.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

            //((ZnodePortalCart)ShoppingCartMap).AddressCarts.FirstOrDefault(y => y.OrderShipmentID == orderShipment.OmsOrderShipmentId);

            //if (addressCart != null && orderShipments.Count() > 1)
            //{
            //    object globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
            //    if (globalResourceObject != null)
            //    {
            //        BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), addressCart.SubTotal, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
            //    }

            //    BuildOrderShipmentTotalLineItem($"Shipping Cost({addressCart.Shipping.ShippingName})", addressCart.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

            //    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax").ToString(), addressCart.SalesTax, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

            //    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), addressCart.VAT, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

            //    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), addressCart.GST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

            //    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), addressCart.HST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

            //    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), addressCart.PST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
            //}
            multipleAddressTable.Rows.Add(addressRow);
            //}
        }

        public virtual DataTable CreateQuoteTable()
        {
            DataTable orderTable = new DataTable();
            // Additional info
            orderTable.Columns.Add("SiteName");
            orderTable.Columns.Add("StoreLogo");
            orderTable.Columns.Add("ReceiptText");
            orderTable.Columns.Add("CustomerServiceEmail");
            orderTable.Columns.Add("CustomerServicePhoneNumber");
            orderTable.Columns.Add("FeedBack");
            orderTable.Columns.Add("AdditionalInstructions");
            orderTable.Columns.Add("AdditionalInstructLabel");

            // Payment info
            orderTable.Columns.Add("CardTransactionID");
            orderTable.Columns.Add("CardTransactionLabel");
            orderTable.Columns.Add("PaymentName");

            orderTable.Columns.Add("PONumber");
            orderTable.Columns.Add("PurchaseNumberLabel");

            // Customer info
            orderTable.Columns.Add("OrderId");
            orderTable.Columns.Add("OrderDate");
            orderTable.Columns.Add("UserId");
            orderTable.Columns.Add("BillingAddress");
            orderTable.Columns.Add("ShippingAddress");
            orderTable.Columns.Add("PromotionCode");
            orderTable.Columns.Add("TotalCost");
            // Returned total cost
            orderTable.Columns.Add("ReturnedTotalCost");
            orderTable.Columns.Add("StyleSheetPath");

            orderTable.Columns.Add("ShippingName");
            orderTable.Columns.Add("TrackingNumber");

            orderTable.Columns.Add("Custom1");
            orderTable.Columns.Add("Message");
            return orderTable;
        }
        public virtual string FormatStringComma(string input)
        {
            return input.Replace(",", ", ");
        }

        // Builds the Shipment order line item table.
        public virtual void BuildOrderShipmentTotalLineItem(string title, decimal amount, int OmsOrderShipmentId, DataTable taxTable)
        {
            if (amount > 0)
            {
                DataRow taxAddressRow = taxTable.NewRow();
                taxAddressRow["Title"] = title;
                taxAddressRow["Amount"] = GetFormatPriceWithCurrency(amount);
                taxAddressRow["OmsOrderShipmentID"] = OmsOrderShipmentId;
                taxTable.Rows.Add(taxAddressRow);
            }
        }

        public virtual string GetFormatPriceWithCurrency(decimal priceValue, string uom = "")
        {
            return ZnodeCurrencyManager.FormatPriceWithCurrency(priceValue, _cultureCode, uom);
        }


        public virtual void setGroupProductDetails(OrderLineItemModel lineitem, OrderLineItemModel orderLineItem)
        {
            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)
            {
                lineitem.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Group;
                lineitem.GroupProductQuantity = lineitem.GroupProductQuantity + "<br/>" + double.Parse(orderLineItem.Quantity.ToString());
                lineitem.Description = orderLineItem.Description + "<br/>" + orderLineItem.ProductName;
                lineitem.GroupProductPrice = lineitem.GroupProductPrice + "<br/>" + GetFormatPriceWithCurrency(orderLineItem.Price);
                lineitem.Sku = orderLineItem.Sku;
            }
            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable)
            {
                lineitem.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Configurable;
                lineitem.Sku = orderLineItem.Sku;
                lineitem.Price = orderLineItem.Price;
                lineitem.Quantity = orderLineItem.Quantity;
            }
        }

        public DataTable GetSizesDisplayOrder(string sizeliststr)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@SizeListString", sizeliststr, ParameterDirection.Input, SqlDbType.VarChar);
            DataSet data = objStoredProc.GetSPResultInDataSet("AWCT_GetSizeDisplayOrder");
            DataTable sizeDisplayOrderTable = data.Tables[0];
            return sizeDisplayOrderTable;
        }

        public void BuildDepositAmountTable(string title, decimal amount, DataTable orderAmountTable)
        {

            DataRow row = orderAmountTable.NewRow();
            row["DTitle"] = title;
            row["DAmount"] = GetFormatPriceWithCurrency(amount);

            orderAmountTable.Rows.Add(row);

        }

        public virtual void BuildOrderAmountTable(string title, decimal amount, DataTable orderAmountTable)
        {
            if (amount != 0)
            {
                DataRow row = orderAmountTable.NewRow();
                row["Title"] = title;
                row["Amount"] = GetFormatPriceWithCurrency(amount);

                orderAmountTable.Rows.Add(row);
            }
        }

        public virtual DataTable CreateQuoteAmountTable()
        {
            DataTable orderAmountTable = new DataTable();
            orderAmountTable.Columns.Add("Title");
            orderAmountTable.Columns.Add("Amount");
            return orderAmountTable;
        }

        public virtual DataTable CreateDepositTable()
        {
            DataTable orderAmountTable = new DataTable();
            orderAmountTable.Columns.Add("DTitle");
            orderAmountTable.Columns.Add("DAmount");
            return orderAmountTable;
        }


        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
            {
                return receiptContent.Replace("#FeedBack#", GenerateOrderAdditionalInfoTemplate(customData));
            }
            else
            {
                return receiptContent;
            }
        }

        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            try
            {
                Dictionary<string, string> CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
                if (HelperUtility.IsNotNull(CustomDict))
                {
                    template = ("<b>Additional Information</b>");

                    if (CustomDict.ContainsKey("ProductName"))
                    {
                        template += $" <br />Product will be used by  { CustomDict["ProductName"]}";
                    }

                    if (CustomDict.ContainsKey("RecipientName"))
                    {
                        template += $" <br />Recipient of the product {CustomDict["RecipientName"]}";
                    }

                    if (CustomDict.ContainsKey("ApproverManager"))
                    {
                        template += $" <br />Approving Manager {CustomDict["ApproverManager"]}";
                    }

                    if (CustomDict.ContainsKey("ProjectName"))
                    {
                        template += $" <br />Project Name {CustomDict["ProjectName"]}";
                    }

                    if (CustomDict.ContainsKey("EventDate"))
                    {
                        template += $" <br />Event Date {CustomDict["EventDate"]}";
                    }

                    if (CustomDict.ContainsKey("InHandsDate"))
                    {
                        template += $" <br />In Hands Date {CustomDict["InHandsDate"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return template;
        }

        protected virtual List<ZnodeOmsQuoteLineItem> GetQuoteDetailsForReceiptById(int QuoteId)
        {

            List<ZnodeOmsQuoteLineItem> quoteDetails = new List<ZnodeOmsQuoteLineItem>();
            try
            {
                if (QuoteId!=0)
                {
                    int RowCount = 0;
                    IZnodeViewRepository<ZnodeOmsQuoteLineItem> objStoredProc = new ZnodeViewRepository<ZnodeOmsQuoteLineItem>();
                    objStoredProc.SetParameter("QuoteId", QuoteId, ParameterDirection.Input, DbType.String);
                    quoteDetails = objStoredProc.ExecuteStoredProcedureList("AWCT_GetQuoteDetailsForReceiptById @QuoteId").ToList();
                }
                return quoteDetails;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return quoteDetails;
            }

        }

        private ZnodeUserAddress UpdateUserAddressUserId(int userid, int userDetailsId)
        {
            ZnodeUserAddress userAddress = new ZnodeUserAddress();
            try
            {
                //SP call to save/update savedCartLineItem
                int Status = 0;
                IZnodeViewRepository<ZnodeUserAddress> objStoredProc = new ZnodeViewRepository<ZnodeUserAddress>();
                objStoredProc.SetParameter("UserId", userid, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("userDetailsUserId", userDetailsId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("Status", Status, ParameterDirection.Output, DbType.Int32);
                userAddress = objStoredProc.ExecuteStoredProcedureList("AWCT_UpdateUserAddressUserId @UserId,@userDetailsUserId, @Status OUT", 0,out Status).FirstOrDefault();
                return userAddress;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return userAddress;
            }
        }
        #endregion
    }
}
