﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;


namespace Znode.Api.Custom.Service.Service
{

    public class AWCTCMSWidgetConfigurationService : CMSWidgetConfigurationService
    {
        private readonly IZnodeRepository<ZnodeCMSWidgetProduct> _cmsWidgetProductRepository;
        private readonly IZnodeRepository<ZnodePublishProductDetail> _znodePublishProductRepository;
        public AWCTCMSWidgetConfigurationService()
        {
            _cmsWidgetProductRepository = new ZnodeRepository<ZnodeCMSWidgetProduct>();
            _znodePublishProductRepository = new ZnodeRepository<ZnodePublishProductDetail>();

        }
        public override bool AssociateProduct(CMSWidgetProductListModel cmsWidgetProductListModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (IsNull(cmsWidgetProductListModel) || IsNull(cmsWidgetProductListModel.CMSWidgetProducts))
            {
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);
            }

            if (cmsWidgetProductListModel.CMSWidgetProducts.Count < 1)
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorCMSWidgetProductsCountLessThanZero);
            }

            cmsWidgetProductListModel.CMSWidgetProducts.ForEach(item =>
            {

                ZnodePublishProductDetail publlishProductDetails = _znodePublishProductRepository.Table.Where(x => x.SKU == item.SKU).OrderByDescending(y => y.ModifiedDate).FirstOrDefault();
                if (publlishProductDetails != null)
                {
                    item.PublishProductId = publlishProductDetails.PublishProductId;
                }
            });
            IEnumerable<ZnodeCMSWidgetProduct> associateProduct = _cmsWidgetProductRepository.Insert(cmsWidgetProductListModel.CMSWidgetProducts.ToEntity<ZnodeCMSWidgetProduct>()?.ToList());

            if (cmsWidgetProductListModel.CMSWidgetProducts.FirstOrDefault().TypeOfMapping.ToLower() == ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString().ToLower() && associateProduct?.Count() > 0)
            {
                UpdateContentPageAfterPublish(cmsWidgetProductListModel.CMSWidgetProducts.FirstOrDefault().CMSMappingId, false);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return associateProduct?.Count() > 0;
        }

        private void UpdateContentPageAfterPublish(int contentPageId, bool value)
        {
            ZnodeRepository<ZnodeCMSContentPage> _contentPageRepository = new ZnodeRepository<ZnodeCMSContentPage>();
            //Updating the IsPublished flag for Content Page
            ZnodeLogging.LogMessage("contentPageId to get content page entity: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, contentPageId);
            ZnodeCMSContentPage entity = _contentPageRepository.GetById(contentPageId);
            entity.IsPublished = value;
            _contentPageRepository.Update(entity);
        }
    }
}
