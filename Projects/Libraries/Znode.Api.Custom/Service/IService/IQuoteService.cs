﻿using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Custom.Service.IService
{
    public interface IQuoteService
    {
        QuoteListModel GetQuoteList(FilterCollection filters,NameValueCollection sorts,NameValueCollection page);

        QuoteCreateModel Create(QuoteCreateModel quoteCreateModel);

        QuoteResponseModel GetQuoteReceipt(int quoteId);

        QuoteResponseModel GetQuoteById(int omsQuoteId);

        OrderModel ConvertQuoteToOrder(ConvertQuoteToOrderModel convertToOrderModel);

        List<QuoteLineItemModel> GetQuoteLineItems(int omsQuoteId);

        BooleanModel UpdateQuote(UpdateQuoteModel model);

        QuoteResponseModel GetGuestQuoteReceipt(QuoteModel quoteModel);
    }
}