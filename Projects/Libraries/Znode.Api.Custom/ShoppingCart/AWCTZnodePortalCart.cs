﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Api.Custom.ShoppingCart
{
    public class AWCTZnodePortalCart : ZnodePortalCart
    {
        public AWCTZnodePortalCart()
        {
           
        }

        public new int PortalID { get; set; }
        public new List<ZnodeMultipleAddressCart> AddressCarts { get; set; }
        public new decimal ShippingCost { get; set; }
        public new decimal TaxCost { get; set; }
        public new decimal SubTotal { get; set; }
        public new decimal Total { get; set; }
        public new decimal Discount { get; set; }

        //public new void Calculate();
        //public new void Calculate(int? profileId, bool isCalCulateTaxAndShipping = true);
    }
}
