﻿using System.Collections.Generic;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.WebStore.Custom.ViewModel
{
    public class QuoteListViewModel : BaseViewModel
    {
        public List<QuoteViewModel> Quotes { get; set; }
        public GridModel GridModel { get; set; }
        public int UserId { get; set; }
    }
}
