﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.WebStore.Custom.ViewModel.Quote
{
    public class AWCTCheckoutViewModel: CheckoutViewModel
    {
        public bool IsQuoteRequest { get; set; }
    }
}
