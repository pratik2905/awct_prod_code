﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.WebStore.Custom.Agents.IAgents
{
   public interface IAWCTWidgetDataAgent
    {
        /// <summary>
        /// Get sub total of cart.
        /// </summary>
        /// <returns>Sub total of cart.</returns>

        decimal GetCart();
    }
}
