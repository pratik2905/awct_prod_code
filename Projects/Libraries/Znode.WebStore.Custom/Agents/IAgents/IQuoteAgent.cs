﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IQuoteAgent
    {
        /// <summary>
        /// Create Quote
        /// </summary>
        /// <param name="quoteCreateViewModel"></param>
        /// <returns>QuoteCreateViewModel</returns>
        QuoteCreateViewModel Create(QuoteCreateViewModel quoteCreateViewModel);

        /// <summary>
        /// Get Quote Receipt by quoteId
        /// </summary>
        /// <param name="omsOrderId"></param>
        /// <returns>QuoteResponseViewModel</returns>
        QuoteResponseViewModel GetQuoteReceipt(int omsQuoteId);

        /// <summary>
        /// Get Quote List
        /// </summary>
        /// <returns>List of Quotes</returns>
        List<QuoteViewModel> GetQuoteList();

        /// <summary>
        /// Get Quote Details
        /// </summary>
        /// <param name="omsQuoteId"></param>
        /// <returns></returns>
        QuoteResponseViewModel GetQuote(int omsQuoteId);

        /// <summary>
        /// Convert quote to order.
        /// </summary>
        /// <param name="convertToOrderModel"></param>
        /// <returns>OrdersViewModel</returns>
        OrdersViewModel ConvertQuoteToOrder(ConvertQuoteToOrderViewModel convertToOrderModel);


        ///// <summary>
        ///// Get Paged Quote List
        ///// </summary>
        ///// <param name="filters"></param>
        ///// <param name="sortCollection"></param>
        ///// <param name="pageIndex"></param>
        ///// <param name="recordPerPage"></param>
        ///// <returns>Quote List</returns>
        QuoteListViewModel GetQuoteList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);


        QuoteResponseViewModel GetGuestQuoteReceipt(string QuoteNumber,string EmailAddress);
    }
}
