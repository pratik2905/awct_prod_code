﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.Agents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
   public interface IAWCTProductAgent : IProductAgent
    {
        ConfigurableProductModel GetConfigurableProductViewModel(int productID);

        ConfigurableProductModel GetPriceSizeList(int configurableProductId);

        new AWCTProductViewModel GetProduct(int productID);

        AWCTTruColorViewModel GetGlobalAttributeData(string globalAttributeCodes);

      // new AWCTProductViewModel GetExtendedProductDetails(int productID, string[] expandKeys);
    }
}
