﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.Agents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IAWCTCustomizedFormAgent
    {
        bool ModelSearch(AWCTModelSearchViewModel model);
        bool NewCustomerApplication(AWCTNewCustomerApplicationViewModel model);

        bool BecomeAContributer(AWCTBecomeAContributerViewModel model);

        bool PreviewShow(AWCTPreviewShowViewModel model);
        /*Start Quote Lookup*/
        bool QuoteLookup(AWCTQuoteLookupViewModel model);
        /*End Quote Lookup*/
    }
}
