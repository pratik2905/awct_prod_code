﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Core.Agents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTFormBuilderAgent: FormBuilderAgent
    {
        private readonly IFormBuilderClient _formBuilderClient;

        public AWCTFormBuilderAgent(IFormBuilderClient formBuilderClient, IGlobalAttributeClient globalAttributeClient, IGlobalAttributeEntityClient globalAttributeEntityClient): base( formBuilderClient,  globalAttributeClient,  globalAttributeEntityClient)
        {
            _formBuilderClient = GetClient<IFormBuilderClient>(formBuilderClient);
            
        }

        public override FormSubmitViewModel CreateFormTemplate(BindDataModel bindDataModel)
        {
           
            RemoveNonAttributeKeys(bindDataModel);
            RemoveAttributeWithEmptyValue(bindDataModel);
            FormSubmitViewModel attributeViewModel = GetFormSubmitViewModel(bindDataModel);
            try
            {
                foreach (var item in attributeViewModel.Attributes)
                {
                    string AttributeCode = ConfigurationManager.AppSettings["AttributeCode"].ToString();
                    bool IsAttributeCode = AttributeCode.Split(',').Contains(item.AttributeCode);
                    if (IsAttributeCode)
                    {
                        string emailRegex = "^[0-9a-zA-Z!?.,# ]+$";
                        Regex re = new Regex(emailRegex);
                        if (!re.IsMatch(item.AttributeValue))
                        {
                            attributeViewModel.SuccessMessage = "Failed.";
                            return attributeViewModel;
                        }

                    }

                }
            }
            catch (ZnodeException ex)
            {
                attributeViewModel.SuccessMessage = ex.ErrorMessage;
                return attributeViewModel;
                
            }
            FormSubmitModel model = _formBuilderClient.CreateFormTemplate(attributeViewModel.ToModel<FormSubmitModel>());
            return model?.ToViewModel<FormSubmitViewModel>();
        }

        private FormSubmitViewModel GetFormSubmitViewModel(BindDataModel model)
        {
            FormSubmitViewModel entityAttributeModel = new FormSubmitViewModel();
            model.ControlsData?.ToList().ForEach(item =>
            {
                List<object> itemList = new List<object>();
                itemList.AddRange(item.Key.Split('_'));
                if (itemList.Count() >= 5)
                {
                    entityAttributeModel.Attributes.Add(new FormSubmitAttributeViewModel
                    {
                        AttributeCode = itemList[0].ToString(),
                        GlobalAttributeId = Convert.ToInt32(itemList[1]),
                        GlobalAttributeDefaultValueId = Convert.ToInt32(itemList[2]),
                        GlobalAttributeValueId = Convert.ToInt32(itemList[3]),
                        AttributeValue = item.Value.ToString().Trim(),
                        LocaleId = Convert.ToInt32(DefaultSettingHelper.DefaultLocale),
                        FormTemplateId = Convert.ToInt32(itemList[4])
                    });
                }
            });
            entityAttributeModel.FormBuilderId = entityAttributeModel.Attributes?.FirstOrDefault()?.FormTemplateId;
            entityAttributeModel.PortalId = PortalAgent.CurrentPortal.PortalId;
            entityAttributeModel.LocaleId = PortalAgent.LocaleId;
            entityAttributeModel.CustomerEmail = GetCustomerEmail(model);
            return entityAttributeModel;
        }
        private string GetCustomerEmail(BindDataModel model)
        {
            var controls = model.ControlsData.Where(o => o.Key.Contains("_Email")).Select(x => x.Value).ToList();
            return string.Join(",", controls);
        }
    }
}
