﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Sample.Api.Model.CustomizedFormModel;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;


namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTCustomizedFormAgent : IAWCTCustomizedFormAgent
    {

       
        private readonly IAWCTCustomizedFormClient _AWCTCustomizedFormClient;

        public AWCTCustomizedFormAgent(IAWCTCustomizedFormClient AWCTCustomizedFormClient) 
        {
            _AWCTCustomizedFormClient = AWCTCustomizedFormClient;
        }


        public virtual bool ModelSearch(AWCTModelSearchViewModel model)
        {
            var search = _AWCTCustomizedFormClient.ModelSearch(model.ToModel<AWCTModelSearchModel>());
            return true;
        }

        public virtual bool NewCustomerApplication(AWCTNewCustomerApplicationViewModel model)
        {
            var customer = _AWCTCustomizedFormClient.NewCustomerApplication(model.ToModel<AWCTNewCustomerApplicationModel>());
            return true;
        }

        public virtual bool BecomeAContributer(AWCTBecomeAContributerViewModel model)
        {
            var contributor = _AWCTCustomizedFormClient.BecomeAContributer(model.ToModel<AWCTBecomeAContributerModel>());
            return true;
        }

        public virtual bool PreviewShow(AWCTPreviewShowViewModel model)
        {
            var contributor = _AWCTCustomizedFormClient.PreviewShow(model.ToModel<AWCTPreviewShowModel>());
            return true;
        }
        /*Start Quote Lookup*/
        public virtual bool QuoteLookup(AWCTQuoteLookupViewModel model)
        {
            var quotelookup = _AWCTCustomizedFormClient.QuoteLookup(model.ToModel<AWCTQuoteLookupModel>());
            return true;
        }
        /*End Quote Lookup*/

    }
}
