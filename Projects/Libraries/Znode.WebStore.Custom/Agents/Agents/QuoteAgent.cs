﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model.Quote;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;



namespace Znode.WebStore.Custom.Agents.Agents
{
    public class QuoteAgent : BaseAgent, IQuoteAgent
    {
        #region Private Variables
        private readonly IQuoteClient _quoteClient;
        private readonly ICartAgent _cartAgent;
        private readonly IShippingClient _shippingsClient;
        private readonly IUserAgent _userAgent;
        //private readonly ICartAgent _awctcartAgent;
        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly ICheckoutAgent _checkoutAgent;
        private readonly IUserClient _userClient;
        private readonly IWebStoreUserClient _webStoreAccountClient;


        #endregion

        public QuoteAgent(IQuoteClient quoteClient, IShippingClient shippingsClient, IUserClient userClient, IWebStoreUserClient webStoreAccountClient)
        {
            _quoteClient = GetClient<IQuoteClient>(quoteClient);
            _shippingsClient = GetClient<IShippingClient>(shippingsClient);
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _userAgent = new UserAgent(GetClient<CountryClient>(), GetClient<WebStoreUserClient>(), GetClient<WishLishClient>(), GetClient<UserClient>(), GetClient<PublishProductClient>(), GetClient<CustomerReviewClient>(), GetClient<OrderClient>(), GetClient<GiftCardClient>(), GetClient<AccountClient>(), GetClient<AccountQuoteClient>(), GetClient<OrderStateClient>(), GetClient<PortalCountryClient>(), GetClient<ShippingClient>(), GetClient<PaymentClient>(), GetClient<CustomerClient>(), GetClient<StateClient>(), GetClient<PortalProfileClient>());
            _shoppingCartsClient = GetClient<ShoppingCartClient>();
            _checkoutAgent = new CheckoutAgent(GetClient<ShippingClient>(), GetClient<PaymentClient>(), GetClient<PortalProfileClient>(), GetClient<CustomerClient>(), GetClient<UserClient>(), GetClient<OrderClient>(), GetClient<AccountClient>(), GetClient<WebStoreUserClient>(), GetClient<PortalClient>(), GetClient<ShoppingCartClient>(), GetClient<AddressClient>());
            _userClient = GetClient<IUserClient>(userClient);
            _webStoreAccountClient = GetClient<IWebStoreUserClient>(webStoreAccountClient);


        }

        #region Public Methods

        /// <summary>
        /// Create Quote
        /// </summary>
        /// <param name="quoteCreateViewModel"></param>
        /// <returns></returns>
        public virtual QuoteCreateViewModel Create(QuoteCreateViewModel quoteCreateViewModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            try
            {
                if (IsNotNull(quoteCreateViewModel))
                {
                    //Get cart from session or by cookie.
                    ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);//?? _cartAgent.GetCartFromCookie();

                    UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
                    UserViewModel user = userViewModel ?? GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey);
                    if (IsNull(user) || user?.UserId < 1)
                    {

                        
                        user = CreateAnonymousUserAccount(cartModel.BillingAddress, cartModel.ShippingAddress?.EmailAddress);
                        quoteCreateViewModel.Custom1 = "GuestUser";
                        quoteCreateViewModel.Custom2 = cartModel.ShippingAddress?.EmailAddress;
                        //AddressViewModel addressViewModel = _webStoreAccountClient.CreateAccountAddress(addressViewModel?.ToModel<AddressModel>())?.ToViewModel<AddressViewModel>();
                        //if (!addressViewModel.HasError)
                        //    addressViewModel.SuccessMessage = WebStore_Resources.SuccessAddressAdded;

                        GetAnonymousUserAddresses(cartModel, quoteCreateViewModel);


                        UserViewModel oldSession = GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey);
                        if (!Equals(oldSession, null))
                        {
                            oldSession.GuestUserId = oldSession.UserId;
                            cartModel.UserId = oldSession.UserId;
                            if (IsNotNull(userViewModel))
                            {
                                userViewModel.UserId = oldSession.UserId;
                            }

                            SaveInSession(WebStoreConstants.GuestUserKey, oldSession);
                        }
                    }

                    if (IsNotNull(cartModel))
                    {
                        //Send shipping address in cart for validation
                        BooleanModel booleanModel = IsValidAddressForCheckout(cartModel?.ShippingAddress, quoteCreateViewModel.ShippingAddressId);
                        if ((!booleanModel.IsSuccess) && !(bool)PortalAgent.CurrentPortal.PortalFeatureValues.Where(x => x.Key.Contains(StoreFeature.Require_Validated_Address.ToString()))?.FirstOrDefault().Value)
                        {
                            return (QuoteCreateViewModel)GetViewModelWithErrorMessage(new QuoteCreateViewModel(), booleanModel.ErrorMessage ?? WebStore_Resources.AddressValidationFailed);
                        }

                        AddressViewModel addressViewModel = _checkoutAgent.GetAddressById(quoteCreateViewModel.ShippingAddressId, WebStoreConstants.ShippingAddressType);

                        //Set details from shopping cart
                        SetShoppingCartDetailsForQuotes(quoteCreateViewModel, cartModel);
                        quoteCreateViewModel.CookieMappingId = cartModel.CookieMappingId;
                        quoteCreateViewModel.OmsQuoteStatus = ZnodeOrderStatusEnum.SUBMITTED.ToString();
                        quoteCreateViewModel.QuoteTypeCode = ZnodeConstant.Quote;

                        int quoteExpiredInDays = GetQuoteExpireDays();
                        quoteCreateViewModel.QuoteExpirationDate = DateTime.Now.AddDays(quoteExpiredInDays);

                        if (IsNotNull(PortalAgent.CurrentPortal.PublishState))
                        {
                            quoteCreateViewModel.PublishStateId = (byte)PortalAgent.CurrentPortal.PublishState;
                        }
                        foreach (ShoppingCartItemModel childLineItem in cartModel.ShoppingCartItems)
                        {
                            //quoteCreateViewModel.Custom5 = cartModel.ShoppingCartItems[0].PersonaliseValuesDetail;

                            StringBuilder PersonalizeDesc = new StringBuilder();
                            string personalizeDetails = Convert.ToString(childLineItem.PersonaliseValuesDetail?.FirstOrDefault(x => x.PersonalizeCode == "OptionValue")?.PersonalizeValue);
                            if (!string.IsNullOrEmpty(personalizeDetails))
                            {
                                if (personalizeDetails.ToString() != "PersonaliseCode[sku")
                                {
                                    //quoteCreateViewModel.Custom5 = personalizeDetails + "<br />";

                                    quoteCreateViewModel.productDetails.ForEach(x =>
                                    {
                                        //if (Convert.ToString(x.SKU) == Convert.ToString(childLineItem.SKU))
                                        //{
                                        //}
                                        if (Convert.ToString(x.OmsSavedcartLineItemId) == Convert.ToString(childLineItem.OmsSavedcartLineItemId))
                                        {
                                            x.Custom5 = personalizeDetails + "<br />";

                                        }

                                    });

                                }
                               
                            }
                            else {

                                quoteCreateViewModel.productDetails.ForEach(x => {
                                    if (Convert.ToString(x.Custom4) == "AddOn")
                                    {
                                        x.Custom5 = childLineItem.ProductName;

                                    }

                                }); 
                            }

                        }
                        QuoteCreateViewModel createdQuoteViewModel = _quoteClient.Create(quoteCreateViewModel.ToModel<QuoteCreateModel>()).ToViewModel<QuoteCreateViewModel>();
                        if (IsNotNull(createdQuoteViewModel) && !createdQuoteViewModel.HasError)
                        {
                            ClearCartCountFromSession();
                            SaveInSession(WebStoreConstants.CartModelSessionKey, new ShoppingCartModel());
                            return createdQuoteViewModel;
                        }
                        return (QuoteCreateViewModel)GetViewModelWithErrorMessage(new QuoteCreateViewModel(), createdQuoteViewModel.ErrorMessage);
                    }
                }
                ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return (QuoteCreateViewModel)GetViewModelWithErrorMessage(new QuoteCreateViewModel(), WebStore_Resources.ErrorFailedToCreate);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (QuoteCreateViewModel)GetViewModelWithErrorMessage(new QuoteCreateViewModel(), WebStore_Resources.ProcessingFailedError);
            }
        }

        public virtual QuoteResponseViewModel GetQuoteReceipt(int omsQuoteId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            try
            {
                if (omsQuoteId > 0)
                {
                    QuoteResponseViewModel quoteModel = _quoteClient.GetQuoteReceipt(omsQuoteId)?.ToViewModel<QuoteResponseViewModel>();

                    if (IsNotNull(quoteModel))
                    {
                        return quoteModel;

                    }
                }
                ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return (QuoteResponseViewModel)GetViewModelWithErrorMessage(new QuoteResponseViewModel(), WebStore_Resources.ErrorFailedToCreate);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (QuoteResponseViewModel)GetViewModelWithErrorMessage(new QuoteResponseViewModel(), WebStore_Resources.ProcessingFailedError);
            }
        }

        ////Get Quote List
        public virtual List<QuoteViewModel> GetQuoteList()
        {
            UserViewModel userDetails = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            int userId = Convert.ToInt32(userDetails?.UserId);

            FilterCollection _filters = new FilterCollection();

            //Set userId Filter.
            SetUserIdFilter(_filters, userId);

            //Set QuoteTypeId Filter.
            SetQuoteTypeIdFilter(_filters);

            //Add portal id in filter collection.
            AddPortalIdInFilters(_filters, PortalAgent.CurrentPortal.PortalId);

            QuoteListModel quoteListModel = GetQuotes(_filters, 1, 3, null);

            List<QuoteViewModel> quoteList = quoteListModel?.Quotes?.ToViewModel<QuoteViewModel>()?.ToList();

            return quoteList;
        }

        ////Get quote list.
        public virtual QuoteListViewModel GetQuoteList(FilterCollection filters, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            //filters = new FilterCollection();
            AddPortalIdInFilters(filters, PortalAgent.CurrentPortal.PortalId);
            QuoteListModel quoteList = GetQuotes(filters, pageIndex, recordPerPage, sortCollection);

            QuoteListViewModel quoteListViewModel = new QuoteListViewModel { Quotes = quoteList?.Quotes.ToViewModel<QuoteViewModel>().ToList() };
            quoteListViewModel.Filters = filters;
            SetListPagingData(quoteListViewModel, quoteList);

            return IsNotNull(quoteListViewModel?.Quotes) ? quoteListViewModel : new QuoteListViewModel();
        }

        ////Get Quote View by omsQuoteId.
        public virtual QuoteResponseViewModel GetQuote(int omsQuoteId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            try
            {
                if (omsQuoteId > 0)
                {
                    QuoteResponseViewModel quoteModel = _quoteClient.GetQuoteReceipt(omsQuoteId)?.ToViewModel<QuoteResponseViewModel>();
                    quoteModel.EnableConvertToOrder = IsQuoteValidForConvertToOrder(quoteModel);
                    if (IsNotNull(quoteModel))
                    {
                        return quoteModel;
                    }
                }
                ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return (QuoteResponseViewModel)GetViewModelWithErrorMessage(new QuoteResponseViewModel(), WebStore_Resources.ErrorFailedToCreate);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (QuoteResponseViewModel)GetViewModelWithErrorMessage(new QuoteResponseViewModel(), WebStore_Resources.ProcessingFailedError);
            }
        }

        // Convert Quote to Order.
        public virtual OrdersViewModel ConvertQuoteToOrder(ConvertQuoteToOrderViewModel convertToOrderModel)
        {
            try
            {
                if (IsNotNull(convertToOrderModel))
                {
                    OrdersViewModel ordersViewModel = _quoteClient.ConvertQuoteToOrder(convertToOrderModel.ToModel<ConvertQuoteToOrderModel>())?.ToViewModel<OrdersViewModel>();
                    if (IsNotNull(ordersViewModel))
                    {
                        IWebstoreHelper helper = GetService<IWebstoreHelper>();
                        helper.SaveDataInCookie(WebStoreConstants.UserOrderReceiptOrderId, Convert.ToString(ordersViewModel.OmsOrderId), 1);
                    }
                    return ordersViewModel;

                }
                return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), WebStore_Resources.ErrorFailedToCreate);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), WebStore_Resources.ProcessingFailedError);
            }
        }

        public virtual UserViewModel CreateAnonymousUserAccount(AddressModel address, string emailAddress)
        {
            UserViewModel user = _userClient.CreateCustomerAccount(new UserModel { FirstName = address?.FirstName, LastName = address?.LastName, Email = emailAddress, IsGuestUser = true, PortalId = PortalAgent.CurrentPortal.PortalId, ProfileId = PortalAgent.CurrentPortal.ProfileId })?.ToViewModel<UserViewModel>();
            user.Email = emailAddress;
            SaveInSession(WebStoreConstants.GuestUserKey, user);
            return user;
        }

        public virtual List<AddressModel> GetAnonymousUserAddresses(ShoppingCartModel cartModel, QuoteCreateViewModel quoteCreateViewModel)
        {
            List<AddressModel> addressList = new List<AddressModel>();
            if (IsNotNull(cartModel))
            {
                int userId = (GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey)?.UserId).GetValueOrDefault();
                if (userId < 1)
                {
                    userId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId ?? 0;
                }

                //Check if shipping address of cart model is not null then create it shipping address of guest user.
                if (IsNotNull(cartModel.ShippingAddress))
                {
                    AddressModel address = CreateGuestUserShippingAddress(userId, cartModel);
                    if (IsNotNull(address))
                    {
                        addressList.Add(address);
                        quoteCreateViewModel.ShippingAddressId = address.AddressId;
                        quoteCreateViewModel.BillingAddressId = address.AddressId;
                        cartModel.ShippingAddress.AddressId = address.AddressId;
                    }
                }
                //For amazon pay skipping billing address for anonymous user.
                if (Equals(cartModel.BillingAddress, cartModel.ShippingAddress) && Equals(cartModel.Payment.PaymentName, ZnodeConstant.Amazon_Pay))
                {
                    return addressList;
                }
                if (IsNotNull(cartModel.BillingAddress) && !cartModel.BillingAddress.IsShippingBillingDifferent)
                {
                    cartModel.BillingAddress.AddressId = cartModel.ShippingAddress.AddressId;
                    cartModel.BillingAddress.UserId = cartModel.ShippingAddress.UserId;
                    cartModel.BillingAddress.IsGuest = cartModel.ShippingAddress.IsGuest;
                }
                //Check if billing address of cart model is not null then create it billing address of guest user.
                if (IsNotNull(cartModel.BillingAddress) && cartModel.BillingAddress.IsShippingBillingDifferent == true)
                {
                    cartModel.BillingAddress.UserId = userId;

                    //Create guest users addresses.
                    AddressModel address = CreateAnonymousUserAddress(cartModel.BillingAddress);
                    if (IsNotNull(address))
                    {
                        addressList.Add(address);
                        quoteCreateViewModel.BillingAddressId = address.AddressId;
                        cartModel.BillingAddress.AddressId = address.AddressId;
                    }
                }
            }
            return addressList;
        }
        #endregion
        private AddressModel CreateGuestUserShippingAddress(int userId, ShoppingCartModel cartModel)
        {
            cartModel.ShippingAddress.UserId = userId;
            cartModel.ShippingAddress.IsGuest = true;
            //Create guest users addresses.
            return CreateAnonymousUserAddress(cartModel.ShippingAddress);
        }
        private AddressModel CreateAnonymousUserAddress(AddressModel address)
        {
            return _webStoreAccountClient.CreateAccountAddress(address);
        }
        #region Protected Methods

        //returns the value of store level global attribute which signifies the number of days in which quote will get expire
        public static int GetQuoteExpireDays()
        => Convert.ToInt32(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "QuoteExpireInDays", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue);


        //Check whether shipping address is valid or not.
        protected virtual BooleanModel IsValidAddressForCheckout(AddressModel shippingAddress, int shippingAddressId)
        {
            if ((bool)PortalAgent.CurrentPortal.PortalFeatureValues.Where(x => x.Key.Contains(StoreFeature.Address_Validation.ToString()))?.FirstOrDefault().Value)
            {
                if (IsNull(shippingAddress) || shippingAddress?.AddressId == 0)
                {
                    //if it is not available then only send shipping address from user address list for validation in USPS.
                    //Get the list of all addresses associated to current logged in user.
                    List<AddressModel> userAddresses = GetUserAddressList();
                    shippingAddress = userAddresses?.Where(x => x.AddressId == shippingAddressId)?.FirstOrDefault();
                }

                if (shippingAddress != null)
                {
                    shippingAddress.PublishStateId = (byte)PortalAgent.CurrentPortal.PublishState;
                }

                //Do not allow the customer to go to next page if valid shipping address required is enabled.
                return _shippingsClient.IsShippingAddressValid(shippingAddress);
            }

            return new BooleanModel { IsSuccess = true };
        }

        // Set QuoteTypeId Filter.
        protected virtual void SetQuoteTypeIdFilter(FilterCollection filters)
        {
            filters?.RemoveAll(x => string.Equals(x.FilterName, ZnodeOmsQuoteEnum.OmsQuoteTypeId.ToString(), StringComparison.CurrentCultureIgnoreCase));
            filters.Add(new FilterTuple("OmsQuoteTypeId", FilterOperators.Equals, "3"));

        }
        //Add Portal id in filter collection
        protected virtual void AddPortalIdInFilters(FilterCollection filters, int portalId)
        {
            if (portalId > 0)
            {
                filters.RemoveAll(x => string.Equals(x.FilterName, FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase));
                filters.Add(new FilterTuple(FilterKeys.PortalId.ToString(), FilterOperators.Equals, portalId.ToString()));
            }
        }
        // Set userId Filter.
        protected virtual void SetUserIdFilter(FilterCollection filters, int userId)
        {
            if (userId > 0)
            {
                filters?.RemoveAll(x => string.Equals(x.FilterName, ZnodeOmsQuoteEnum.UserId.ToString(), StringComparison.CurrentCultureIgnoreCase));
                filters.Add(new FilterTuple(FilterKeys.UserId, FilterOperators.Equals, userId.ToString()));
            }
        }
        //To remove the cart count key from session.
        private void ClearCartCountFromSession() => RemoveInSession("CartCount");
        ////Get quote list.
        protected virtual QuoteListModel GetQuotes(FilterCollection filters, int? pageIndex, int? recordPerPage, SortCollection sortCollection = null)
        {
            if (HelperUtility.IsNull(filters))
            {
                filters = new FilterCollection();
            }

            UserViewModel userDetails = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            int userId = Convert.ToInt32(userDetails?.UserId);

            //Set userId Filter.
            SetUserIdFilter(filters, userId);

            //Set QuoteTypeId Filter.
            SetQuoteTypeIdFilter(filters);

            //Add portal id in filter collection.
            AddPortalIdInFilters(filters, PortalAgent.CurrentPortal.PortalId);

            QuoteListModel quoteList = _quoteClient.GetQuoteList(filters, sortCollection, pageIndex, recordPerPage);

            return quoteList;
        }

        //Check if the Quote is Valid For Convert To an Order
        protected virtual bool IsQuoteValidForConvertToOrder(QuoteResponseViewModel quoteModel)
        {
            return (IsNotNull(quoteModel.QuoteExpirationDate) && quoteModel.QuoteExpirationDate == DateTime.Now ||
                    (string.Equals(quoteModel.QuoteStatus, "EXPIRED".ToString(), StringComparison.InvariantCultureIgnoreCase)
                    //|| string.Equals(quoteModel.QuoteStatus, ZnodeOrderStatusEnum.SUBMITTED.ToString(), StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(quoteModel.QuoteStatus, ZnodeOrderStatusEnum.APPROVED.ToString(), StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(quoteModel.QuoteStatus, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    || quoteModel.IsConvertedToOrder) ? false : true;
        }
        //#endregion

        //#region Private Methods

        //Get all address list of customer.
        private List<AddressModel> GetUserAddressList()
        {
            return _userAgent.GetAddressList()?.AddressList?.ToModel<AddressModel>()?.ToList();
        }

        //Bind all details of shopping  cart model.
        private void SetShoppingCartDetailsForQuotes(QuoteCreateViewModel quoteCreateViewModel, ShoppingCartModel cartModel)
        {
            if (IsNotNull(cartModel))
            {
                quoteCreateViewModel.CreatedDate = DateTime.Now.ToString();
                quoteCreateViewModel.UserId = cartModel.UserId.GetValueOrDefault();
                quoteCreateViewModel.PortalId = cartModel.PortalId;

                quoteCreateViewModel.QuoteExpirationDate = DateTime.Now;

                quoteCreateViewModel.ShippingCost = cartModel.ShippingCost;
                quoteCreateViewModel.TaxCost = cartModel.TaxCost;
                quoteCreateViewModel.QuoteTotal = cartModel.Total;

                quoteCreateViewModel.ShippingId = quoteCreateViewModel.ShippingId == 0 ? cartModel.ShippingId : quoteCreateViewModel.ShippingId;

                quoteCreateViewModel.AdditionalInstruction = quoteCreateViewModel.AdditionalInstruction;
                quoteCreateViewModel.PublishStateId = (byte)ZnodePublishStatesEnum.PRODUCTION;
                quoteCreateViewModel.InHandDate = quoteCreateViewModel.InHandDate;
                quoteCreateViewModel.ShippingConstraintCode = quoteCreateViewModel.ShippingConstraintCode;
                quoteCreateViewModel.productDetails = cartModel.ShoppingCartItems.ToViewModel<ProductDetailModel>().ToList();
            }
        }

        public virtual QuoteResponseViewModel GetGuestQuoteReceipt(string QuoteNumber,string EmailAddress)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            try
            {
                  
                if (!string.IsNullOrEmpty(QuoteNumber) && !string.IsNullOrEmpty(EmailAddress))
                {
                    QuoteModel model = new QuoteModel();
                    model.QuoteNumber = QuoteNumber;
                    model.EmailId = EmailAddress;
                    ZnodeLogging.LogMessage("start GetGuestQuoteReceipt", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                    QuoteResponseViewModel quoteModel = _quoteClient.GetGuestQuoteReceipt(model)?.ToViewModel<QuoteResponseViewModel>();
                    ZnodeLogging.LogMessage("end GetGuestQuoteReceipt", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    if (IsNotNull(quoteModel))
                    {
                        return quoteModel;

                    }
                }
                ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return (QuoteResponseViewModel)GetViewModelWithErrorMessage(new QuoteResponseViewModel(), WebStore_Resources.ErrorFailedToCreate);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (QuoteResponseViewModel)GetViewModelWithErrorMessage(new QuoteResponseViewModel(), WebStore_Resources.ProcessingFailedError);
            }

        }
        #endregion
    }
}
