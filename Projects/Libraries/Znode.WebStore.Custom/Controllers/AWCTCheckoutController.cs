﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Controllers
{
    public class AWCTCheckoutController : CheckoutController
    {
        
        private readonly IAWCTCartAgent _cartAgent;
        private string TotalTableView = "_TotalTable";
        private readonly ICheckoutAgent _checkoutAgent;
        private readonly bool IsEnableSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;
        public AWCTCheckoutController(IUserAgent userAgent, ICheckoutAgent checkoutAgent, IAWCTCartAgent cartAgent, IPaymentAgent paymentAgent) : base(userAgent, checkoutAgent, cartAgent, paymentAgent)
        {
            _cartAgent = cartAgent;
            _checkoutAgent = checkoutAgent;
        }
        [HttpGet]
        //[ValidateAntiForgeryToken] -- For future use.
        public virtual JsonResult RemoveDiscount(string Promostatus)
        {
            ZnodeLogging.LogMessage("RemoveDiscount enter", ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            CartViewModel cartViewModel = _cartAgent.RemoveDiscount(Promostatus);
            //return PartialView(TotalTableView, cartViewModel);
            string totalView = RenderRazorViewToString(TotalTableView, cartViewModel);
            return Json(new
            {
                html = totalView,
                coupons = cartViewModel.Coupons,
                giftCardCode = "",
                isGiftCard = false,
                message = cartViewModel.SuccessMessage
            }, JsonRequestBehavior.AllowGet);
        }
        public override ActionResult Index(bool IsSinglePage = true)
        {
            CartViewModel accountQuoteViewModel = null;
            if (!Equals(Request.QueryString["QuoteId"], null))
            {
                accountQuoteViewModel = _cartAgent.SetQuoteCart(Convert.ToInt32(Request.QueryString["QuoteId"]));
            }

            if (!Equals(Request.QueryString["ShippingId"], null))
            {
                _cartAgent.AddEstimatedShippingIdToCartViewModel(int.Parse(Convert.ToString(Request.QueryString["ShippingId"])));
            }

            if ((User.Identity.IsAuthenticated || Convert.ToString(Request.QueryString["mode"]) == "guest"))
            {
              
                return _cartAgent.GetCartCount() < 1 ? RedirectToAction<HomeController>(x => x.Index()) : IsEnableSinglePageCheckout ? View("SinglePage", _checkoutAgent.GetUserDetails(accountQuoteViewModel?.UserId ?? 0)) : View("MultiStepCheckout", _checkoutAgent.GetBillingShippingAddress());
            }
            
            return RedirectToAction("Login", "User", new { returnUrl = "~/checkout", isSinglePageCheckout = IsEnableSinglePageCheckout });
        }


        //Get logged in user cart to review
        public override ActionResult CartReview(int? shippingOptionId, int? shippingAddressId, string shippingCode, string additionalInstruction = "")
        {
            return PartialView("_CartReview", _cartAgent.CalculateShippingCartReview(shippingOptionId.GetValueOrDefault(), shippingAddressId.GetValueOrDefault(), shippingCode, additionalInstruction));
        }
        public override JsonResult GetAjaxHeaders()
        {
            return Json(new AjaxHeadersModel(), JsonRequestBehavior.AllowGet);
        }

    }
}
