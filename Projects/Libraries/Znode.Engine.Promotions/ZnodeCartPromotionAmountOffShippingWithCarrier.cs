﻿using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionAmountOffShippingWithCarrier : ZnodeCartPromotionType
    {
        #region Private Variable
        private readonly ZnodePromotionHelper helper = new ZnodePromotionHelper();
        #endregion

        #region Constructor
        public ZnodeCartPromotionAmountOffShippingWithCarrier()
        {
            Name = "Amount Off Shipping With Carrier";
            Description = "Applies an amount off shipping with carrier for an order; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the amount off shipping for the order.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            //to check promotion is applicable for this shipping method
            if (!IsPromotionApplicable())
                return;

            OrderBy = nameof(PromotionModel.OrderMinimum);
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));


            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                    return;

                ApplyDiscount(isCouponValid, couponIndex);
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, true))
                    return;

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        ApplyDiscount(isCouponValid, couponIndex, coupon.Code);
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
                AddPromotionMessage(couponIndex);
            }
        }

        #endregion

        #region Private Method
        //to Apply Discount
        private void ApplyDiscount(bool isCouponValid, int? couponIndex, string couponCode = "")
        {
            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            if (PromotionBag.MinimumOrderAmount <= subTotal)
            {
                ShoppingCart.Shipping.ShippingDiscount += PromotionBag.Discount;
                ShoppingCart.IsAnyPromotionApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = false;
            }

            if (!string.IsNullOrEmpty(couponCode) && ShoppingCart.IsAnyPromotionApplied)
            {
                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                ShoppingCart.Shipping.ShippingDiscountDescription = couponCode;
                ShoppingCart.Shipping.ShippingDiscountType = (int)OrderDiscountTypeEnum.COUPONCODE;
            }
            else if (ShoppingCart.IsAnyPromotionApplied)
            {
                ShoppingCart.IsAnyPromotionApplied = true;
                ShoppingCart.Shipping.ShippingDiscountDescription = PromotionBag.PromoCode;
                ShoppingCart.Shipping.ShippingDiscountType = (int)OrderDiscountTypeEnum.PROMOCODE;
            }
        }

        //to get shopping cart shipping id
        private int GetCartShippingId()
            => ShoppingCart?.Shipping?.ShippingID ?? 0;

        //to check this promotion is applicable for shopping cart shipping option if yes the retun true 
        private bool IsPromotionApplicable()
        {
            int cartShippingId = GetCartShippingId();
            if (cartShippingId > 0)
            {
                List<ShippingModel> promotionsShipping = helper.GetPromotionShipping(PromotionBag.PromotionId);
                if (HelperUtility.IsNotNull(promotionsShipping))
                {
                    foreach (ShippingModel shipping in promotionsShipping)
                    {
                        if (shipping.ShippingId == cartShippingId)
                            return true;
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
