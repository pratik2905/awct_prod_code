﻿using System.Collections.Generic;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IQuoteClient : IBaseClient
    {
        QuoteListModel GetQuoteList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        QuoteCreateModel Create(QuoteCreateModel quoteCreateModel);

        QuoteResponseModel GetQuoteReceipt(int quoteId);

        QuoteResponseModel GetQuoteById(int omsQuoteId);

        OrderModel ConvertQuoteToOrder(ConvertQuoteToOrderModel convertToOrderModel);

        List<QuoteLineItemModel> GetQuoteLineItemByQuoteId(int omsQuoteId);

        BooleanModel UpdateQuote(UpdateQuoteModel model);

        QuoteResponseModel GetGuestQuoteReceipt(QuoteModel model);
    }
}
