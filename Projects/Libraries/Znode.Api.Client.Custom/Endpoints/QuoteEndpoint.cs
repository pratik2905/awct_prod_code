﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class QuoteEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return BaseEndpoint.ApiRoot + "/quote/list";
        }

        public static string Create()
        {
            return BaseEndpoint.ApiRoot + "/quote/create";
        }

        public static string GetQuoteReceipt(int quoteId)
        {
            return string.Format("{0}/quote/getquotereceipt/{1}", (object)BaseEndpoint.ApiRoot, (object)quoteId);
        }

        //public static string GetGuestQuoteReceipt( string QuoteNumber, string EmailAddress)
        //{
        //    return string.Format("{0}/quote/getguestquotereceipt/{1}/{2}", (object)BaseEndpoint.ApiRoot, (object)QuoteNumber, (object)EmailAddress);
        //}

        public static string GetGuestQuoteReceipt() => $"{ApiRoot}/quote/getguestquotereceipt";


        public static string GetQuoteById(int omsQuoteId)
        {
            return string.Format("{0}/quote/getquotebyid/{1}", (object)BaseEndpoint.ApiRoot, (object)omsQuoteId);
        }

        public static string ConvertQuoteToOrder()
        {
            return BaseEndpoint.ApiRoot + "/quote/ConvertQuoteToOrder";
        }

        public static string GetQuoteLineItemByQuoteId(int omsQuoteId)
        {
            return string.Format("{0}/quote/getquotelineitembyquoteid/{1}", (object)BaseEndpoint.ApiRoot, (object)omsQuoteId);
        }

        public static string UpdateQuote()
        {
            return BaseEndpoint.ApiRoot + "/quote/updatequote";
        }
    }
}
