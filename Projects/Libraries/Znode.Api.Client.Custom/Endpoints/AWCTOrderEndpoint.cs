﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AWCTOrderEndpoint : BaseEndpoint
    {
        // Get order list endpoint.
        public static string GenerateOrderNumber() => $"{ApiRoot}/AWCTOrder/generateordernumber";
    }
}
