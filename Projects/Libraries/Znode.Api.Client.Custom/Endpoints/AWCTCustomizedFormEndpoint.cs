﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AWCTCustomizedFormEndpoint : BaseEndpoint
    {
        public static string ModelSearch() => $"{ApiRoot}/awctcustomizedform/modelsearch";
        public static string NewCustomerApplication() => $"{ApiRoot}/awctcustomizedform/newcustomerapplication";
        public static string BecomeAContributer() => $"{ApiRoot}/awctcustomizedform/becomeacontributer";
        public static string PreviewShow() => $"{ApiRoot}/awctcustomizedform/previewshow";
        /*Start Quote Lookup*/
        public static string QuoteLookup() => $"{ApiRoot}/awctcustomizedform/quotelookup";
        /*End Quote Lookup*/
    }
}
