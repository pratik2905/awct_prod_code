﻿using AutoMapper;
using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.MongoDB.Data;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Engine.Api
{
    public static class CustomAutoMapperConfig
    {
        public static void Execute()
        {           
            Mapper.CreateMap<ZnodeCustomPortalDetail, CustomPortalDetailModel>().ReverseMap();
            Mapper.CreateMap<AWCTPublishProductModel, PublishProductModel>().ReverseMap();
            Mapper.CreateMap<PublishProductModel, AWCTPublishProductModel>();
            Mapper.CreateMap<ProductEntity, AWCTPublishProductModel>();

            #region Quote mappers
            Mapper.CreateMap<Znode.Sample.Api.Model.Quote.ZnodeOmsQuote, QuoteResponseModel>()
            .ForMember(d => d.OmsQuoteStateId, opt => opt.MapFrom(src => src.OmsOrderStateId))
            .ForMember(d => d.TaxAmount, opt => opt.MapFrom(src => src.TaxCost))
            .ForMember(d => d.QuoteTotal, opt => opt.MapFrom(src => src.QuoteOrderTotal));

            Mapper.CreateMap<Znode.Sample.Api.Model.Quote.ZnodeOmsQuote, ShoppingCartModel>();

            Mapper.CreateMap<ZnodeOmsQuoteLineItem, ShoppingCartItemModel>()
                .ForMember(d => d.UnitPrice, opt => opt.MapFrom(src => src.Price))
                .ForMember(d => d.CustomUnitPrice, opt => opt.MapFrom(src => src.Price))
                .ForMember(d => d.CartDescription, opt => opt.MapFrom(src => src.Description));

            Mapper.CreateMap<Znode.Sample.Api.Model.Quote.ZnodeOmsQuote, QuoteCreateModel>().ReverseMap();

            Mapper.CreateMap<Znode.Sample.Api.Model.Quote.ZnodeOmsQuote, QuoteResponseModel>()
                  .ForMember(d => d.OmsQuoteStateId, opt => opt.MapFrom(src => src.OmsOrderStateId))
                  .ForMember(d => d.QuoteTotal, opt => opt.MapFrom(src => src.QuoteOrderTotal))
                  .ForMember(d => d.PublishCatalogId, opt => opt.MapFrom(src => src.PublishStateId)).ReverseMap();
            Mapper.CreateMap<ZnodeOmsQuoteLineItem, QuoteLineItemModel>()
                .ForMember(d => d.Quantity, opt => opt.MapFrom(src => src.Quantity))
                .ForMember(d => d.Price, opt => opt.MapFrom(src => src.Price));

            Mapper.CreateMap<Znode.Sample.Api.Model.Quote.ZnodeOmsQuote, AddressModel>();
            Mapper.CreateMap<OrderHistoryModel, ZnodeOmsQuoteHistory>().ReverseMap();

            Mapper.CreateMap<ShoppingCartModel, CartParameterModel>().ReverseMap();

            Mapper.CreateMap<ConvertQuoteToOrderModel, ShoppingCartModel>().ReverseMap();
            #endregion
        }
    }
}